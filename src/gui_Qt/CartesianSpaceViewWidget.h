/*
 This file is part of EASAL.

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  Created on: 2016-2017
 *      Author: Chkhaidze Giorgio
 */

#ifndef EASAL_CARTESIANSPACEVIEWWIDGET_H
#define EASAL_CARTESIANSPACEVIEWWIDGET_H

#include <easalcore/Atlas.h>
#include <easalcore/AtlasNode.h>
#include <easalcore/Cartesian.h>
#include <easalcore/SaveLoader.h>
#include <easalcore/Settings.h>
#include <easalcore/Utils.h>
#include <easalcore/UniformCartesianSampler.h>

#include <QCheckBox>
#include <QComboBox>
#include <QGridLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QMouseEvent>
#include <QOpenGLBuffer>
#include <QOpenGLContext>
#include <QOpenGLExtraFunctions>
#include <QOpenGLFramebufferObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWidget>
#include <QSlider>
#include <QTimer>
#include <QWheelEvent>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include "BondViewWidget.h"
#include "Camera.h"
#include "CustomSlider.h"
#include "Mesh3D.h"
#include "Renderer.h"
#include "SharedDataGUI.h"

#define PI 3.14159265

using namespace std;

class CartesianSpaceViewWidget : public QOpenGLWidget
{
Q_OBJECT
public:
    CartesianSpaceViewWidget(SharedDataGUI *sharedData);

    void setAtlasNode(AtlasNode *atlasNode);

    void initializeGL() override;

    void paintGL() override;

    void resizeGL(int w, int h) override;

    void setData(Atlas *atlas, SaveLoader *saveLoader);

private Q_SLOTS:

    void showCartesianSlot();

    void translationOnlySlot();

    void coordSelectSlot();

    void showGridSlot();

    void showProjectedSpaceSlot();

    void showNotRealisableSlot();

    void showCollisionSlot();

    void showBadAngleSlot();

    void showValidSlot();

    void showOriginalCayleySlot();

    void showFlipSlot();

    void scalingSlot();

private:
    //[ COMPONENTS]
    Camera camera;
    Renderer renderer;

    QCheckBox showUCResultCheckBox;
    QCheckBox translationOnlyCheckBox;

    QLabel coordSelectLabel[3];
    QComboBox coordSelectComboBox[3];

    QCheckBox showGridCheckBox;
    QCheckBox showProjectedCheckBox;

    QCheckBox showNotRealisableCheckBox;
    QCheckBox showCollisionCheckBox;
    QCheckBox showBadAngleCheckBox;
    QCheckBox showValidCheckBox;
    QCheckBox showOriginalCayleyCheckBox;

    QCheckBox showFlipCheckBoxes[8];

    QSlider scalingSlider;
    //[~COMPONENTS]
    //[ DEPENDENCIES]
    SaveLoader *saveLoader;
    Atlas *atlas;
    SharedDataGUI *sharedData;
    //[~DEPENDENCIES]
    //[ MOUSE]
    bool mouseWasPressed = false;
    bool projectOnCartesianSpace = false;
    bool projectionChanged = false;
    //[~MOUSE]
    //[ TIMER]
    /** @brief Timer that triggers screen updates once each m_updateTimeInMS*/
    QTimer m_timer;
    double m_updateTimeInMS;
    //[ TIMER]
    //[ UNIFORMS]

    // For corresponding points
    vector<QMatrix4x4> translationMatricesPoints;
    vector<QMatrix4x4> rotationMatricesPoints;
    vector<QMatrix4x4> scaleMatricesPoints;
    QMatrix4x4 cameraMatrix;
    vector<QVector4D> colorVectorsPoints;
    QVector3D lightPositionVector;

    // For background cubes
    vector<QMatrix4x4> translationMatricesCubes;
    vector<QMatrix4x4> rotationMatricesCubes;
    vector<QMatrix4x4> scaleMatricesCubes;
    vector<QVector4D> colorVectorsCubes;

    // For showing cube analysis
    vector<QMatrix4x4> translationMatricesCentres;
    vector<QMatrix4x4> rotationMatricesCentres;
    vector<QMatrix4x4> scaleMatricesCentres;
    vector<QVector4D> colorVectorsCentres;

    // For axis
    vector<QMatrix4x4> translationMatricesAxis;
    vector<QMatrix4x4> rotationMatricesAxis;
    vector<QMatrix4x4> scaleMatricesAxis;
    vector<QVector4D> colorVectorsAxis;

    //[~UNIFORMS]
    //[ MESHES 3D]
    Mesh3D cubeMesh;
    Mesh3D sphereMesh;
    Mesh3D cylinderMesh;
    //[ MESHES 3D]
    //[ VARIABLES]
    AtlasNode *atlasNode;

    vector<CartesianPoint *> allCartesianPointsToRender;
    vector<CartesianHypercube *> allCartesianCubesToRender;

    vector<CayleyPoint *> allCayleyToRender;
    CartesianHypercube *curCube = nullptr;

    //[~VARIABLES]
    //[ FLAGS]
    bool atlasNodeIsSet = false;
    bool dataWasSet = false;

    vector<int> selectedCoords = {0, 1, 2};

    bool showGrid = false;
    bool showUCResult = true; // true: show result from UC; false: show result from regular EASAL
    bool translationOnly = false;
    bool showProjected = false;

    bool showNotRealisable = false;
    bool showCollision = false;
    bool showBadAngle = false;
    bool showValid = true;
    bool showOriginalCayley = false;
    bool showFlip[8] = {false, false, false, false, false, false, false, false};

    AtlasBuilder *builder;
    UniformCartesianSampler *sampler;

    //[~FLAGS]
    //[ INDICIES]
    int currentNodeID = -1;
    //[~INDICIES]
    //[ CONSTANTS]
    double scalingFactor = 0.1;
    float pointTransparencyStrength = 1;
    float cubeTransparencyStrength = 0.1;
    //[~CONSTANTS]
    //[ MISC]
    QGridLayout gridLayout;
    //[~MISC]

    //[COLOUR_PALETTE]
    QVector4D originalCayleyColorVector = QVector4D(1, 1, 1, 1);
    QVector4D validColorVector[8] = {QVector4D(0.35, 1, 0, 1),
                                     QVector4D(0.25, 1, 0, 1),
                                     QVector4D(0.15, 1, 0, 1),
                                     QVector4D(0.05, 1, 0, 1),
                                     QVector4D(0, 1, 0.05, 1),
                                     QVector4D(0, 1, 0.15, 1),
                                     QVector4D(0, 1, 0.25, 1),
                                     QVector4D(0, 1, 0.35, 1)};
    QVector4D collisionColorVector = QVector4D(1, 0, 0, 1);
    QVector4D badAngleColorVector = QVector4D(1, 0.1, 0.1, 1);
    QVector4D notRealisableColorVector = QVector4D(0, 0, 1, 1);
    //[~COLOUR_PALETTE]

    int cubeToAnalyseNum = 200;
    CartesianHypercube* cubeToAnalyse;


    void keyPressEvent(QKeyEvent *keyEvent) Q_DECL_OVERRIDE;

    void mousePressEvent(QMouseEvent *mouseEvent) Q_DECL_OVERRIDE;

    void mouseReleaseEvent(QMouseEvent *mouseEvent) Q_DECL_OVERRIDE;

    void wheelEvent(QWheelEvent *wheelEvent) Q_DECL_OVERRIDE;

    void generateTransformMatricesForAxis();

    void renderAxis();

    void updateAtlasNode();

    void updateTransformationMatrices();


    /**
     * @brief updateCurrentCayleyPointIndex: takes care of mousepicking but
     * doesn't work on first click so after it is called check if
     * currentCayleyPointGroupedIndex != -1
     */
    void updateCurrentCayleyPointIndex();

    void updateCameraPosition();

    void clearTransformationMatricesPoints();

    void clearTransformationMatricesCubes();

    void clearTransformationMatricesCentres();

    void clearCartesian();

    void fillCartesian(vector<CartesianPoint *> points, vector<CartesianHypercube *> cubes);

    void clearCayley();

    void fillCayley(ActiveConstraintRegion *acr);


    /**
     * @brief transformCylinderAccordingToData: generates transformation matrices
     * for cylinderMesh that is between two points
     * @param positionStart
     * @param positionEnd
     * @param radious
     * @param translation: in out matrix
     * @param rotation: in out matrix
     * @param scale: in out matrix
     */
    void transformCylinderAccordingToData(QVector3D positionStart,
                                          QVector3D positionEnd, float radious,
                                          QMatrix4x4 *translation,
                                          QMatrix4x4 *rotation,
                                          QMatrix4x4 *scale);

    void updateIndecies(int meshID, PointType pointType);

    void updateBoundary();

    void huetoColor(double hue, float color[3]);

    std::vector<CayleyPoint *> projectSpace(std::vector<CayleyPoint *> spc);

    Matrix3d quaternionToRotation(double q0, double q1, double q2, double q3);
};

#endif //EASAL_CARTESIANSPACEVIEWWIDGET_H

/*
 This file is part of EASAL.

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  Created on: 2021
 *      Author: Yichi Zhang
 */
#include "CartesianSpaceViewWidget.h"

#include "Eigen/Core"
#include "Eigen/Geometry"

using Eigen::Matrix3d;
using Eigen::Quaterniond;
using Eigen::Vector3d;

CartesianSpaceViewWidget::CartesianSpaceViewWidget(SharedDataGUI *sharedData)
{
    this->sharedData = sharedData;
    camera.init(this->size(), QVector3D(0, 0, 0), QVector3D(0, 0, -1));
    //[ TIMER SETTINGS]
    // start argument determines update rate
    m_updateTimeInMS = 30;
    m_timer.start(m_updateTimeInMS);
    // repaint is function of QWidget that triggers paintEvent
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(update()));
    //[ TIMER SETTINGS]
    //[ WIDGET SETTINGS]
    setFocusPolicy(Qt::StrongFocus);
    setUpdateBehavior(UpdateBehavior::PartialUpdate);
    //[~WIDGET SETTINGS]
    //[ MISC]

    gridLayout.setAlignment(Qt::AlignBottom);

    showUCResultCheckBox.setText("Cartesian");
    showUCResultCheckBox.setTristate(false);
    showUCResultCheckBox.setChecked(showUCResult);
    connect(&showUCResultCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showCartesianSlot()));
    gridLayout.addWidget(&showUCResultCheckBox, 0, 0, 1, 1);

    translationOnlyCheckBox.setText("Translation only");
    translationOnlyCheckBox.setTristate(false);
    translationOnlyCheckBox.setChecked(translationOnly);
    connect(&translationOnlyCheckBox, SIGNAL(stateChanged(int)), this, SLOT(translationOnlySlot()));
    gridLayout.addWidget(&translationOnlyCheckBox, 0, 1, 1, 1);

    coordSelectLabel[0].setText("Selected coord 1");
    coordSelectLabel[1].setText("Selected coord 2");
    coordSelectLabel[2].setText("Selected coord 3");

    coordSelectComboBox[0].addItem(QString("1"));
    coordSelectComboBox[0].addItem(QString("2"));
    coordSelectComboBox[0].addItem(QString("3"));
    coordSelectComboBox[0].addItem(QString("4"));
    coordSelectComboBox[0].addItem(QString("5"));
    coordSelectComboBox[0].addItem(QString("6"));
    connect(&coordSelectComboBox[0], SIGNAL(currentIndexChanged(int)), this, SLOT(coordSelectSlot()));

    coordSelectComboBox[1].addItem(QString("1"));
    coordSelectComboBox[1].addItem(QString("2"));
    coordSelectComboBox[1].addItem(QString("3"));
    coordSelectComboBox[1].addItem(QString("4"));
    coordSelectComboBox[1].addItem(QString("5"));
    coordSelectComboBox[1].addItem(QString("6"));
    coordSelectComboBox[1].setCurrentIndex(1);
    connect(&coordSelectComboBox[1], SIGNAL(currentIndexChanged(int)), this, SLOT(coordSelectSlot()));

    coordSelectComboBox[2].addItem(QString("1"));
    coordSelectComboBox[2].addItem(QString("2"));
    coordSelectComboBox[2].addItem(QString("3"));
    coordSelectComboBox[2].addItem(QString("4"));
    coordSelectComboBox[2].addItem(QString("5"));
    coordSelectComboBox[2].addItem(QString("6"));
    coordSelectComboBox[2].setCurrentIndex(2);
    connect(&coordSelectComboBox[2], SIGNAL(currentIndexChanged(int)), this, SLOT(coordSelectSlot()));

    gridLayout.addWidget(&coordSelectLabel[0], 1, 0, 1, 1);
    gridLayout.addWidget(&coordSelectComboBox[0], 1, 1, 1, 1);
    gridLayout.addWidget(&coordSelectLabel[1], 1, 2, 1, 1);
    gridLayout.addWidget(&coordSelectComboBox[1], 1, 3, 1, 1);
    gridLayout.addWidget(&coordSelectLabel[2], 1, 4, 1, 1);
    gridLayout.addWidget(&coordSelectComboBox[2], 1, 5, 1, 1);

    showGridCheckBox.setText("Grid");
    showGridCheckBox.setTristate(false);
    showGridCheckBox.setChecked(showGrid);
    connect(&showGridCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showGridSlot()));
    gridLayout.addWidget(&showGridCheckBox, 2, 0, 1, 1);

    showProjectedCheckBox.setText("Projected Space");
    showProjectedCheckBox.setTristate(false);
    showProjectedCheckBox.setChecked(showProjected);
    connect(&showProjectedCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showProjectedSpaceSlot()));
    gridLayout.addWidget(&showProjectedCheckBox, 2, 1, 1, 1);

    showNotRealisableCheckBox.setText("Not realisable");
    showNotRealisableCheckBox.setTristate(false);
    showNotRealisableCheckBox.setChecked(showNotRealisable);
    connect(&showNotRealisableCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showNotRealisableSlot()));
    gridLayout.addWidget(&showNotRealisableCheckBox, 3, 0, 1, 1);

    showCollisionCheckBox.setText("Collision");
    showCollisionCheckBox.setTristate(false);
    showCollisionCheckBox.setChecked(showCollision);
    connect(&showCollisionCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showCollisionSlot()));
    gridLayout.addWidget(&showCollisionCheckBox, 3, 1, 1, 1);

    showBadAngleCheckBox.setText("Bad angle");
    showBadAngleCheckBox.setTristate(false);
    showBadAngleCheckBox.setChecked(showBadAngle);
    connect(&showBadAngleCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showBadAngleSlot()));
    gridLayout.addWidget(&showBadAngleCheckBox, 3, 2, 1, 1);

    showValidCheckBox.setText("Valid");
    showValidCheckBox.setTristate(false);
    showValidCheckBox.setChecked(showValid);
    connect(&showValidCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showValidSlot()));
    gridLayout.addWidget(&showValidCheckBox, 3, 3, 1, 1);

    showOriginalCayleyCheckBox.setText("Original Cayley");
    showOriginalCayleyCheckBox.setTristate(false);
    showOriginalCayleyCheckBox.setChecked(showOriginalCayley);
    connect(&showOriginalCayleyCheckBox, SIGNAL(stateChanged(int)), this, SLOT(showOriginalCayleySlot()));
    gridLayout.addWidget(&showOriginalCayleyCheckBox, 3, 4, 1, 1);

    for (int i = 0; i < 8; i++)
    {
        showFlipCheckBoxes[i].setText("Flip " + QString::number(i));
        showFlipCheckBoxes[i].setTristate(false);
        showFlipCheckBoxes[i].setChecked(showFlip[i]);
        connect(&showFlipCheckBoxes[i], SIGNAL(stateChanged(int)), this,
                SLOT(showFlipSlot()));
        gridLayout.addWidget(&showFlipCheckBoxes[i], 4, i, 1, 1);
    }

    scalingSlider.setTickPosition(QSlider::NoTicks);
    scalingSlider.setValue(10);
    scalingSlider.setMaximum(20);
    scalingSlider.setMinimum(0);
    scalingSlider.setOrientation(Qt::Horizontal);
    connect(&scalingSlider, SIGNAL(valueChanged(int)), this, SLOT(scalingSlot()));
    gridLayout.addWidget(&scalingSlider, 0, 2, 1, 1);

    setLayout(&gridLayout);
    //[~MISC]re
}

void CartesianSpaceViewWidget::setData(Atlas *atlas, SaveLoader *saveLoader)
{
    this->atlas = atlas;
    this->saveLoader = saveLoader;
    dataWasSet = true;
}

void CartesianSpaceViewWidget::showCartesianSlot()
{
    this->showUCResult = this->showUCResultCheckBox.isChecked();
    if (this->showUCResult)
    {
        cout << "Showing Cartesian" << endl;
    }
    else
    {
        cout << "Showing Cayley" << endl;
    }
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::translationOnlySlot()
{
    this->translationOnly = this->translationOnlyCheckBox.isChecked();
    cout << "Translation only set to " << this->translationOnly << endl;
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::coordSelectSlot()
{
    for (int i = 0; i < 3; ++i)
    {
        selectedCoords[i] = coordSelectComboBox[i].currentIndex();
    }
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showGridSlot()
{
    this->showGrid = this->showGridCheckBox.isChecked();
    cout << "Show grid set to " << this->showGrid << endl;
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showProjectedSpaceSlot()
{
    this->showProjected = this->showProjectedCheckBox.isChecked();
    cout << "Show projected set to " << this->showProjected << endl;
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showNotRealisableSlot()
{
    if (this->showNotRealisableCheckBox.isChecked())
    {
        this->showNotRealisable = true;
    }
    else
    {
        this->showNotRealisable = false;
    }

    cout << "Show not realizable set to " << this->showNotRealisable << endl;

    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showCollisionSlot()
{
    this->showCollision = this->showCollisionCheckBox.isChecked();
    cout << "Show collision set to " << this->showCollision << endl;
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showBadAngleSlot()
{
    this->showBadAngle = this->showBadAngleCheckBox.isChecked();
    cout << "Show bad angle set to " << this->showBadAngle << endl;
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showValidSlot()
{
    this->showValid = this->showValidCheckBox.isChecked();
    cout << "Show valid set to " << this->showValid << endl;
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showOriginalCayleySlot()
{
    this->showOriginalCayley = this->showOriginalCayleyCheckBox.isChecked();
    cout << "Show original Cayley set to " << this->showOriginalCayley << endl;
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::showFlipSlot()
{
    for (int i = 0; i < 8; ++i)
    {
        this->showFlip[i] = this->showFlipCheckBoxes[i].isChecked();
    }

    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::scalingSlot()
{
    this->scalingFactor = this->scalingSlider.value() * .01; // TODO tweak this coeff here
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::initializeGL()
{
    renderer.initOpenGL();
    //[ INIT MESHES]
    cubeMesh.createAndBind("Cube.obj");
    cylinderMesh.createAndBind("12Cylinder.obj");
    sphereMesh.createAndBind("12by12UVsphere.obj");
    //[~INIT MESHES]
    generateTransformMatricesForAxis();
}

void CartesianSpaceViewWidget::paintGL()
{
    if (sharedData->atlasNodeWasSet)
    {
        updateAtlasNode();
        renderer.renderingProgram->setUniformValue("lightPositionVector",
                                                   camera.getEyeLocation() + camera.getRight() * 10 +
                                                   camera.getUp() * 30);
        renderer.renderingProgram->setUniformValue("cameraMatrix",
                                                   camera.getCameraMatrix(mapFromGlobal(cursor().pos())));

        if (mouseWasPressed)
        {
            updateCurrentCayleyPointIndex();
            mouseWasPressed = false;
        }
        else
        {
            renderer.bindRenderProgramAndClear();
            // Render corresponding Cartesian points
            renderer.renderWithTransperansy(&sphereMesh, translationMatricesPoints, rotationMatricesPoints,
                                            scaleMatricesPoints, colorVectorsPoints);
            // Render background cubes
            if (showGrid)
            {
                renderer.renderWithTransperansy(&cubeMesh, translationMatricesCubes, rotationMatricesCubes,
                                                scaleMatricesCubes, colorVectorsCubes);
            }
            // Render 3 cylinders that represent axis
            renderer.render(&cylinderMesh, translationMatricesAxis, rotationMatricesAxis, scaleMatricesAxis,
                            colorVectorsAxis);

            renderer.renderWithTransperansy(&sphereMesh, translationMatricesCentres, rotationMatricesCentres,
                                            scaleMatricesCentres, colorVectorsCentres);
        }
    }
}

void CartesianSpaceViewWidget::resizeGL(int w, int h)
{
    camera.resizeEvent(w, h);
}

void CartesianSpaceViewWidget::updateTransformationMatrices()
{
    bool analyseHypercube = false;
    if (analyseHypercube)
    {
        clearTransformationMatricesCentres();

        // NOTE switch cube here

        bool showCorrPoint = true;
        bool showCentre = true;

        // Cartesian
        if (this->showUCResult)
        {
            for (int i = 0; i < 729; ++i)
            {
                if (this->showGrid)
                {
                    // show Cayley facets
                    auto relativeCoord = Cartesian::unsignedDec2ter(i);
                    int dimCount = (relativeCoord[0] == 1) + (relativeCoord[1] == 1) + (relativeCoord[2] == 1) +
                                   (relativeCoord[3] == 1) + (relativeCoord[4] == 1) + (relativeCoord[5] == 1);
                    auto coord = cubeToAnalyse->allCartesianFacetCentresInCartesian[i];
                    if (!coord.empty())
                    {
                        QVector3D cubePosition(coord[selectedCoords[0]], coord[selectedCoords[1]],
                                               coord[selectedCoords[2]]);
                        QMatrix4x4 translationMatrix;
                        QMatrix4x4 rotationMatrix;
                        QMatrix4x4 scaleMatrix;
                        QVector4D colorVector;
                        translationMatrix.translate(cubePosition);
                        rotationMatrix.setToIdentity();
                        scaleMatrix.scale(scalingFactor);
                        // NOTE borrowing showFlip for show different dimension of points
                        if (dimCount == 0 && showFlip[0])
                        {
                            colorVector = QVector4D(0, 0, 0, 1);
                        }
                        else if (dimCount == 1 && showFlip[1])
                        {
                            colorVector = QVector4D(1, 0, 0, 1);
                        }
                        else if (dimCount == 2 && showFlip[2])
                        {
                            colorVector = QVector4D(1, 1, 0, 1);
                        }
                        else if (dimCount == 3 && showFlip[3])
                        {
                            colorVector = QVector4D(0, 1, 0, 1);
                        }
                        else if (dimCount == 4 && showFlip[4])
                        {
                            colorVector = QVector4D(0, 1, 1, 1);
                        }
                        else if (dimCount == 5 && showFlip[5])
                        {
                            colorVector = QVector4D(0, 0, 1, 1);
                        }
                        translationMatricesCentres.push_back(translationMatrix);
                        rotationMatricesCentres.push_back(rotationMatrix);
                        scaleMatricesCentres.push_back(scaleMatrix);
                        colorVectorsCentres.push_back(colorVector);
                    }
                }
                else
                {
                    // show Cayley facets
                    auto relativeCoord = Cartesian::unsignedDec2ter(i);
                    int dimCount = (relativeCoord[0] == 1) + (relativeCoord[1] == 1) + (relativeCoord[2] == 1) +
                                   (relativeCoord[3] == 1) + (relativeCoord[4] == 1) + (relativeCoord[5] == 1);
                    auto coord = cubeToAnalyse->allCayleyFacetCentresInCartesian[i];
                    if (!coord.empty())
                    {
                        QVector3D cubePosition(coord[selectedCoords[0]], coord[selectedCoords[1]],
                                               coord[selectedCoords[2]]);
                        QMatrix4x4 translationMatrix;
                        QMatrix4x4 rotationMatrix;
                        QMatrix4x4 scaleMatrix;
                        QVector4D colorVector;
                        translationMatrix.translate(cubePosition);
                        rotationMatrix.setToIdentity();
                        scaleMatrix.scale(scalingFactor);
                        if (dimCount == 0 && showFlip[0])
                        {
                            colorVector = QVector4D(0, 0, 0, 1);
                        }
                        if (dimCount == 1 && showFlip[1])
                        {
                            colorVector = QVector4D(1, 0.7, 0.7, 1);
                        }
                        else if (dimCount == 2 && showFlip[2])
                        {
                            colorVector = QVector4D(1, 1, 0.7, 1);
                        }
                        else if (dimCount == 3 && showFlip[3])
                        {
                            colorVector = QVector4D(0.7, 1, 0.7, 1);
                        }
                        else if (dimCount == 4 && showFlip[4])
                        {
                            colorVector = QVector4D(0.7, 1, 1, 1);
                        }
                        else if (dimCount == 5 && showFlip[5])
                        {
                            colorVector = QVector4D(0.7, 0.7, 1, 1);
                        }
                        translationMatricesCentres.push_back(translationMatrix);
                        rotationMatricesCentres.push_back(rotationMatrix);
                        scaleMatricesCentres.push_back(scaleMatrix);
                        colorVectorsCentres.push_back(colorVector);
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < 729; ++i)
            {
                if (this->showGrid)
                    // Show Cartesian analysis result
                {
                    auto relativeCoord = Cartesian::unsignedDec2ter(i);
                    int dimCount = (relativeCoord[0] == 1) + (relativeCoord[1] == 1) + (relativeCoord[2] == 1) +
                                   (relativeCoord[3] == 1) + (relativeCoord[4] == 1) + (relativeCoord[5] == 1);
                    auto coord = cubeToAnalyse->allCartesianFacetCentresInCayley[i];
                    QVector3D cubePosition(coord[selectedCoords[0]], coord[selectedCoords[1]],
                                           coord[selectedCoords[2]]);
                    QMatrix4x4 translationMatrix;
                    QMatrix4x4 rotationMatrix;
                    QMatrix4x4 scaleMatrix;
                    QVector4D colorVector;
                    translationMatrix.translate(cubePosition);
                    rotationMatrix.setToIdentity();
                    scaleMatrix.scale(scalingFactor);
                    if (dimCount == 0 && showFlip[0])
                    {
                        colorVector = QVector4D(0, 0, 0, 1);
                    }
                    else if (dimCount == 1 && showFlip[1])
                    {
                        colorVector = QVector4D(1, 0, 0, 1);
                    }
                    else if (dimCount == 2 && showFlip[2])
                    {
                        colorVector = QVector4D(1, 1, 0, 1);
                    }
                    else if (dimCount == 3 && showFlip[3])
                    {
                        colorVector = QVector4D(0, 1, 0, 1);
                    }
                    else if (dimCount == 4 && showFlip[4])
                    {
                        colorVector = QVector4D(0, 1, 1, 1);
                    }
                    else if (dimCount == 5 && showFlip[5])
                    {
                        colorVector = QVector4D(0, 0, 1, 1);
                    }
                    translationMatricesCentres.push_back(translationMatrix);
                    rotationMatricesCentres.push_back(rotationMatrix);
                    scaleMatricesCentres.push_back(scaleMatrix);
                    colorVectorsCentres.push_back(colorVector);
                }
                else
                    // Show Cayley analysis result
                {
                    auto relativeCoord = Cartesian::unsignedDec2ter(i);
                    int dimCount = (relativeCoord[0] == 1) + (relativeCoord[1] == 1) + (relativeCoord[2] == 1) +
                                   (relativeCoord[3] == 1) + (relativeCoord[4] == 1) + (relativeCoord[5] == 1);
                    auto coord = cubeToAnalyse->allCayleyFacetCentresInCayley[i];
                    QVector3D cubePosition(coord[selectedCoords[0]], coord[selectedCoords[1]],
                                           coord[selectedCoords[2]]);
                    QMatrix4x4 translationMatrix;
                    QMatrix4x4 rotationMatrix;
                    QMatrix4x4 scaleMatrix;
                    QVector4D colorVector;
                    translationMatrix.translate(cubePosition);
                    rotationMatrix.setToIdentity();
                    scaleMatrix.scale(scalingFactor);
                    if (dimCount == 0 && showFlip[0])
                    {
                        colorVector = QVector4D(0, 0, 0, 1);
                    }
                    if (dimCount == 1 && showFlip[1])
                    {
                        colorVector = QVector4D(1, 0.7, 0.7, 1);
                    }
                    else if (dimCount == 2 && showFlip[2])
                    {
                        colorVector = QVector4D(1, 1, 0.7, 1);
                    }
                    else if (dimCount == 3 && showFlip[3])
                    {
                        colorVector = QVector4D(0.7, 1, 0.7, 1);
                    }
                    else if (dimCount == 4 && showFlip[4])
                    {
                        colorVector = QVector4D(0.7, 1, 1, 1);
                    }
                    else if (dimCount == 5 && showFlip[5])
                    {
                        colorVector = QVector4D(0.7, 0.7, 1, 1);
                    }
                    translationMatricesCentres.push_back(translationMatrix);
                    rotationMatricesCentres.push_back(rotationMatrix);
                    scaleMatricesCentres.push_back(scaleMatrix);
                    colorVectorsCentres.push_back(colorVector);
                }

            }

            if (this->showFlip[7]) // show centre
            {
                auto coord = cubeToAnalyse->allCartesianFacetCentresInCayley[364];
                QVector3D cubePosition(coord[0], coord[1], coord[2]);
                QMatrix4x4 translationMatrix;
                QMatrix4x4 rotationMatrix;
                QMatrix4x4 scaleMatrix;
                QVector4D colorVector;
                translationMatrix.translate(cubePosition);
                rotationMatrix.setToIdentity();
                scaleMatrix.scale(scalingFactor);

                colorVector = QVector4D(1, 0.5, 1, 1);

                translationMatricesCentres.push_back(translationMatrix);
                rotationMatricesCentres.push_back(rotationMatrix);
                scaleMatricesCentres.push_back(scaleMatrix);
                colorVectorsCentres.push_back(colorVector);
            }
        }
    }
    else
    {
        clearTransformationMatricesPoints();
        clearTransformationMatricesCubes();
        if (showUCResult)
        {
            if (!allCartesianPointsToRender.empty())
            {
                if (translationOnly) // Cartesian translations only TODO fix later
                {/*
                    vector<vector<double>> allTranslationCubes[8]; // allTranslationCubes keeps track of all translation coords and status
                    for (auto &cube: allCartesianCubesToRender)
                    {
                        if (cube->status >= 2)
                        {
                            if (this->showFlip[cube->flipNum])
                            {
                                vector<double> curTranslationCube = {cube->centre->cartesianCoord[0],
                                                                     cube->centre->cartesianCoord[1],
                                                                     cube->centre->cartesianCoord[2],
                                                                     static_cast<double>(cube->status)};
                                bool found = false;
                                for (vector<double> &otherCube: allTranslationCubes[cube->flipNum])
                                {
                                    if ((abs(otherCube[0] - curTranslationCube[0]) < 1e-6) &&
                                        (abs(otherCube[1] - curTranslationCube[1]) < 1e-6) &&
                                        (abs(otherCube[2] - curTranslationCube[2]) < 1e-6) &&
                                        otherCube[3] >= curTranslationCube[3])
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    allTranslationCubes[cube->flipNum].push_back(curTranslationCube);
                                    bool renderCurrentNotRealisable = cube->status == 1 && showNotRealisable;
                                    bool renderCurrentCollision = cube->status == 2 && showCollision;
                                    bool renderCurrentBadAngle = cube->status == 3 && showBadAngle;
                                    bool renderCurrentValid = cube->status >= 4 && showValid;
                                    if (renderCurrentNotRealisable || renderCurrentCollision || renderCurrentBadAngle ||
                                        renderCurrentValid)
                                    {
                                        QVector3D coordPointPosition(cube->corrPoint->cartesianCoord[0],
                                                                     cube->corrPoint->cartesianCoord[1],
                                                                     cube->corrPoint->cartesianCoord[2]);
                                        QMatrix4x4 translationMatrix;
                                        QMatrix4x4 rotationMatrix;
                                        QMatrix4x4 scaleMatrix;
                                        QVector4D colorVector;
                                        translationMatrix.translate(coordPointPosition);
                                        rotationMatrix.setToIdentity();
                                        scaleMatrix.scale(cube->stepSize[0] * scalingFactor * 0.25,
                                                          cube->stepSize[1] * scalingFactor * 0.25,
                                                          cube->stepSize[2] * scalingFactor * 0.25);
                                        if (renderCurrentNotRealisable)
                                        {
                                            colorVector = QVector4D(0, 0, 1, 1); // blue
                                        }
                                        else if (renderCurrentCollision || renderCurrentBadAngle)
                                        {
                                            colorVector = QVector4D(1, 0, 0, 1); // red
                                        }
                                        else if (renderCurrentValid)
                                        {
                                            colorVector = QVector4D(0, 1, 0, 1); // green
                                        }
                                        translationMatricesPoints.push_back(translationMatrix);
                                        rotationMatricesPoints.push_back(rotationMatrix);
                                        scaleMatricesPoints.push_back(scaleMatrix);
                                        colorVectorsPoints.push_back(colorVector);
                                        QVector3D cubePosition(cube->centre->cartesianCoord[0],
                                                               cube->centre->cartesianCoord[1],
                                                               cube->centre->cartesianCoord[2]);
                                        QMatrix4x4 translationMatrixCube;
                                        QMatrix4x4 rotationMatrixCube;
                                        QMatrix4x4 scaleMatrixCube;
                                        QVector4D colorVectorCube;
                                        translationMatrixCube.translate(cubePosition);
                                        rotationMatrixCube.setToIdentity();
                                        scaleMatrixCube.scale(cube->stepSize[0] * 0.48, cube->stepSize[1] * 0.48,
                                                              cube->stepSize[2] * 0.48);
                                        if (renderCurrentNotRealisable)
                                        {
                                            colorVectorCube = QVector4D(0, 0, 1, cubeTransparencyStrength);
                                        }
                                        else if (renderCurrentCollision || renderCurrentBadAngle)
                                        {
                                            colorVectorCube = QVector4D(1, 0, 0, cubeTransparencyStrength);
                                        }
                                        else if (renderCurrentValid)
                                        {
                                            colorVectorCube = QVector4D(0, 1, 0, cubeTransparencyStrength);
                                        }
                                        translationMatricesCubes.push_back(translationMatrixCube);
                                        rotationMatricesCubes.push_back(rotationMatrixCube);
                                        scaleMatricesCubes.push_back(scaleMatrixCube);
                                        colorVectorsCubes.push_back(colorVectorCube);
                                    }
                                }
                            }
                        }
                    }
                */}
                else // Showing normal Cartesian sampling result
                {
                    if (this->showGrid) // show grid cubes and a point in it
                    {
                        for (auto &cube: this->allCartesianCubesToRender)
                        {
                            QVector3D cubePosition = QVector3D(
                                    cube->centre->cartesianCoord[this->selectedCoords[0]],
                                    cube->centre->cartesianCoord[this->selectedCoords[1]],
                                    cube->centre->cartesianCoord[this->selectedCoords[2]]);
                            QMatrix4x4 translationMatrixCube;
                            QMatrix4x4 rotationMatrixCube;
                            QMatrix4x4 scaleMatrixCube;
                            QVector4D colorVectorCube;
                            translationMatrixCube.translate(cubePosition);
                            rotationMatrixCube.setToIdentity();
                            // show boundaries between cubes
                            scaleMatrixCube.scale(cube->stepSize[selectedCoords[0]] * 0.48,
                                                  cube->stepSize[selectedCoords[1]] * 0.48,
                                                  cube->stepSize[selectedCoords[2]] * 0.48);

                            if (cube->status == 5 && showOriginalCayley)
                            {
                                colorVectorCube = this->originalCayleyColorVector;
                            }
                            else if (cube->status >= 4 && showValid)
                            {
                                colorVectorCube = this->validColorVector[cube->centre->flipNo];
                            }
                            else if (cube->status == 3 && showBadAngle)
                            {
                                colorVectorCube = this->badAngleColorVector;
                            }
                            else if (cube->status == 2 && showCollision)
                            {
                                colorVectorCube = this->collisionColorVector;
                            }
                            else if (cube->status == 1 && showNotRealisable)
                            {
                                colorVectorCube = this->notRealisableColorVector;
                            }
                            translationMatricesCubes.push_back(translationMatrixCube);
                            rotationMatricesCubes.push_back(rotationMatrixCube);
                            scaleMatricesCubes.push_back(scaleMatrixCube);
                            colorVectorsCubes.push_back(colorVectorCube);
                        }
                    }
                    else // show all sampled points
                    {
                        for (auto &point: this->allCartesianPointsToRender)
                        {
                            if (this->showFlip[point->flipNo])
                            {
                                QVector3D pointPosition;
                                if (showProjected) // show Cayley
                                {
                                    int dim = point->cayleyParameters.size();
                                    vector<double> coordParams = point->cayleyParameters;
                                    if (dim == 1)
                                    {
                                        pointPosition = QVector3D(coordParams[this->selectedCoords[0]], 0, 0);
                                    }
                                    else if (dim == 2)
                                    {
                                        pointPosition = QVector3D(coordParams[this->selectedCoords[0]],
                                                                  coordParams[this->selectedCoords[1]], 0);
                                    }
                                    else if (dim >= 3)
                                    {
                                        pointPosition = QVector3D(coordParams[this->selectedCoords[0]],
                                                                  coordParams[this->selectedCoords[1]],
                                                                  coordParams[this->selectedCoords[2]]);
                                    }
                                }
                                else // show Cartesian
                                {
                                    pointPosition = QVector3D(
                                            point->cartesianCoord[this->selectedCoords[0]],
                                            point->cartesianCoord[this->selectedCoords[1]],
                                            point->cartesianCoord[this->selectedCoords[2]]);
                                }
                                QMatrix4x4 translationMatrix;
                                QMatrix4x4 rotationMatrix;
                                QMatrix4x4 scaleMatrix;
                                QVector4D colorVector;
                                translationMatrix.translate(pointPosition);
                                rotationMatrix.setToIdentity();
                                scaleMatrix.scale(this->scalingFactor, this->scalingFactor, this->scalingFactor);

                                if (point->status == 5 && showOriginalCayley)
                                {
                                    colorVector = this->originalCayleyColorVector;
                                }
                                else if (point->status == 4 && showValid)
                                {
                                    colorVector = this->validColorVector[point->flipNo];
                                }
                                else if (point->status == 3 && showBadAngle)
                                {
                                    colorVector = this->badAngleColorVector;
                                }
                                else if (point->status == 2 && showCollision)
                                {
                                    colorVector = this->collisionColorVector;
                                }
                                else if (point->status == 1 && showNotRealisable)
                                {
                                    colorVector = this->notRealisableColorVector;
                                }
                                translationMatricesPoints.push_back(translationMatrix);
                                rotationMatricesPoints.push_back(rotationMatrix);
                                scaleMatricesPoints.push_back(scaleMatrix);
                                colorVectorsPoints.push_back(colorVector);
                            }
                        }
                    }
                }
            }
        }
        else // Showing result from regular EASAL sampling TODO need heavy check here!
        {
            if (!allCayleyToRender.empty())
            {
                Settings *sett = Settings::getInstance();
                vector<CayleyPoint *> projected = projectSpace(allCayleyToRender);
                // Show regular EASAL result in Cartesian
                if (showProjected)
                {
                    for (auto &point: projected)
                    {
                        for (int i = 0; i < 8; ++i)
                        {
                            if (showFlip[i])
                            {
                                if (point->hasOrientation(i))
                                {
                                    int numberOfOrientations = point->getOrientations().size();
                                    bool renderCurrentNotRealisable = !point->isRealizable() && showNotRealisable;
                                    bool renderCurrentCollision = point->getCollidN() > 0 && showCollision;
                                    bool renderCurrentBadAngle = point->getBadAngleN() > 0 && showBadAngle;
                                    bool renderCurrentValid = numberOfOrientations > 0 && showValid;
                                    if (renderCurrentNotRealisable || renderCurrentCollision ||
                                        renderCurrentBadAngle ||
                                        renderCurrentValid)
                                    {
                                        double cayleyParams[6] = {0, 0, 0, 0, 0, 0};
                                        point->getPoint(cayleyParams);
                                        QVector3D cubePosition(cayleyParams[this->selectedCoords[0]],
                                                               cayleyParams[this->selectedCoords[1]],
                                                               cayleyParams[this->selectedCoords[2]]);
                                        QMatrix4x4 translationMatrix;
                                        QMatrix4x4 rotationMatrix;
                                        QMatrix4x4 scaleMatrix;
                                        QVector4D colorVector;
                                        translationMatrix.translate(cubePosition);
                                        rotationMatrix.setToIdentity();
                                        scaleMatrix.scale(scalingFactor);
                                        if (renderCurrentNotRealisable)
                                        {
                                            colorVector = QVector4D(0, 0, 1, 1); // blue
                                        }
                                        else if (renderCurrentCollision || renderCurrentBadAngle)
                                        {
                                            colorVector = QVector4D(1, 0, 0, 1); // red
                                        }
                                        else if (renderCurrentValid)
                                        {
                                            colorVector = QVector4D(0, 1, 0, 1); // green
                                        }
                                        translationMatricesPoints.push_back(translationMatrix);
                                        rotationMatricesPoints.push_back(rotationMatrix);
                                        scaleMatricesPoints.push_back(scaleMatrix);
                                        colorVectorsPoints.push_back(colorVector);
                                    }
                                }
                            }
                        }
                    }
                }
                    // Show regular EASAL result in Cayley
                else
                {
                    for (auto &point: allCayleyToRender)
                    {
                        double cayleyParams[6] = {0, 0, 0, 0, 0, 0};
                        point->getPoint(cayleyParams);
                        QVector3D cubePosition(cayleyParams[this->selectedCoords[0]],
                                               cayleyParams[this->selectedCoords[1]],
                                               cayleyParams[this->selectedCoords[2]]);
                        QMatrix4x4 translationMatrix;
                        QMatrix4x4 rotationMatrix;
                        QMatrix4x4 scaleMatrix;
                        QVector4D colorVector;
                        translationMatrix.translate(cubePosition);
                        rotationMatrix.setToIdentity();
                        scaleMatrix.scale(scalingFactor);

                        bool showCurGreen = false;
                        bool showCurRed = false;
                        for (int i = 0; i < 8; ++i)
                        {
                            if (showFlip[i] && point->hasOrientation(i) && showValid)
                            {
                                showCurGreen = true;
                                break;
                            }
                            else if (showCollision || showBadAngle)
                            {
                                showCurRed = true;
                            }
                        }

                        if (showCurGreen)
                        {
                            colorVector = QVector4D(0, 1, 0, 1); // green
                        }
                        else if (showCurRed)
                        {
                            colorVector = QVector4D(1, 0, 0, 1); // red
                        }
                        translationMatricesPoints.push_back(translationMatrix);
                        rotationMatricesPoints.push_back(rotationMatrix);
                        scaleMatricesPoints.push_back(scaleMatrix);
                        colorVectorsPoints.push_back(colorVector);
                    }
                }
            }
        }
    }
}

void CartesianSpaceViewWidget::updateCurrentCayleyPointIndex()
{
    QOpenGLFramebufferObject fbo(this->size());
    fbo.setAttachment(QOpenGLFramebufferObject::Depth);
    fbo.bind();

    renderer.bindPickingProgramAndClear();
    // Render cubes that represent cayley points
    renderer.render(&cubeMesh, translationMatricesPoints, rotationMatricesPoints, scaleMatricesPoints,
                    colorVectorsPoints);

    fbo.release();
    updateCameraPosition();
}

void CartesianSpaceViewWidget::updateCameraPosition()
{
    if (curCube != nullptr)
    {
        QVector3D cubePosition(curCube->centre->cartesianCoord[selectedCoords[0]],
                               curCube->centre->cartesianCoord[selectedCoords[1]],
                               curCube->centre->cartesianCoord[selectedCoords[2]]);
        camera.setPosition(cubePosition);

    }
}

void CartesianSpaceViewWidget::updateAtlasNode()
{
    if (currentNodeID != sharedData->currentAtlasNode->getID())
    {
        setAtlasNode(sharedData->currentAtlasNode);
    }
}

void CartesianSpaceViewWidget::clearTransformationMatricesPoints()
{
    translationMatricesPoints.clear();
    rotationMatricesPoints.clear();
    scaleMatricesPoints.clear();
    colorVectorsPoints.clear();
}

void CartesianSpaceViewWidget::clearTransformationMatricesCubes()
{
    translationMatricesCubes.clear();
    rotationMatricesCubes.clear();
    scaleMatricesCubes.clear();
    colorVectorsCubes.clear();
}

void CartesianSpaceViewWidget::clearTransformationMatricesCentres()
{
    translationMatricesCentres.clear();
    rotationMatricesCentres.clear();
    scaleMatricesCentres.clear();
    colorVectorsCentres.clear();
}

void CartesianSpaceViewWidget::clearCartesian()
{
    this->allCartesianPointsToRender.clear();
    this->allCartesianCubesToRender.clear();
}

void
CartesianSpaceViewWidget::fillCartesian(vector<CartesianPoint *> points, vector<CartesianHypercube *> cubes)
{
    this->allCartesianPointsToRender = points;
    this->allCartesianCubesToRender = cubes;
}

void CartesianSpaceViewWidget::clearCayley()
{
    this->allCayleyToRender.clear();
}

void CartesianSpaceViewWidget::fillCayley(ActiveConstraintRegion *acr)
{
    this->allCayleyToRender.clear();
    for (auto &point: acr->space)
    {
        allCayleyToRender.push_back(point);
    }
    for (auto &point: acr->witspace)
    {
        allCayleyToRender.push_back(point);
    }
}

void CartesianSpaceViewWidget::setAtlasNode(AtlasNode *atlasNode)
{
    currentNodeID = atlasNode->getID();

    // Test if sampled file already exist by manually putting .txt file in place
    string currentFileName = "Driver2data/Node" + to_string(currentNodeID) + "_Cartesian.txt";
    if (FILE *file = fopen(currentFileName.c_str(), "r"))
    {
        fclose(file);
        atlasNode->uniformCartesianSampled = true;
    }

    Settings *sett = Settings::getInstance();
    Atlas *atlas = new Atlas();
    builder = new AtlasBuilder(sett->runTimeObjects.muA, sett->runTimeObjects.muB,
                               sett->runTimeObjects.save_loader, sett->runTimeObjects.df, atlas);
    ActiveConstraintGraph *acg = atlasNode->getCG();
    CayleyParameterization *cParam = new CayleyParameterization(acg, false);
    ConvexChart *cc = new ConvexChart(acg, false, cParam, sett->runTimeObjects.df);
    ActiveConstraintRegion *region = new ActiveConstraintRegion();

    vector<Orientation *> oriLists[8];
    this->saveLoader->loadNode(atlasNode->getID(), region);
    cc->initializeChart(true, region);
    auto space = region->getJustSpace();
    for (auto &cayleyPoint: space)
    {
        auto oris = cayleyPoint->getOrientations();
        for (auto &ori: oris)
        {
            oriLists[ori->getFlipNum()].emplace_back(ori);
        }
    }

    vector<double> distanceStepSize{sett->Sampling.cartesianSteps[0], sett->Sampling.cartesianSteps[1],
                                    sett->Sampling.cartesianSteps[2]};
    vector<int> angularStepNum{int(sett->Sampling.cartesianSteps[3]), int(sett->Sampling.cartesianSteps[4]),
                               int(sett->Sampling.cartesianSteps[5])};

    Orientation *firstOri;
    if (!oriLists[0].empty())
    {
        firstOri = oriLists[0][0];
    }
    else if (!oriLists[1].empty())
    {
        firstOri = oriLists[1][0];
    }
    else if (!oriLists[2].empty())
    {
        firstOri = oriLists[2][0];
    }
    else if (!oriLists[3].empty())
    {
        firstOri = oriLists[3][0];
    }
    else if (!oriLists[4].empty())
    {
        firstOri = oriLists[4][0];
    }
    else if (!oriLists[5].empty())
    {
        firstOri = oriLists[5][0];
    }
    else if (!oriLists[6].empty())
    {
        firstOri = oriLists[6][0];
    }
    else if (!oriLists[7].empty())
    {
        firstOri = oriLists[7][0];
    }
    else
    {
        cout << "No good orientation for current node." << endl;
        firstOri = nullptr;
    }

    this->sampler = new UniformCartesianSampler(atlasNode, acg, cc, sett->runTimeObjects.df, firstOri, distanceStepSize,
                                                angularStepNum, oriLists, this->saveLoader,
                                                sett->Sampling.cartesianIntersectionMode,
                                                sett->Sampling.cartesianSkipThres);
    if (!atlasNode->uniformCartesianSampled)
    {
        this->sampler->sampleAndAnalyseEachFlip();
        this->sampler->storeResultCubes(false);
    }

    atlasNode->uniformCartesianSampled = true;


    //[ GET NEW ACR]

    // Load all Cartesian cubes
    vector<CartesianPoint *> points;
    vector<CartesianHypercube *> cubes;
    saveLoader->loadCartesian(atlasNode->getID(), points, true, cubes);
    clearCartesian();
    fillCartesian(points, cubes);

    // Load all Cayley points
    auto acr = new ActiveConstraintRegion();
    saveLoader->loadNode(atlasNode->getID(), acr);
    clearCayley();
    fillCayley(acr);

    // NOTE switch cube here
    /*
    CartesianPoint *pt = allCartesianPointsToRender[cubeToAnalyseNum];
    this->cubeToAnalyse = new CartesianHypercube(pt, sampler->stepSize);
    sampler->analyseCube(cubeToAnalyse);
    cout << cubeToAnalyse->allCayleyFacetCentresInCayley[0][0] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[0][1] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[0][2] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[0][3] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[0][4] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[0][5] << endl;
    cout << cubeToAnalyse->allCayleyFacetCentresInCayley[364][0] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[364][1] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[364][2] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[364][3] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[364][4] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[364][5] << endl;
    cout << cubeToAnalyse->allCayleyFacetCentresInCayley[728][0] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[728][1] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[728][2] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[728][3] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[728][4] << ", "
         << cubeToAnalyse->allCayleyFacetCentresInCayley[728][5] << endl;
*/
    this->curCube = nullptr;

    updateCameraPosition();
    updateTransformationMatrices();
}

void CartesianSpaceViewWidget::keyPressEvent(QKeyEvent *keyEvent)
{
    camera.keyPressEvent(keyEvent);

    switch (keyEvent->key())
    {
        case Qt::Key_1:
            showValid = !showValid;
            if (showValid)
            {
                showBadAngle = false;
                showCollision = false;
            }
            updateTransformationMatrices();
            break;
        case Qt::Key_2:
            showBadAngle = !showBadAngle;
            if (showBadAngle)
            {
                showValid = false;
                showCollision = false;
            }
            updateTransformationMatrices();
            break;
        case Qt::Key_3:
            showCollision = !showCollision;
            if (showCollision)
            {
                showValid = false;
                showBadAngle = false;
            }
            updateTransformationMatrices();
            break;
        case Qt::Key_P:
            cout << "Pressed the project key. Cartesian Projection is set to " << projectOnCartesianSpace << endl;
            projectOnCartesianSpace = !projectOnCartesianSpace;
            projectionChanged = true;
            //updateAtlasNode();
            setAtlasNode(sharedData->currentAtlasNode);
            break;
        case Qt::Key_G:
            showGrid = !showGrid;
            cout << "Grid rendering is set to " << showGrid << endl;
            setAtlasNode(sharedData->currentAtlasNode);
            break;
    }
}

void CartesianSpaceViewWidget::mousePressEvent(QMouseEvent *mouseEvent)
{
    camera.mousePressEvent(mouseEvent);
    mouseWasPressed = true;
}

void CartesianSpaceViewWidget::mouseReleaseEvent(QMouseEvent *mouseEvent)
{
    camera.mouseReleaseEvent(mouseEvent);
}

void CartesianSpaceViewWidget::wheelEvent(QWheelEvent *wheelEvent)
{
    camera.wheelEvent(wheelEvent);
}

void CartesianSpaceViewWidget::generateTransformMatricesForAxis()
{
    for (int i = 0; i < 3; i++)
    {
        translationMatricesAxis.push_back(QMatrix4x4());
        rotationMatricesAxis.push_back(QMatrix4x4());
        scaleMatricesAxis.push_back(QMatrix4x4());
        colorVectorsAxis.push_back(QVector4D());
    }

    colorVectorsAxis[0] = QVector4D(1, 0, 0, 1);
    colorVectorsAxis[1] = QVector4D(0, 1, 0, 1);
    colorVectorsAxis[2] = QVector4D(0, 0, 1, 1);

    QVector3D start(0, 0, 0);
    QVector3D endX(10, 0.001, 0);
    QVector3D endY(0.001, 10, 0);
    QVector3D endZ(0, 0.001, 10);

    double axisRadius = 0.02;

    transformCylinderAccordingToData(
            start, endX, axisRadius, &translationMatricesAxis[0],
            &rotationMatricesAxis[0], &scaleMatricesAxis[0]);
    transformCylinderAccordingToData(
            start, endY, axisRadius, &translationMatricesAxis[1],
            &rotationMatricesAxis[1], &scaleMatricesAxis[1]);
    transformCylinderAccordingToData(
            start, endZ, axisRadius, &translationMatricesAxis[2],
            &rotationMatricesAxis[2], &scaleMatricesAxis[2]);
}

void CartesianSpaceViewWidget::transformCylinderAccordingToData(
        QVector3D positionStart, QVector3D positionEnd, float radious,
        QMatrix4x4 *translation, QMatrix4x4 *rotation, QMatrix4x4 *scale)
{
    //[ FIND ROTATION]
    QVector3D directionNotNormalized = QVector3D(positionEnd - positionStart);
    QVector3D direction = QVector3D(positionEnd - positionStart);
    QVector3D directionProjection = QVector3D(direction.x(), 0, direction.z());
    direction.normalize();
    QVector3D axisOfRotation =
            QVector3D::crossProduct(directionProjection, direction);
    float angle = acos(QVector3D::dotProduct(direction, directionProjection) /
                       (direction.length() * directionProjection.length()));
    angle = angle * 180 / PI + 90;
    //[~FIND ROTATION]
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();

    translation->setToIdentity();
    rotation->setToIdentity();
    scale->setToIdentity();

    translation->translate(positionStart + directionNotNormalized / 2);
    rotation->rotate(angle, axisOfRotation);
    scale->scale(
            QVector3D(radious, directionNotNormalized.length() / 2, radious));
}

void CartesianSpaceViewWidget::huetoColor(double hue, float color[3])
{
    double degHue = hue * 6.0;
    int h = int(degHue) % 6;
    double f = (degHue) - floor(degHue);
    double v = 1.0;
    double p = 0;
    double q = (1 - f);
    double t = (1 - (1 - f));
    switch (h)
    {
        case 0:
            color[0] = v;
            color[1] = t;
            color[2] = p;
            break;
        case 1:
            color[0] = q;
            color[1] = v;
            color[2] = p;
            break;
        case 2:
            color[0] = p;
            color[1] = v;
            color[2] = t;
            break;
        case 3:
            color[0] = p;
            color[1] = q;
            color[2] = v;
            break;
        case 4:
            color[0] = t;
            color[1] = p;
            color[2] = v;
            break;
        case 5:
            color[0] = v;
            color[1] = p;
            color[2] = q;
            break;
    }
}

// TODO call function from easalcore/UniformCartesianSampler here?
std::vector<CayleyPoint *> CartesianSpaceViewWidget::projectSpace(std::vector<CayleyPoint *> currentSpace)
{
    vector<CayleyPoint *> projectedSpace;

    Settings *sett = Settings::getInstance();
    vector<Atom *> helB = sett->runTimeObjects.muB->getAtoms();

    for (CayleyPoint *point: currentSpace)
    {
        vector<Orientation *> oris = point->getOrientations();
        for (Orientation *ori: oris)
        {/*
      vector<double> pos;
      double fb[3][3], tb[3][3];
      ori->getFromTo(fb, tb);

      Vector3d p1(fb[0][0], fb[0][1], fb[0][2]);
      Vector3d p2(fb[1][0], fb[1][1], fb[1][2]);
      Vector3d p3(fb[2][0], fb[2][1], fb[2][2]);

      Vector3d P1(tb[0][0], tb[0][1], tb[0][2]);
      Vector3d P2(tb[1][0], tb[1][1], tb[1][2]);
      Vector3d P3(tb[2][0], tb[2][1], tb[2][2]);

      Vector3d v1 = p2 - p1;
      Vector3d v2 = p3 - p1;
      Vector3d v3 = v1.cross(v2);

      Vector3d V1 = P2 - P1;
      Vector3d V2 = P3 = P1;
      Vector3d V3 = V1.cross(V2);

      Matrix3d m, M, R;
      m << v1(0), v2(0), v3(0), v1(1), v2(1), v3(1), v1(2), v2(2), v3(2);
      M << V1(0), V2(0), V3(0), V1(1), V2(1), V3(1), V1(2), V2(2), V3(2);

      R = M * m.inverse();
      Vector3d t = P1 - R * p1;

      Quaterniond q(R);

      // compute mean
      Vector3d mean(0, 0, 0);
      for (Atom *atom : helB) {
        double *l = atom->getLocation();
        Vector3d p(l[0], l[1], l[2]);
        mean += R * p;
      }
      mean = mean / helB.size();

      Vector3d TB = t + mean;
      Matrix3d RB = quaternionToRotation(q.w(), q.x(), q.y(), q.z());

      Vector3d eaB = Utils::RotMatToEuler(R);
      eaB[0] = eaB[0] * 180 / PI;
      eaB[2] = eaB[2] * 180 / PI;

      //ActiveConstraintGraph *acg = this->atlasNode->getCG();

//      if (acg->independent_directions.size() > 0) {
//        for (vector<int>::iterator iter = acg->independent_directions.begin();
//             iter != acg->independent_directions.end(); iter++) {
//          if ((*iter) < 3) {
//            pos.push_back(TB(*iter));
//          } else {
//            pos.push_back(eaB(*iter - 3));
//          }
//        }
//      } else
      {
        pos.push_back(TB(0));
        pos.push_back(TB(1));
        pos.push_back(TB(2));
        //pos.push_back(eaB(0));
        //pos.push_back(eaB(1));
        //pos.push_back(eaB(2));
        pos.push_back(0);
        pos.push_back(0);
        pos.push_back(0);//TODO change all angles to 0
      }
*/
            double x, y, z, phi, theta, psi;
            double from[3][3], to[3][3];
            ori->getFromTo(from, to);
            Vector3d f[3], t[3]; // turn from and to to vector in Eigen
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    f[i](j) = from[i][j];
                    t[i](j) = to[i][j];
                }
            }

            // Calculate rotation
            Vector3d vf0 = f[1] - f[0];
            Vector3d vf1 = f[2] - f[0];
            Vector3d vf2 = vf0.cross(vf1);
            Vector3d vt0 = t[1] - t[0];
            Vector3d vt1 = t[2] - t[0];
            Vector3d vt2 = vt0.cross(vt1);

            Matrix3d Mf, Mt;

            Mf << vf0(0), vf1(0), vf2(0), vf0(1), vf1(1), vf2(1), vf0(2), vf1(2), vf2(2);
            Mt << vt0(0), vt1(0), vt2(0), vt0(1), vt1(1), vt2(1), vt0(2), vt1(2), vt2(2);

            Matrix3d R = Mt * Mf.inverse();

            phi = atan2(R(0, 1), R(0, 0));
            theta = asin(-R(0, 2)); // TODO handle potential NaN due to inaccuracy
            psi = atan2(R(1, 2), R(2, 2));

            // Calculate translation
            for (auto &i: f)
            {
                i = R * i;
            }

            x = t[0](0) - f[0](0);
            y = t[0](1) - f[0](1);
            z = t[0](2) - f[0](2);
            vector<double> pos = {x, y, z, 0, 0, 0};
            CayleyPoint *output = new CayleyPoint(pos);
            output->setRealizable(point->isRealizable());
            output->setBadAngleN(point->getBadAngleN());
            output->setCollidN(point->getCollidN());
            output->axis = point->axis;
            output->zIndex = point->zIndex;
            output->setOrientations(point->getOrientations());

            projectedSpace.push_back(output);
        }
    }

    return projectedSpace;
}

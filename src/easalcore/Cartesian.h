/*
 This file is part of EASAL.

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <boost/math/special_functions/binomial.hpp>
#include <queue>
#include <unordered_set>
#include <vector>

#include "ActiveConstraintGraph.h"
#include "ConvexChart.h"

const int FACTORIAL[7] = {1, 1, 2, 6, 24, 120, 720};
const int POWER_OF_TWO[6] = {1, 2, 4, 8, 16, 32};
const double TOL_EQUAL = 1e-3; // used in checking if point is in hypercube

using namespace std;

class vector_double_eq
{
public:
    bool operator()(const vector<double> &v0, const vector<double> &v1) const;
};

class vector_double_hash
{
public:
    size_t operator()(const vector<double> &v) const;
};

class vector_int_eq
{
public:
    bool operator()(const vector<int> &v0, const vector<int> &v1) const;
};

class vector_int_hash
{
public:
    size_t operator()(const vector<int> &v) const;
};

class CartesianPoint
{
public:
    explicit CartesianPoint(vector<double> coord); // init from 6 parameters

    explicit CartesianPoint(CartesianPoint *copy); // copy init
    ~CartesianPoint();

    void printPoint(bool formatted);

    vector<double> cartesianCoord;
    vector<double> cayleyDistances;
    vector<double> cayleyParameters; // distances for constant(set) distance, parameters for variable

    size_t flipNo;
    // -1: not sampled. 0: waiting to be sampled. 1: violate triangular/tetrahedral ineq. 2: violate main helix angle
    // 3: collision. 4: feasible. 5: feasible, created by previous sampling, used as starting point
    int status;
};

class CartesianSimplex
{
public:
    CartesianSimplex(int _dim, vector<CartesianPoint *> _vertices);

    ~CartesianSimplex();

    int dim;
    vector<CartesianPoint *> vertices;
};

// Store hypercubes and facets in Cartesian space.
class CartesianHypercube
{
public:
    CartesianHypercube(CartesianPoint *centre, vector<double> stepSize);

    explicit CartesianHypercube(vector<int> centreRelCoord);

    explicit CartesianHypercube(CartesianHypercube *copy);

    ~CartesianHypercube();

    // Get certain vertex of a hypercube
    // 6 directions correspond to x, y, z, alpha, beta, gamma
    // -1 for lower coord, 0 for mid, and 1 for higher coord
    CartesianPoint *getPoint(vector<int> directions);

    // Generate all Cartesian facets of hypercube with given dimension
    vector<vector<int>> getAllFacetRelCoords(int resultDim);

    vector<vector<vector<int>>> getAllSimplexRelCoords();

    // Decompose cube to segments
    // fiveDimCentre true: segments of vertex - 5-dim face centre
    // false: segments of edges of cube
    vector<vector<vector<int>>> getAllSegmentRelCoordsFrom5dFace();

    // Break each facet in allFacetsRelativeCoord into simplices
    // Store in allSimplices
    vector<CartesianSimplex *> getAllSimplices();

    // Return true if Cartesian point pt is in this Cartesian hypercube
    bool isPointIn(vector<double> pt);

    // Return neighbouring cube on given direction
    // 0 to 11 correspond to x, y, z, alpha, beta, gamma, -x, -y, -z, -alpha, -beta, -gamma
    CartesianHypercube *getNeighbour(int direction);

    vector<int> relCoord;
    vector<double> origin;
    int dim;
    int flipNum;
    // 0: no intersection (blue), 1: intersection not within region, 2: collision, 3: bad angle, 4: feasible
    size_t status;

    CartesianPoint *centre; // centre of hypercube in Cartesian space
    vector<double> stepSize; // size of cube on each Cartesian direction

    vector<vector<double>> foundValidCartesianPoints;
    vector<vector<double>> foundBadAngleCartesianPoints;
    vector<vector<double>> foundCollisionCartesianPoints;
    vector<vector<double>> foundNotRealizableCartesianPoints;

    vector<double> foundCayleyParametersForParaMethod;
    vector<double> foundCartesianIntersectionForParaMethod;

    vector<vector<double>> allCartesianFacetCentresInCartesian;
    vector<vector<double>>
            allCartesianFacetCentresInCayley; // all facet centres calculated by mapping Cartesian midpoints to Cayley
    vector<vector<double>> allCayleyFacetCentresInCayley; // all facet centres calculated by Cayley midpoints
    vector<vector<double>> allCayleyFacetCentresInCartesian;

    // for basis method

    // Get centre of 5-dim facet
    // coordAxis: direction of facet (0-5 corresponding x, y, z, \alpha, \beta, \gamma
    // direction: false for lower coord and true for higher coord
    CartesianPoint *get5dFacetCentre(int coordAxis, bool direction);

    Eigen::MatrixXd cayleyParaBasisDistance; // set of basis vectors of corresponding Cayley hyper-parallelepiped
    Eigen::MatrixXd cayleyParaBasisParameter;
};

class Cartesian
{
public:
    // Return true if cpA and cpB are the same Cayley point
    static bool isEqual(vector<double> cpA, vector<double> cpB);

    static bool
    isIn(const vector<double> &cp, const unordered_set<vector<double>, vector_double_hash, vector_double_eq> &cps);

    // Return true if Cayley point is in the set of Cayley points
    static bool
    isIn(const vector<int> &cp, const unordered_set<vector<int>, vector_int_hash, vector_int_eq> &cps);

    // Return true if Cartesian hypercube is in the set of Cartesian hypercubes
    static bool isIn(CartesianHypercube *cube,
                     const unordered_set<CartesianHypercube *, vector_double_hash, vector_double_eq> &cubes);

    // Calculate translational and angular distance between 2 Cartesian points
    static tuple<double, double> cartesianDist(CartesianPoint *p1, CartesianPoint *p2);

    static vector<double> calcPointFromRelCoord(vector<double> origin, vector<double> stepSize, vector<int> relCoord);

    // Input a number, return corresponding tertiary number. Used for listing all possible facets of a hypercube
    static vector<int> signedDec2ter(int dec);

    //
    static vector<int> unsignedDec2ter(int dec);

    // Input a number dec and a vector, return given vector's dec'th permutation according to dict order.
    // Used for breaking facet into simplices
    static vector<int> dec2perm(int dec, const vector<int> &toPerm);

    static vector<double> getMidPoint(vector<double> pt1, vector<double> pt2);

    static vector<vector<int>> calcCubesFromFacet(vector<int> facetRelCoord);
};

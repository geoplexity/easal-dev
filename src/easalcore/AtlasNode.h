/*
 This file is part of EASAL.

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATLASNODE_H_
#define ATLASNODE_H_

#include <cstddef>
#include <queue>
#include <unordered_set>
#include <utility>
#include <vector>
#include <memory>

#include "ActiveConstraintGraph.h"
#include "ActiveConstraintRegion.h"
#include "Cartesian.h"
#include "EventPointNode.h"

/**
 * Node in the Atlas, each represents an active
 * constraint region labeled by ActiveConstraintGraph
 */

#ifdef CAF
#include "caf/all.hpp"
// For CAF message passing
struct AtlasNodeStruct {
  int numID;
  int firstParentID;
  bool complete;
  bool noGoodOrientation;
  bool dimWrittenSample;
  bool dimWrittenWitness;
  bool dimWrittenEPTable;
  int dim;
  std::vector<int> connections;
  std::vector<std::pair<int, int> > cg;
  ActiveConstraintGraph acg;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, AtlasNodeStruct& x) {
  return f(caf::meta::type_name("AtlasNodeStruct"), x.numID, x.firstParentID,
           x.complete, x.noGoodOrientation, x.dim, x.connections, x.cg);
}
#endif

class Atlas;

class AtlasNode
{
public:
    /////////////////////////////////
    // Constructors/Destructors
    /////////////////////////////////

    /** @brief Default constructor, gives basic physical values. */
    AtlasNode();

#ifdef CAF
    AtlasNode(AtlasNodeStruct);
    AtlasNodeStruct getAtlasNodeStruct();
#endif

    /** @brief Constructor that allows initialization of a broad range of values
     */
    AtlasNode(int ID, int fParentID, bool complete, bool empty, int numdim,
              std::vector<int> connection);

    /** @brief Destructor that deletes ACG and ACR of instance. */
    virtual ~AtlasNode();

    /////////////////////////////////
    // Connections
    /////////////////////////////////

    /**
     * @brief Put a link between this node and the other.
     */
    void addConnection(int other);

    bool removeConnection(int other);

    /**
     * @return True if there is an edge/link between this node and the other,
     * False otherwise.
     */
    bool isConnectedTo(int other);

    /** @return The set of indices of the nodes that this node is connected to */
    std::vector<int> getConnection();

    void setConnection(std::vector<int>);

    /* This function will be used to get all flips in witness points. This will
     * mainly be used for pathfinding*/
    void getWitnessFlips(std::vector<int> &v);

    /////////////////////////////////
    // Others
    /////////////////////////////////
    /** @return The Index of the node in the atlas */
    int getID() const;

    void setID(int id);

    /** @return The Index of the firstParent that discovered this node */
    int getFirstParentID() const;

    /** @return Dimension which is 6 - number_of_contacts */
    int getDim();

    void setDim(int dim);

    /** @return Sampling dimension. paramdim can be greater than dim in case of
     * short_range_sampling */
    int getParamDim();

    /**
     * @param complete The boolean whether sampling of the node's region is
     * completed or not
     */
    void setComplete(bool complete);

    /**
     * @return True if the sampling is finished, False otherwise.
     */
    bool isComplete();

    /** @return True if there is at least one accepted Orientation, False
     * otherwise */
    bool hasAnyGoodOrientation();

    void setFoundGoodOrientation(bool s);

    /**
     * @return The ActiveConstraintGraph of the atlas node
     */
    ActiveConstraintGraph *getCG();

    void setCG(ActiveConstraintGraph *newID);

    /**
     * @return The ActiveConstraintRegion of the atlas node
     */
    ActiveConstraintRegion *getACR();

    void setACR(ActiveConstraintRegion *region);

    /** @brief Cleans the ActiveConstraintRegion and ActiveConstraintGraph of the
     * node */
    void trimNode();

    void setFlipSpace(vector<vector<pair<CayleyPoint *, int> *> *>);

    vector<vector<pair<CayleyPoint *, int> *> *> getFlipSpace();

    void setEventPointForest(vector<EventPointNode *>);

    vector<pair<CayleyPoint *, int> *> getAllEntryPoints();

    void setAllEntryPoints(vector<pair<CayleyPoint *, int> *>);

    /** searches through the CC of entry point 1, stops when component is complete
     * or ep_2 is found */
    std::vector<std::pair<CayleyPoint *, int> *> findContinuousPath(
            CayleyPoint *point_1, CayleyPoint *point_2, int flip_1, int flip_2,
            Atlas *atlas);

    /** finds the EventPointNode in all_ep
     */
    EventPointNode *findCorrespondingEP(CayleyPoint *point, int flip);

    /** returns true if ep_1 and ep_2 are in the same connected component
     */
    bool connectedCheck(EventPointNode *ep_1, EventPointNode *ep_2);

    bool buildDFStree(int treeNum, EventPointNode **dst, EventPointNode *src,
                      vector<vector<pair<CayleyPoint *, int> *> *> flipSpace);

    void printEventForest() const;

    void printTree(const EventPointNode *current) const;

    double findL2Distance(MolecularUnit *MuA, MolecularUnit *MuB, Orientation *o1,
                          Orientation *o2);

    std::vector<std::pair<CayleyPoint *, int> *> findTreePath(
            EventPointNode *src, EventPointNode *dst,
            vector<vector<pair<CayleyPoint *, int> *> *> flipSpace, int flip_1,
            int flip_2);

    bool findEntryPoint(AtlasNode *AtlasNode_0D, int flip,
                        std::pair<CayleyPoint *, int> &);

    /** For a 0D child region and a child flip, find a set of entryPoints from
     *  that region and flip into this AtlasNode
     */
    bool findEntryPoint(int child_id, int child_flip,
                        std::vector<std::pair<std::unique_ptr<CayleyPoint>, int>> &eps);

    std::vector<CayleyPoint *> sortSpace(ActiveConstraintRegion *region);

    void splitSpace(std::vector<CayleyPoint *>);

    int searchSpace(
            EventPointNode *point,
            std::vector<std::vector<std::pair<CayleyPoint *, int> *> *> flipSpace);

    /** Is given point in region a bifurcation*/
    vector<pair<int, int> *> checkBifurcation(
            std::pair<CayleyPoint *, int> *, int index,
            vector<vector<pair<CayleyPoint *, int> *> *> flipSpace);

    void testBifurcation(int flip);

    /** Is given point in region an entry point */
    bool checkEntryPoint(std::pair<CayleyPoint *, int> *point);

    bool checkEntryPoint(CayleyPoint *point, int flip);

    /** Given two EPN's that are connected, returns the path denoted by that edge
     */
    vector<pair<CayleyPoint *, int> *> findEdgePath(
            EventPointNode *, EventPointNode *,
            vector<vector<pair<CayleyPoint *, int> *> *> flipSpace, int flip_1,
            int flip_2);

    void printFlip(int y);

    void setFlipScheme(std::vector<std::vector<int> > flipScheme);

    std::vector<std::vector<int> > getFlipScheme();

    bool convertTagsToPointers(Orientation *ori,
                               ActiveConstraintRegion *parentACR);

    /**
     * Check the return status before using the bool value
     **/
    int isPartial3Tree(bool &);

    /** Adds a new entry point to the entryPointTable*/
    void addToEntryPointTable(const int &, const int &, const int &, const int &);

    /** Checks if a given witness point - entry point pair exists.
     *  I.e. checks if a given (child_id, child_flip, entry_point_flip)
     *  is stored in entryPointTable. */
    bool entryPointExists(const int &, const int &, const int &) const;

    /** Checks if current node has certain pairs as participants
     * True if input participants and this node's participants match
     * */
    bool checkNodeByParticipants(vector<pair<int, int>> participantsToCheck);

    /** Checks if current node is offspring of node with certain pairs as participants
     * True if current node is target node's offspring, i.e. current node's participants covers all entries of the input vector
     * */
    bool checkOffspringByParticipants(vector<pair<int, int>> participantsToCheck);

    /** Checks if current node is ancestor of node with certain pairs as participants
     * True if current node is target node's ancestor, i.e. all current node's participants are provided in the input vector
     * */
    bool checkAncestorByParticipants(vector<pair<int, int>> participantsToCheck);


    /** Hasher for std::pair type keys. */
    struct hash_pair
    {
        template<class T1, class T2>
        size_t operator()(const pair<T1, T2> &p) const
        {
            auto hash1 = hash<T1>{}(p.first);
            auto hash2 = hash<T2>{}(p.second);
            return hash1 ^ hash2;
        }
    };
    /////////////////////////////////
    // Public variables
    /////////////////////////////////

    std::vector<vector<int> > flipScheme;

    /**All root EventPointNodes*/
    vector<EventPointNode *> EventPointForest;

    /**true if tree is completely explored, false if else  */
    vector<bool> treeCompleted;

    vector<EventPointNode *> allEventPoints;
    unordered_map<string, EventPointNode *> allEventPointsHash;

    vector<pair<CayleyPoint *, int> *> allEntryPoints;

    /** to keep track of this node visited or not through some search and analysis
     * criteria used by Statistic class*/
    bool visited;

    /*to keep track of dimension is written to the node.txt file or not*/
    bool dimWrittenSample;
    bool dimWrittenWitness;
    bool dimWrittenEPTable;

    bool uniformCartesianSampled;

private:
    vector<vector<pair<CayleyPoint *, int> *> *> flipSpace;
    /** The Index of the node in the atlas */
    int numID;

    /** The Index of the first parent that discovers this node */
    int firstParentID;

    /** Dimension which is 6 - number_of_contacts */
    int dim;  // dim should not be paramDim since paramDim can be same for all
    // nodes in 6d sampling (i.e. paramdim=6 for n=2 molecules) for all
    // nodes, in case of short-distance sampling hence will prevent
    // distinguishing the display of nodes.

    /** True if the sampling is finished, False otherwise. */
    bool complete;

    /** True if the node is convexifiable */
    bool partial3tree;

    /**
     * True if there is no accepted Orientation, False if there is one or more
     * accepted Orientations if True, then do not display the node
     */
    bool noGoodOrientation;

    /** The set of IDs of the nodes that this node is connected to */
    std::vector<int> connection;

    /** The active constraint graph that labels the node. */
    ActiveConstraintGraph *constraintGraph;

    /** The set of Cayley points in the active region. */
    ActiveConstraintRegion *region;

    list<pair<AtlasNode *, Orientation *> > ListOfChildNodes;
    list<pair<CayleyPoint *, Orientation *> > ListOfVisitedPoints;
    unordered_set<string> ChildNodeWitnessSet;

    /** Stores lookup table (multi-map) from child_id to a tuple of {child_flip,
     * entry_point_id, parent_flip} */
    std::unique_ptr<std::unordered_map<std::pair<int, int>, unordered_map<int, int>, hash_pair>> entryPointTable;

public:
    // int numPass;
    // For mark - Yichi
    // True if already done marking
    bool isMarked;

    vector<CartesianHypercube *> cartesianRegion[8]; // TODO check if needed, if no then remove


    list<pair<AtlasNode *, Orientation *> > getListOfChildNodes();

    void pushBackChildNode(AtlasNode *node, Orientation *ori);

    void clearListOfChildNodes();

    list<pair<CayleyPoint *, Orientation *> > getListOfVisitedPoints();

    void pushBackVisitedPoint(CayleyPoint *point, Orientation *ori);

    void clearListOfVisitedPoints();

    bool insertIntoChildNodeWitnessSet(int, int);

    std::unordered_map<std::pair<int, int>, unordered_map<int, int>, AtlasNode::hash_pair> *getEntryPointTable();
};

#endif /* ROADNODE_H_ */

/*
 This file is part of EASAL.

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * UniformCartesianSampler.cpp
 *
 *  Created on: 2020
 *      Author: Yichi Zhang
 */

#include "UniformCartesianSampler.h"

#include <utility>

#define PI 3.14159265
#define TOL_LAMBDA .1
#define TOL_FLIP_CHECK 0

const int POW3[6] = {243, 81, 27, 9, 3, 1};

UniformCartesianSampler::UniformCartesianSampler(AtlasNode *node, ActiveConstraintGraph *acg, ConvexChart *cc,
                                                 PredefinedInteractions *df, Orientation *originOri,
                                                 vector<double> distanceStepSize, vector<int> angularStepNum,
                                                 vector<Orientation *> startingOriLists[8], SaveLoader *snl, int mode,
                                                 int skipThreshold) : verbose(false)
{
    cout << "Initialising UC sampler." << endl;
    this->node = node;
    this->acg = acg;
    this->cc = cc;
    this->df = df;

    for (int i = 0; i < 3; ++i)
    {
        this->angularStepCount[i] = angularStepNum[i];
    }
    this->stepSize = vector<double>(6, 0);
    for (int i = 0; i < 3; ++i)
    {
        this->stepSize[i] = distanceStepSize[i];
        this->stepSize[i + 3] = 2 * PI / angularStepNum[i];
    }
    this->snl = snl;
    if (originOri == nullptr)
    {
        cout << "Warning: bad origin orientation." << endl;
        this->origin = vector<double>(6, 0);
    }
    else
    {
        this->origin = ori2Cartesian(originOri);
        for (int i = 0; i < 8; ++i)
        {
            if (!startingOriLists[i].empty())
            {
                this->startingOriLists[i] = startingOriLists[i];
            }
            else
            {
                this->startingOriLists[i] = vector<Orientation *>();
            }
        }
    }

    this->curFlipNo = -1;
    this->curCube = nullptr;
    this->mode = mode;
    this->skipThreshold = skipThreshold;

    this->savePointCounter = 0;
    this->savePointFreq = 10000;
}

void UniformCartesianSampler::initFlip(int flipNo)
{
    cout << "Initialising flip " << flipNo << endl;
    this->curFlipNo = flipNo;
    this->originalPointCount[flipNo] = 0;
    this->validPointCount[flipNo] = 0;
    this->badAnglePointCount[flipNo] = 0;
    this->collisionPointCount[flipNo] = 0;
    this->sampledValidCubeCount[flipNo] = 0;
    this->sampledBadAngleCubeCount[flipNo] = 0;
    this->sampledCollisionCubeCount[flipNo] = 0;
    this->sampledNotRealizableCubeCount[flipNo] = 0;
    this->sampledNoIntersectionCubeCount[flipNo] = 0;

    this->mappedValidCubeCount[flipNo] = 0;
    this->mappedBadAngleCubeCount[flipNo] = 0;
    this->mappedCollisionCubeCount[flipNo] = 0;
    this->mappedNotRealizableCubeCount[flipNo] = 0;

    this->visitedCubeCount[flipNo] = 0;
    this->visitedFacetCount[flipNo] = 0;
    this->visitedSimplexCount[flipNo] = 0;
    this->pointCount[flipNo] = 0;
    for (auto &ori: startingOriLists[flipNo])
    {
        auto cartesianCoord = ori2Cartesian(ori);
        auto cartesianPoint = new CartesianPoint(cartesianCoord);
        cartesianPoint->status = 5;
        cartesian2Cayley(cartesianPoint);
        cartesianPoint->flipNo = ori->getFlipNum();
        ++this->originalPointCount[flipNo];
        this->pointBuffer.push_back(cartesianPoint);
        storeMapAndClearPointBuffer();
    }
}

vector<double> UniformCartesianSampler::ori2Cartesian(Orientation *ori)
{
    double x, y, z, alpha, beta, gamma; // alpha: rotation on z axis; beta: on y axis; gamma: on x axis
    double from[3][3], to[3][3];
    if (ori == nullptr)
    {
        cout << "error: converting null orientation to Cartesian" << endl;
        throw;
    }
    ori->getFromTo(from, to);
    Vector3d f[3], t[3]; // turn "from-to" to vector in Eigen
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            f[i](j) = from[i][j];
            t[i](j) = to[i][j];
        }
    }

    // Calculate rotation
    Vector3d vf0 = f[1] - f[0];
    Vector3d vf1 = f[2] - f[0];
    Vector3d vf2 = vf0.cross(vf1);
    Vector3d vt0 = t[1] - t[0];
    Vector3d vt1 = t[2] - t[0];
    Vector3d vt2 = vt0.cross(vt1);

    Matrix3d Mf, Mt;

    Mf << vf0(0), vf1(0), vf2(0), vf0(1), vf1(1), vf2(1), vf0(2), vf1(2), vf2(2);
    Mt << vt0(0), vt1(0), vt2(0), vt0(1), vt1(1), vt2(1), vt0(2), vt1(2), vt2(2);

    Matrix3d R = Mt * Mf.inverse();
    // Fix potential NaN due to numerical inaccuracy
    if (R(0, 2) > 1)
    {
        R(0, 2) = 1;
    }
    if (R(0, 2) < -1)
    {
        R(0, 2) = -1;
    }

    alpha = atan2(R(0, 1), R(0, 0));
    beta = asin(-R(0, 2));
    gamma = atan2(R(1, 2), R(2, 2));

    // Calculate translation
    for (auto &i: f)
    {
        i = R * i;
    }

    x = t[0](0) - f[0](0);
    y = t[0](1) - f[0](1);
    z = t[0](2) - f[0](2);
    vector<double> pt = {x, y, z, alpha, beta, gamma};
    return pt;
}

void UniformCartesianSampler::cartesian2Cayley(CartesianPoint *cp) const
{
    cp->cayleyDistances.clear();
    cp->cayleyParameters.clear();

    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    vector<pair<int, int>> constraints = acg->getParticipants();
    vector<pair<int, int>> parameters = acg->getParamLines();

    // Calculate Cartesian location of each atom in 2 molecules.
    vector<tuple<double, double, double>> posA, posB;

    // Atoms in molecule A remains unchanged.
    vector<Atom *> atomA = molA->getAtoms();
    auto atomNumA = atomA.size();
    for (int i = 0; i < atomNumA; ++i)
    {
        tuple<double, double, double> curPos(atomA[i]->getLocation()[0], atomA[i]->getLocation()[1],
                                             atomA[i]->getLocation()[2]);
        posA.push_back(curPos);
    }

    // Calculate position of atoms in molecule B.
    // Translation
    Vector3d transVet(cp->cartesianCoord[0], cp->cartesianCoord[1], cp->cartesianCoord[2]);
    // Rotation
    Matrix3d rotMat;
    rotMat(0, 0) = cos(cp->cartesianCoord[3]) * cos(cp->cartesianCoord[4]);
    rotMat(0, 1) = sin(cp->cartesianCoord[3]) * cos(cp->cartesianCoord[4]);
    rotMat(0, 2) = -sin(cp->cartesianCoord[4]);
    rotMat(1, 0) = cos(cp->cartesianCoord[3]) * sin(cp->cartesianCoord[4]) * sin(cp->cartesianCoord[5])
                   - sin(cp->cartesianCoord[3]) * cos(cp->cartesianCoord[5]);
    rotMat(1, 1) = sin(cp->cartesianCoord[3]) * sin(cp->cartesianCoord[4]) * sin(cp->cartesianCoord[5])
                   + cos(cp->cartesianCoord[3]) * cos(cp->cartesianCoord[5]);
    rotMat(1, 2) = cos(cp->cartesianCoord[4]) * sin(cp->cartesianCoord[5]);
    rotMat(2, 0) = cos(cp->cartesianCoord[3]) * sin(cp->cartesianCoord[4]) * cos(cp->cartesianCoord[5])
                   + sin(cp->cartesianCoord[3]) * sin(cp->cartesianCoord[5]);
    rotMat(2, 1) = sin(cp->cartesianCoord[3]) * sin(cp->cartesianCoord[4]) * cos(cp->cartesianCoord[5])
                   - cos(cp->cartesianCoord[3]) * sin(cp->cartesianCoord[5]);
    rotMat(2, 2) = cos(cp->cartesianCoord[4]) * cos(cp->cartesianCoord[5]);

    vector<Atom *> atomB = molB->getAtoms();
    auto atomNumB = atomB.size();
    for (int i = 0; i < atomNumB; ++i)
    {
        Vector3d originVec(atomB[i]->getLocation()[0],
                           atomB[i]->getLocation()[1],
                           atomB[i]->getLocation()[2]);
        Vector3d target = transVet + rotMat * originVec;
        tuple<double, double, double> curPos(target(0), target(1), target(2));
        posB.push_back(curPos);
    }

    // Measure Cayley distances
    for (auto &constraint: constraints)
    {
        double dist = sqrt((get<0>(posA[constraint.first]) - get<0>(posB[constraint.second])) *
                           (get<0>(posA[constraint.first]) - get<0>(posB[constraint.second]))
                           + (get<1>(posA[constraint.first]) - get<1>(posB[constraint.second])) *
                             (get<1>(posA[constraint.first]) - get<1>(posB[constraint.second]))
                           + (get<2>(posA[constraint.first]) - get<2>(posB[constraint.second])) *
                             (get<2>(posA[constraint.first]) - get<2>(posB[constraint.second])));
        cp->cayleyDistances.push_back(dist);
    }

    for (auto &parameter: parameters)
    {
        double dist = sqrt(pow(get<0>(posA[parameter.first]) - get<0>(posB[parameter.second]), 2)
                           + pow(get<1>(posA[parameter.first]) - get<1>(posB[parameter.second]), 2)
                           + pow(get<2>(posA[parameter.first]) - get<2>(posB[parameter.second]), 2));
        cp->cayleyParameters.push_back(dist);
    }
}

/*
CartesianHypercube *UniformCartesianSampler::intersectFacetUsingSimplexMethod(vector<int> relCoord)
{
    auto facet = new CartesianHypercube(relCoord);
    facet->centre = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, relCoord));
    facet->stepSize = this->stepSize;
    facet->origin = this->origin;
    facet->status = 0;

    if (checkPointInFlipUsingTetVol(facet->centre->cartesianCoord, this->flipNum))
    {
        auto simplexCoords = facet->getAllSimplexRelCoords();


        auto simplices = facet->getAllSimplices();

        for (auto &simplex: simplices)
        {
            ++this->visitedSimplexCount;
            auto curIntersect = intersectSimplexWithRegion(simplex); // Intersection between simplex and hyperplane
            // Intersection found
            if (!curIntersect.empty())
            {
                // Check if intersection point in manifold
                bool fail = false;
                auto ori = UniformCartesianSampler::computeRealization(this->flipNum, curIntersect, fail);
                if (!fail)
                {
                    auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);
                    bool isClose = true;

                    for (int i = 0; i < 6; ++i)
                    {
                        double distToCentre = (cartesianCoord[i] - facet->centre->cartesianCoord[i]) / stepSize[i];
//                        cout << i << "th coord, point: " << cartesianCoord[i] << ", facet: "
//                             << facet->centre->cartesianCoord[i] << ", stepsize: " << stepSize[i] << ", ratio: "
//                             << distToCentre << endl;
                        double threshold = 4;
                        if (distToCentre > threshold || distToCentre < -threshold)
                        {
                            //cout << "Too far" << endl;
                            //isClose = false;
                            break;
                        }
                    }
                    if (isClose)
                    {
                        auto check = new ConstraintCheck(acg, df);
                        bool collision = check->stericsViolated(ori);
                        // Point found.
                        if (!collision)
                        {
                            double fromB[3][3];
                            double toB[3][3];
                            ori->getFromTo(fromB, toB);
                            bool badAngle = check->angleViolationCheck(fromB, toB);
                            if (!badAngle) // status = 4
                            {
                                if (facet->status < 4)
                                {
                                    facet->status = 4;
                                }
                                facet->foundValidCartesianPoints.push_back(cartesianCoord);
                            }
                            else // status = 3
                            {
                                if (facet->status < 3)
                                {
                                    facet->status = 3;
                                }
                                if (this->skipThreshold <= 3)
                                {
                                    facet->foundBadAngleCartesianPoints.push_back(cartesianCoord);
                                }
                            }
                        }
                        else // status = 2
                        {
                            if (facet->status < 2)
                            {
                                facet->status = 2;
                            }
                            if (this->skipThreshold <= 2)
                            {
                                facet->foundCollisionCartesianPoints.push_back(cartesianCoord);
                            }
                        }
                        delete (check);
                    }
                }
                else // status = 1
                {
                    if (facet->status < 1)
                    {
                        facet->status = 1;
                    }
                }
                delete (ori);
            }
        }

        for (auto simplex: simplices)
        {
            delete (simplex);
        }
    }
    return facet;
}
*/



CartesianHypercube *UniformCartesianSampler::intersectHypercubeUsing6DSimplexMethod(vector<int> cubeRelCoord)
{
    if (this->verbose)
    {
        cout << "Current cube: " << cubeRelCoord[0] << ", " << cubeRelCoord[1] << ", " << cubeRelCoord[2] << ", "
             << cubeRelCoord[3] << ", " << cubeRelCoord[4] << ", " << cubeRelCoord[5] << endl;
    }
    auto cube = new CartesianHypercube(cubeRelCoord);
    cube->centre = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, cubeRelCoord));
    cube->stepSize = this->stepSize;
    cube->origin = this->origin;
    cube->flipNum = this->curFlipNo;
    cube->status = 0;

    // neighbourUnvisited gets data on which 5-d faces need checking from goodFrontierCubes
    vector<bool> neighbourUnvisited = this->goodFrontierCubes[cubeRelCoord];

    // neighboursToVisit tracks 12 face neighbours of current cube.
    // If an intersection is found by sampling 5-d facet shared by current cube and neighbour, that neighbour needs to be visited.
    vector<bool> neighboursToVisit(12, false);

    auto facetCoords = cube->getAllFacetRelCoords(6 - acg->getDim());

    for (auto facetCoord: facetCoords)
    {
        bool sampleThisFacet = true;

        for (int i = 0; i < 6; ++i)
        {
            if ((facetCoord[i] == cubeRelCoord[i] - 1) && !neighbourUnvisited[i])
            {
                sampleThisFacet = false;
                if (this->verbose)
                {
                    cout << "Skipping facet " << facetCoord[0] << ", " << facetCoord[1] << ", " << facetCoord[2] << ", "
                         << facetCoord[3] << ", " << facetCoord[4] << ", " << facetCoord[5] << ": already sampled."
                         << endl;
                }
                break;
            }
            if ((facetCoord[i] == cubeRelCoord[i] + 1) && !neighbourUnvisited[i + 6])
            {
                sampleThisFacet = false;
                if (this->verbose)
                {
                    cout << "Skipping facet " << facetCoord[0] << ", " << facetCoord[1] << ", " << facetCoord[2] << ", "
                         << facetCoord[3] << ", " << facetCoord[4] << ", " << facetCoord[5] << ": already sampled."
                         << endl;
                }
                break;
            }
        }

        if (sampleThisFacet)
        {
            ++this->visitedFacetCount[this->curFlipNo];
            auto facet = new CartesianHypercube(facetCoord);
            facet->centre = new CartesianPoint(
                    Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, facetCoord));
            facet->stepSize = this->stepSize;
            facet->origin = this->origin;
            facet->status = 0;
            if (checkPointInFlipUsingTetVol(facet->centre->cartesianCoord, this->curFlipNo))
            {
                if (this->verbose)
                {
                    cout << "Sampling facet " << facetCoord[0] << ", " << facetCoord[1] << ", " << facetCoord[2] << ", "
                         << facetCoord[3] << ", " << facetCoord[4] << ", " << facetCoord[5] << endl;
                }

                auto allSimplexRelCoords = facet->getAllSimplexRelCoords();
                for (auto &simplexRelCoord: allSimplexRelCoords)
                {
                    vector<CartesianPoint *> allVertices;
                    for (auto &vertexCoord: simplexRelCoord)
                    {
                        auto vertex = new CartesianPoint(
                                Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, vertexCoord));
                        allVertices.push_back(vertex);
                    }
                    auto simplex = new CartesianSimplex(6 - acg->getDim(), allVertices);
                    auto curIntersect = intersectSimplexWithRegion(simplex);

                    // Intersection found
                    if (!curIntersect.empty())
                    {
                        // Check if intersection point in manifold
                        bool fail = false;
                        auto ori = UniformCartesianSampler::computeRealization(this->curFlipNo, curIntersect, fail);
                        if (!fail)
                        {
                            auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);
                            auto check = new ConstraintCheck(acg, df);
                            bool collision = check->stericsViolated(ori);
                            // Point found.
                            if (!collision)
                            {
                                double fromB[3][3];
                                double toB[3][3];
                                ori->getFromTo(fromB, toB);
                                bool badAngle = check->angleViolationCheck(fromB, toB);
                                if (!badAngle) // status = 4
                                {
                                    if (this->verbose)
                                    {
                                        cout << "Valid intersection found." << endl;
                                    }

                                    if (facet->status < 4)
                                    {
                                        facet->status = 4;
                                    }
                                    facet->foundValidCartesianPoints.push_back(cartesianCoord);
                                }
                                else // status = 3
                                {
                                    if (this->verbose)
                                    {
                                        cout << "Bad angle intersection found." << endl;
                                    }

                                    if (facet->status < 3)
                                    {
                                        facet->status = 3;
                                    }
                                    if (this->skipThreshold <= 3)
                                    {
                                        facet->foundBadAngleCartesianPoints.push_back(cartesianCoord);
                                    }
                                }
                            }
                            else // status = 2
                            {
                                if (this->verbose)
                                {
                                    cout << "Collision intersection found." << endl;
                                }
                                if (facet->status < 2)
                                {
                                    facet->status = 2;
                                }
                                if (this->skipThreshold <= 2)
                                {
                                    facet->foundCollisionCartesianPoints.push_back(cartesianCoord);
                                }
                            }
                            delete (check);
                        }
                        else // status = 1
                        {
                            if (this->verbose)
                            {
                                cout << "Not realisable intersection found." << endl;
                            }
                            if (facet->status < 1)
                            {
                                facet->status = 1;
                            }
                        }
                        delete (ori);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "Cannot find intersection. " << endl;
                        }
                    }
                    delete (simplex);
                }

                // Update status
                if (facet->status > cube->status)
                {
                    cube->status = facet->status;
                }

                // Update found points
                cube->foundValidCartesianPoints.insert(cube->foundValidCartesianPoints.end(),
                                                       facet->foundValidCartesianPoints.begin(),
                                                       facet->foundValidCartesianPoints.end());
                if (this->skipThreshold <= 3)
                {
                    cube->foundBadAngleCartesianPoints.insert(cube->foundBadAngleCartesianPoints.end(),
                                                              facet->foundBadAngleCartesianPoints.begin(),
                                                              facet->foundBadAngleCartesianPoints.end());
                }
                if (this->skipThreshold <= 2)
                {
                    cube->foundCollisionCartesianPoints.insert(cube->foundCollisionCartesianPoints.end(),
                                                               facet->foundCollisionCartesianPoints.begin(),
                                                               facet->foundCollisionCartesianPoints.end());
                }
                // Mark neighbours to visit
                if (facet->status >= skipThreshold)
                {
                    for (int i = 0; i < 6; ++i)
                    {
                        if (facetCoord[i] != cubeRelCoord[i])
                        {
                            vector<int> nextCubeRelativeCoord = cubeRelCoord;
                            if (facetCoord[i] < cubeRelCoord[i])
                            {
                                neighboursToVisit[i] = true;
                            }
                            else
                            {
                                neighboursToVisit[i + 6] = true;
                            }
                        }
                    }
                }
            }
            else
            {
                if (this->verbose)
                {
                    cout << "Skipping facet " << facetCoord[0] << ", " << facetCoord[1] << ", " << facetCoord[2] << ", "
                         << facetCoord[3] << ", " << facetCoord[4] << ", " << facetCoord[5] << ": not in current flip."
                         << endl;
                }
            }

            delete (facet->centre);
            delete (facet);
        }
    }

    if (this->verbose)
    {
        cout << "Cube sampled, handling neighbours." << endl;
    }

    for (int i = 0; i < 6; ++i)
    {
        if (neighbourUnvisited[i])
        {
            // Add to good if checked
            if (neighboursToVisit[i])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }

                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
                }

                // Not in good
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either, create and put in good
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad, move to good
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                    // Case 3: already in good
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }

                // Mark current cube on neighbour
                this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
            }
        }

        if (neighbourUnvisited[i + 6])
        {
            // Add to good if checked
            if (neighboursToVisit[i + 6])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }

                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }
                this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
            }
        }
    }
    this->goodFrontierCubes.erase(cubeRelCoord);

    return cube;
}

CartesianHypercube *UniformCartesianSampler::intersectHypercubeUsing5DSimplexMethod(vector<int> cubeRelCoord)
{
    if (this->verbose)
    {
        cout << "Current cube: " << cubeRelCoord[0] << ", " << cubeRelCoord[1] << ", " << cubeRelCoord[2] << ", "
             << cubeRelCoord[3] << ", " << cubeRelCoord[4] << ", " << cubeRelCoord[5] << endl;
    }
    auto cube = new CartesianHypercube(cubeRelCoord);
    cube->centre = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, cubeRelCoord));
    cube->stepSize = this->stepSize;
    cube->origin = this->origin;
    cube->flipNum = this->curFlipNo;
    cube->status = 0;

    // neighbourUnvisited gets data on which 5-d faces need checking from goodFrontierCubes
    vector<bool> neighbourUnvisited = this->goodFrontierCubes[cubeRelCoord];

    // neighboursToVisit tracks 12 face neighbours of current cube.
    // If an intersection is found by sampling 5-d facet shared by current cube and neighbour, that neighbour needs to be visited.
    vector<bool> neighboursToVisit(12, false);

    // Here "face" refers to 5-dim cubes
    auto faceRelCoords = cube->getAllFacetRelCoords(5);
    for (auto faceRelCoord: faceRelCoords)
    {
        if (this->verbose)
        {
            cout << "Current face: " << faceRelCoord[0] << ", " << faceRelCoord[1] << ", " << faceRelCoord[2] << ", "
                 << faceRelCoord[3] << ", " << faceRelCoord[4] << ", " << faceRelCoord[5] << endl;
        }
        bool sampleThis = true;
        for (int i = 0; i < 6; ++i)
        {
            if ((faceRelCoord[i] == cubeRelCoord[i] - 1) && !neighbourUnvisited[i])
            {
                sampleThis = false;
                break;
            }
            if ((faceRelCoord[i] == cubeRelCoord[i] + 1) && !neighbourUnvisited[i + 6])
            {
                sampleThis = false;
                break;
            }
        }

        if (sampleThis)
        {
            auto face = new CartesianHypercube(faceRelCoord);
            face->centre = new CartesianPoint(
                    Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, faceRelCoord));
            face->stepSize = this->stepSize;
            face->origin = this->origin;
            face->status = 0;
            auto facetRelCoords = face->getAllFacetRelCoords(5 - acg->getDim());
            for (auto facetRelCoord: facetRelCoords)
            {
                if (this->verbose)
                {
                    cout << "Current facet: " << facetRelCoord[0] << ", " << facetRelCoord[1] << ", "
                         << facetRelCoord[2] << ", " << facetRelCoord[3] << ", " << facetRelCoord[4] << ", "
                         << facetRelCoord[5] << endl;
                }
                ++this->visitedFacetCount[this->curFlipNo];
                auto facet = new CartesianHypercube(facetRelCoord);
                facet->centre = new CartesianPoint(
                        Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, facetRelCoord));
                facet->stepSize = this->stepSize;
                facet->origin = this->origin;
                facet->status = 0;

                // Facet centre should be within current cube
                if (checkPointInFlipUsingTetVol(facet->centre->cartesianCoord, this->curFlipNo))
                {
                    auto allSimplexRelCoords = facet->getAllSimplexRelCoords();
                    for (auto &simplexRelCoord: allSimplexRelCoords)
                    {
                        ++this->visitedSimplexCount[this->curFlipNo];
                        vector<CartesianPoint *> allVertices;
                        for (auto &vertexCoord: simplexRelCoord)
                        {
                            auto vertex = new CartesianPoint(
                                    Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, vertexCoord));
                            allVertices.push_back(vertex);
                        }
                        allVertices.push_back(new CartesianPoint(face->centre));
                        auto simplex = new CartesianSimplex(6 - acg->getDim(), allVertices);
                        auto curIntersect = intersectSimplexWithRegion(simplex);

                        // Intersection found
                        if (!curIntersect.empty())
                        {
                            // Check if intersection point in manifold
                            bool fail = false;
                            auto ori = UniformCartesianSampler::computeRealization(this->curFlipNo, curIntersect, fail);
                            if (!fail)
                            {
                                auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);

                                auto check = new ConstraintCheck(acg, df);
                                bool collision = check->stericsViolated(ori);
                                // Point found.
                                if (!collision)
                                {
                                    double fromB[3][3];
                                    double toB[3][3];
                                    ori->getFromTo(fromB, toB);
                                    bool badAngle = check->angleViolationCheck(fromB, toB);
                                    if (!badAngle) // status = 4
                                    {
                                        if (this->verbose)
                                        {
                                            cout << "Current simplex feasible." << endl;
                                        }
                                        if (facet->status < 4)
                                        {
                                            facet->status = 4;
                                        }
                                        facet->foundValidCartesianPoints.push_back(cartesianCoord);
                                    }
                                    else // status = 3
                                    {
                                        if (this->verbose)
                                        {
                                            cout << "Current simplex bad angle." << endl;
                                        }
                                        if (facet->status < 3)
                                        {
                                            facet->status = 3;
                                        }
                                        if (this->skipThreshold <= 3)
                                        {
                                            facet->foundBadAngleCartesianPoints.push_back(cartesianCoord);
                                        }
                                    }
                                }
                                else // status = 2
                                {
                                    if (this->verbose)
                                    {
                                        cout << "Current simplex collision." << endl;
                                    }
                                    if (facet->status < 2)
                                    {
                                        facet->status = 2;
                                    }
                                    if (this->skipThreshold <= 2)
                                    {
                                        facet->foundCollisionCartesianPoints.push_back(cartesianCoord);
                                    }
                                }
                                delete (check);
                            }
                            else // status = 1
                            {
                                if (facet->status < 1)
                                {
                                    facet->status = 1;
                                }
                            }
                            delete (ori);
                        }
                        delete (simplex);
                    }

                    // Update status
                    if (facet->status > face->status)
                    {
                        face->status = facet->status;
                    }

                    // Update found points
                    cube->foundValidCartesianPoints.insert(cube->foundValidCartesianPoints.end(),
                                                           facet->foundValidCartesianPoints.begin(),
                                                           facet->foundValidCartesianPoints.end());
                    if (this->skipThreshold <= 3)
                    {
                        cube->foundBadAngleCartesianPoints.insert(cube->foundBadAngleCartesianPoints.end(),
                                                                  facet->foundBadAngleCartesianPoints.begin(),
                                                                  facet->foundBadAngleCartesianPoints.end());
                    }
                    if (this->skipThreshold <= 2)
                    {
                        cube->foundCollisionCartesianPoints.insert(cube->foundCollisionCartesianPoints.end(),
                                                                   facet->foundCollisionCartesianPoints.begin(),
                                                                   facet->foundCollisionCartesianPoints.end());
                    }
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "Facet centre outside of current flip. Abandoning facet." << endl;
                    }
                }
                delete (facet->centre);
                delete (facet);
            }

            if (face->status > cube->status)
            {
                cube->status = face->status;
            }

            // Mark neighbours to visit
            if (face->status >= skipThreshold)
            {
                for (int i = 0; i < 6; ++i)
                {
                    if (faceRelCoord[i] != cubeRelCoord[i])
                    {
                        vector<int> nextCubeRelativeCoord = cubeRelCoord;
                        if (faceRelCoord[i] < cubeRelCoord[i])
                        {
                            neighboursToVisit[i] = true;
                        }
                        else
                        {
                            neighboursToVisit[i + 6] = true;
                        }
                    }
                }
            }

            delete (face->centre);
            delete (face);
        }
        else
        {
            if (this->verbose)
            {
                cout << "Skipping face." << endl;
            }
        }
    }

    if (this->verbose)
    {
        cout << "Cube sampled, handling neighbours." << endl;
    }

    for (int i = 0; i < 6; ++i)
    {
        if (neighbourUnvisited[i])
        {
            // Add to good if checked
            if (neighboursToVisit[i])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }

                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
                }

                // Not in good
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either, create and put in good
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad, move to good
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                    // Case 3: already in good
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }

                // Mark current cube on neighbour
                this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
            }
        }

        if (neighbourUnvisited[i + 6])
        {
            // Add to good if checked
            if (neighboursToVisit[i + 6])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }

                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }
                this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
            }
        }
    }
    this->goodFrontierCubes.erase(cubeRelCoord);

    return cube;
}

CartesianHypercube *UniformCartesianSampler::intersectHypercubeUsingHybridSimplexMethod(vector<int> cubeRelCoord)
{
    if (this->verbose)
    {
        cout << "Current cube: " << cubeRelCoord[0] << ", " << cubeRelCoord[1] << ", " << cubeRelCoord[2] << ", "
             << cubeRelCoord[3] << ", " << cubeRelCoord[4] << ", " << cubeRelCoord[5] << endl;
    }

    auto cube = new CartesianHypercube(cubeRelCoord);
    cube->centre = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, cubeRelCoord));
    cube->stepSize = this->stepSize;
    cube->origin = this->origin;
    cube->flipNum = this->curFlipNo;
    cube->status = 0;

    // neighbourUnvisited gets data on which 5-d faces need checking from goodFrontierCubes
    vector<bool> neighbourUnvisited = this->goodFrontierCubes[cubeRelCoord];

    if (this->verbose)
    {
        cout << "Neighbour stat: " << neighbourUnvisited[0] << ", " << neighbourUnvisited[1] << ", "
             << neighbourUnvisited[2] << ", "
             << neighbourUnvisited[3] << ", " << neighbourUnvisited[4] << ", " << neighbourUnvisited[5] << ", "
             << neighbourUnvisited[6]
             << ", " << neighbourUnvisited[7] << ", " << neighbourUnvisited[8] << ", " << neighbourUnvisited[9] << ", "
             << neighbourUnvisited[10] << ", " << neighbourUnvisited[11] << endl;
    }

    // neighboursToVisit tracks 12 face neighbours of current cube.
    // If an intersection is found by sampling 5-d facet shared by current cube and neighbour, that neighbour needs to be visited.
    vector<bool> neighboursToVisit(12, false);

    auto facetCoords = cube->getAllFacetRelCoords(6 - acg->getDim());

    for (auto facetCoord: facetCoords)
    {
        bool sampleThisFacet = true;

        // TODO temporary, remember to switch back!

        for (int i = 0; i < 6; ++i)
        {
            if ((facetCoord[i] == cubeRelCoord[i] - 1) && !neighbourUnvisited[i])
            {
                sampleThisFacet = false;
                break;
            }
            if ((facetCoord[i] == cubeRelCoord[i] + 1) && !neighbourUnvisited[i + 6])
            {
                sampleThisFacet = false;
                break;
            }
        }

        if (sampleThisFacet)
        {
            if (this->verbose)
            {
                cout << "Current facet: " << facetCoord[0] << ", " << facetCoord[1] << ", " << facetCoord[2] << ", "
                     << facetCoord[3] << ", " << facetCoord[4] << ", " << facetCoord[5] << endl;
            }

            ++this->visitedFacetCount[this->curFlipNo];
            auto facet = new CartesianHypercube(facetCoord);
            facet->centre = new CartesianPoint(
                    Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, facetCoord));
            facet->stepSize = this->stepSize;
            facet->origin = this->origin;
            facet->status = 0;

            if (checkPointInFlipUsingTetVol(facet->centre->cartesianCoord, this->curFlipNo))
            {
                auto allSimplexRelCoords = facet->getAllSimplexRelCoords();
                for (auto &simplexRelCoord: allSimplexRelCoords)
                {
                    vector<CartesianPoint *> allVertices;
                    for (auto &vertexCoord: simplexRelCoord)
                    {
                        auto vertex = new CartesianPoint(
                                Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, vertexCoord));
                        allVertices.push_back(vertex);
                    }
                    auto simplex = new CartesianSimplex(6 - acg->getDim(), allVertices);
                    auto curIntersect = intersectSimplexWithRegion(simplex);

                    // Intersection found
                    if (!curIntersect.empty())
                    {
                        // Check if intersection point in manifold
                        bool fail = false;
                        auto ori = UniformCartesianSampler::computeRealization(this->curFlipNo, curIntersect, fail);
                        if (!fail)
                        {
                            auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);

                            auto check = new ConstraintCheck(acg, df);
                            bool collision = check->stericsViolated(ori);
                            // Point found.
                            if (!collision)
                            {
                                double fromB[3][3];
                                double toB[3][3];
                                ori->getFromTo(fromB, toB);
                                bool badAngle = check->angleViolationCheck(fromB, toB);
                                if (!badAngle) // status = 4
                                {
                                    if (facet->status < 4)
                                    {
                                        facet->status = 4;
                                    }
                                    facet->foundValidCartesianPoints.push_back(cartesianCoord);
                                }
                                else // status = 3
                                {
                                    if (facet->status < 3)
                                    {
                                        facet->status = 3;
                                    }
                                    if (this->skipThreshold <= 3)
                                    {
                                        facet->foundBadAngleCartesianPoints.push_back(cartesianCoord);
                                    }
                                }
                            }
                            else // status = 2
                            {
                                if (facet->status < 2)
                                {
                                    facet->status = 2;
                                }
                                if (this->skipThreshold <= 2)
                                {
                                    facet->foundCollisionCartesianPoints.push_back(cartesianCoord);
                                }
                            }
                            delete (check);
                        }
                        else // status = 1
                        {
                            if (facet->status < 1)
                            {
                                facet->status = 1;
                            }
                        }
                        delete (ori);
                    }
                    delete (simplex);
                }

                // Update status
                if (facet->status > cube->status)
                {
                    cube->status = facet->status;
                }

                // Update found points
                cube->foundValidCartesianPoints.insert(cube->foundValidCartesianPoints.end(),
                                                       facet->foundValidCartesianPoints.begin(),
                                                       facet->foundValidCartesianPoints.end());
                if (this->skipThreshold <= 3)
                {
                    cube->foundBadAngleCartesianPoints.insert(cube->foundBadAngleCartesianPoints.end(),
                                                              facet->foundBadAngleCartesianPoints.begin(),
                                                              facet->foundBadAngleCartesianPoints.end());
                }
                if (this->skipThreshold <= 2)
                {
                    cube->foundCollisionCartesianPoints.insert(cube->foundCollisionCartesianPoints.end(),
                                                               facet->foundCollisionCartesianPoints.begin(),
                                                               facet->foundCollisionCartesianPoints.end());
                }
                // Mark neighbours to visit
                if (facet->status >= skipThreshold)
                {
                    for (int i = 0; i < 6; ++i)
                    {
                        if (facetCoord[i] != cubeRelCoord[i])
                        {
                            vector<int> nextCubeRelativeCoord = cubeRelCoord;
                            if (facetCoord[i] < cubeRelCoord[i])
                            {
                                neighboursToVisit[i] = true;
                            }
                            else
                            {
                                neighboursToVisit[i + 6] = true;
                            }
                        }
                    }
                }
            }
            delete (facet->centre);
            delete (facet);
        }
        else
        {
            if (this->verbose)
            {
                cout << "Skipping facet: " << facetCoord[0] << ", " << facetCoord[1] << ", " << facetCoord[2] << ", "
                     << facetCoord[3] << ", " << facetCoord[4] << ", " << facetCoord[5] << endl;
            }
        }
    }

    if (cube->status < 4)
    {
        if (this->verbose)
        {
            cout << "6-dim decomposition fails to find an intersection, trying 5-dim decomposition." << endl;
        }
        auto faceRelCoords = cube->getAllFacetRelCoords(5);
        for (auto faceRelCoord: faceRelCoords)
        {
            if (this->verbose)
            {
                cout << "Current face: " << faceRelCoord[0] << ", " << faceRelCoord[1] << ", " << faceRelCoord[2]
                     << ", " << faceRelCoord[3] << ", " << faceRelCoord[4] << ", " << faceRelCoord[5] << endl;
            }
            bool sampleThis = true;
            for (int i = 0; i < 6; ++i)
            {
                if ((faceRelCoord[i] == cubeRelCoord[i] - 1) && !neighbourUnvisited[i])
                {
                    sampleThis = false;
                    break;
                }
                if ((faceRelCoord[i] == cubeRelCoord[i] + 1) && !neighbourUnvisited[i + 6])
                {
                    sampleThis = false;
                    break;
                }
            }

            if (sampleThis)
            {
                auto face = new CartesianHypercube(faceRelCoord);
                face->centre = new CartesianPoint(
                        Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, faceRelCoord));
                face->stepSize = this->stepSize;
                face->origin = this->origin;
                face->status = 0;
                auto facetRelCoords = face->getAllFacetRelCoords(5 - acg->getDim());
                for (auto facetRelCoord: facetRelCoords)
                {
                    if (this->verbose)
                    {
                        cout << "Current facet: " << facetRelCoord[0] << ", " << facetRelCoord[1] << ", "
                             << facetRelCoord[2] << ", " << facetRelCoord[3] << ", " << facetRelCoord[4] << ", "
                             << facetRelCoord[5] << endl;
                    }
                    ++this->visitedFacetCount[this->curFlipNo];
                    auto facet = new CartesianHypercube(facetRelCoord);
                    facet->centre = new CartesianPoint(
                            Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, facetRelCoord));
                    facet->stepSize = this->stepSize;
                    facet->origin = this->origin;
                    facet->status = 0;

                    // Facet centre should be within current flip
                    if (checkPointInFlipUsingTetVol(facet->centre->cartesianCoord, this->curFlipNo))
                    {
                        auto allSimplexCoords = facet->getAllSimplexRelCoords();
                        for (auto &simplexCoord: allSimplexCoords)
                        {
                            ++this->visitedSimplexCount[this->curFlipNo];
                            vector<CartesianPoint *> allVertices;
                            for (auto &vertexCoord: simplexCoord)
                            {
                                auto vertex = new CartesianPoint(
                                        Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, vertexCoord));
                                allVertices.push_back(vertex);
                            }
                            allVertices.push_back(new CartesianPoint(face->centre));
                            auto simplex = new CartesianSimplex(6 - acg->getDim(), allVertices);
                            auto curIntersect = intersectSimplexWithRegion(simplex);

                            // Intersection found
                            if (!curIntersect.empty())
                            {
                                // Check if intersection point in manifold
                                bool fail = false;
                                auto ori = UniformCartesianSampler::computeRealization(this->curFlipNo, curIntersect,
                                                                                       fail);
                                if (!fail)
                                {
                                    auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);

                                    auto check = new ConstraintCheck(acg, df);
                                    bool collision = check->stericsViolated(ori);
                                    // Point found.
                                    if (!collision)
                                    {
                                        double fromB[3][3];
                                        double toB[3][3];
                                        ori->getFromTo(fromB, toB);
                                        bool badAngle = check->angleViolationCheck(fromB, toB);
                                        if (!badAngle) // status = 4
                                        {
                                            if (this->verbose)
                                            {
                                                cout << "Current simplex feasible." << endl;
                                            }
                                            if (facet->status < 4)
                                            {
                                                facet->status = 4;
                                            }
                                            facet->foundValidCartesianPoints.push_back(cartesianCoord);
                                        }
                                        else // status = 3
                                        {
                                            if (this->verbose)
                                            {
                                                cout << "Current simplex bad angle." << endl;
                                            }
                                            if (facet->status < 3)
                                            {
                                                facet->status = 3;
                                            }
                                            if (this->skipThreshold <= 3)
                                            {
                                                facet->foundBadAngleCartesianPoints.push_back(cartesianCoord);
                                            }
                                        }
                                    }
                                    else // status = 2
                                    {
                                        if (this->verbose)
                                        {
                                            cout << "Current simplex collision." << endl;
                                        }
                                        if (facet->status < 2)
                                        {
                                            facet->status = 2;
                                        }
                                        if (this->skipThreshold <= 2)
                                        {
                                            facet->foundCollisionCartesianPoints.push_back(cartesianCoord);
                                        }
                                    }
                                    delete (check);
                                }
                                else // status = 1
                                {
                                    if (facet->status < 1)
                                    {
                                        facet->status = 1;
                                    }
                                }
                                delete (ori);
                            }
                            delete (simplex);
                        }

                        // Update status
                        if (facet->status > face->status)
                        {
                            face->status = facet->status;
                        }

                        // Update found points
                        cube->foundValidCartesianPoints.insert(cube->foundValidCartesianPoints.end(),
                                                               facet->foundValidCartesianPoints.begin(),
                                                               facet->foundValidCartesianPoints.end());
                        if (this->skipThreshold <= 3)
                        {
                            cube->foundBadAngleCartesianPoints.insert(cube->foundBadAngleCartesianPoints.end(),
                                                                      facet->foundBadAngleCartesianPoints.begin(),
                                                                      facet->foundBadAngleCartesianPoints.end());
                        }
                        if (this->skipThreshold <= 2)
                        {
                            cube->foundCollisionCartesianPoints.insert(cube->foundCollisionCartesianPoints.end(),
                                                                       facet->foundCollisionCartesianPoints.begin(),
                                                                       facet->foundCollisionCartesianPoints.end());
                        }
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "Facet centre outside of current flip. Abandoning facet." << endl;
                        }
                    }
                    delete (facet->centre);
                    delete (facet);
                }

                if (face->status > cube->status)
                {
                    cube->status = face->status;
                }

                // Mark neighbours to visit
                if (face->status >= skipThreshold)
                {
                    for (int i = 0; i < 6; ++i)
                    {
                        if (faceRelCoord[i] != cubeRelCoord[i])
                        {
                            vector<int> nextCubeRelativeCoord = cubeRelCoord;
                            if (faceRelCoord[i] < cubeRelCoord[i])
                            {
                                neighboursToVisit[i] = true;
                            }
                            else
                            {
                                neighboursToVisit[i + 6] = true;
                            }
                        }
                    }
                }

                delete (face->centre);
                delete (face);
            }
            else
            {
                if (this->verbose)
                {
                    cout << "Skipping face." << endl;
                }
            }
        }
    }

    if (this->verbose)
    {
        cout << "Cube sampled, handling neighbours." << endl;
    }

    for (int i = 0; i < 6; ++i)
    {
        if (neighbourUnvisited[i])
        {
            // Add to good if checked
            if (neighboursToVisit[i])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }

                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
                }

                // Not in good
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either, create and put in good
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad, move to good
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                    // Case 3: already in good
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }

                // Mark current cube on neighbour
                this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
            }
        }

        if (neighbourUnvisited[i + 6])
        {
            // Add to good if checked
            if (neighboursToVisit[i + 6])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }

                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }
                this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", "
                         << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4]
                         << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
            }
        }
    }
    this->goodFrontierCubes.erase(cubeRelCoord);

    return cube;
}


CartesianHypercube *UniformCartesianSampler::intersectHypercubeUsingBasisMethod(vector<int> cubeRelativeCoord)
{
    //cout << "Current cube: " << cubeRelativeCoord[0] << ", " << cubeRelativeCoord[1] << ", " << cubeRelativeCoord[2]
    //     << ", " << cubeRelativeCoord[3] << ", " << cubeRelativeCoord[4] << ", " << cubeRelativeCoord[5] << endl;
    auto cube = getCubeFromRelCoord(cubeRelativeCoord);
    cube->stepSize = this->stepSize;
    cube->origin = this->origin;
    cube->centre =
            new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, cubeRelativeCoord));
    cube->flipNum = this->curFlipNo;
    cube->status = 0;

    vector<bool> neighbourUnvisited = goodFrontierCubes[cubeRelativeCoord];

    vector<bool> neighboursToVisit(12, false);

    int dim = 6 - acg->getDim();

    // calculate hyper-parallelepiped basis
    cube->cayleyParaBasisDistance = Eigen::MatrixXd(6, dim);
    cube->cayleyParaBasisParameter = Eigen::MatrixXd(6, 6 - dim);
    for (int i = 0; i < 6; ++i)
    {
        vector<int> start(6, 0);
        vector<int> end(6, 0);
        start[i] = -1;
        end[i] = 1;
        auto startPt = cube->getPoint(start);
        auto endPt = cube->getPoint(end);
        cartesian2Cayley(startPt);
        cartesian2Cayley(endPt);

        for (int j = 0; j < dim; ++j)
        {
            cube->cayleyParaBasisDistance(i, j) =
                    (endPt->cayleyDistances[j] - startPt->cayleyDistances[j]) / 2;
        }
        for (int j = 0; j < 6 - dim; ++j)
        {
            cube->cayleyParaBasisParameter(i, j) =
                    (endPt->cayleyParameters[j] - startPt->cayleyParameters[j]) / 2;
        }

        delete (startPt);
        delete (endPt);
    }

    cube->status = 1;

    vector<vector<int>> facetCoords;

    for (int i = 0; i < 729; ++i)
    {
        vector<int> ter = Cartesian::signedDec2ter(i);
        if (std::count(ter.begin(), ter.end(), 0) == dim)
        {
            vector<int> facetCoord(6, 0);
            for (int j = 0; j < 6; ++j)
            {
                facetCoord[j] = cubeRelativeCoord[j] + ter[j];
            }

            facetCoords.emplace_back(facetCoord);
        }
    }

    for (auto &facetRelCoord: facetCoords)
    {
        // Calculate intersect between hyper-parallelepiped and manifold
        ++this->visitedFacetCount[this->curFlipNo];
        auto facet = new CartesianHypercube(facetRelCoord);
        facet->centre = new CartesianPoint(
                Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, facetRelCoord));
        facet->stepSize = this->stepSize;
        facet->origin = this->origin;
        facet->status = 0;
        if (checkPointInFlipUsingTetVol(facet->centre->cartesianCoord, this->curFlipNo))
        {
            facet = intersectFacetWithRegion(facetRelCoord);
            // Ignore newly found intersect if it doesn't exist or is already found as an intersection of another cube
            if (!facet->foundCayleyParametersForParaMethod.empty())
            {
                bool fail = false;
                // Check if intersection is in given region of manifold
                auto ori = UniformCartesianSampler::computeRealization(this->curFlipNo,
                                                                       facet->foundCayleyParametersForParaMethod, fail);
                // Intersection in region
                if (!fail)
                {
                    auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);
                    facet->foundCartesianIntersectionForParaMethod = cartesianCoord;

                    auto check = new ConstraintCheck(acg, df);
                    bool collision = check->stericsViolated(ori);

                    if (!collision)
                    {
                        double fromB[3][3];
                        double toB[3][3];
                        ori->getFromTo(fromB, toB);
                        bool badAngle = check->angleViolationCheck(fromB, toB);
                        if (!badAngle) // valid
                        {
                            if (facet->status < 4)
                            {
                                facet->status = 4;
                            }
                            cube->foundValidCartesianPoints.push_back(
                                    facet->foundCartesianIntersectionForParaMethod);
                        }
                        else // bad angle
                        {
                            if (facet->status < 3)
                            {
                                facet->status = 3;
                            }
                            cube->foundBadAngleCartesianPoints.push_back(
                                    facet->foundCartesianIntersectionForParaMethod);
                        }
                    }
                    else // collision
                    {
                        if (facet->status < 2)
                        {
                            facet->status = 2;
                        }
                        cube->foundCollisionCartesianPoints.push_back(
                                facet->foundCartesianIntersectionForParaMethod);
                    }
                    delete (check);
                    if (facet->status > cube->status)
                    {
                        cube->status = facet->status;
                    }
                }
                else // fail == true
                {
                    //cout << "Intersection not in region of manifold" << endl;
                    if (facet->status < 1)
                    {
                        facet->status = 1;
                    }
                }
                delete (ori);
            }

            // Handle neighbours
            if (facet->status >= skipThreshold)
            {
                for (int i = 0; i < 6; ++i)
                {
                    if (facetRelCoord[i] != cubeRelativeCoord[i])
                    {
                        vector<int> nextCubeRelativeCoord = cubeRelativeCoord;
                        if (facetRelCoord[i] < cubeRelativeCoord[i])
                        {
                            neighboursToVisit[i] = true;
                        }
                        else
                        {
                            neighboursToVisit[i + 6] = true;
                        }
                    }
                }
            }
        }
        delete (facet->centre);
        delete (facet);
    }

    // put all neighbours checked to list
    for (int i = 0; i < 6; ++i)
    {
        if (neighbourUnvisited[i])
        {
            // Add to good if checked
            if (neighboursToVisit[i])
            {
                vector<int> nextCubeRelativeCoord = cubeRelativeCoord;
                nextCubeRelativeCoord[i] -= 2;
//                cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
//                     << ", "
//                     << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", " << nextCubeRelativeCoord[4]
//                     << ", " << nextCubeRelativeCoord[5] << endl;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
//
//                if (this->allCubesRelativeCoord[nextCubeRelativeCoord] == 0)
//                {
//                    //cout << "add neighbour no " << i << ", " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
//                    //     << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
//                    //     << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
//                    allCubesRelativeCoord[nextCubeRelativeCoord] = -1;
//                    allCubesToTraverseRelativeCoord.push(nextCubeRelativeCoord);
//                }
//                else
//                {
//                    //cout << "duplicate" << endl;
//                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        //cout << "New cube" << endl;
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad
                    else
                    {
                        //cout << "In bad" << endl;
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                else
                {
                    //cout << "In good" << endl;
                }
                this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelativeCoord;
                nextCubeRelativeCoord[i] -= 2;
//                cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
//                     << ", "
//                     << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", " << nextCubeRelativeCoord[4]
//                     << ", " << nextCubeRelativeCoord[5] << endl;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        //cout << "New cube" << endl;
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        //cout << "In bad" << endl;
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
                else
                {
                    //cout << "In good" << endl;
                    this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }

            }
        }
        if (neighbourUnvisited[i + 6])
        {
            if (neighboursToVisit[i + 6])
            {
                vector<int> nextCubeRelativeCoord = cubeRelativeCoord;
                nextCubeRelativeCoord[i] += 2;
//                cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
//                     << ", "
//                     << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", " << nextCubeRelativeCoord[4]
//                     << ", " << nextCubeRelativeCoord[5] << endl;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }

//                if (this->allCubesRelativeCoord[nextCubeRelativeCoord] == 0)
//                {
//                    //cout << "add neighbour no " << i + 6 << ", " << nextCubeRelativeCoord[0] << ", "
//                    //     << nextCubeRelativeCoord[1] << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3]
//                    //     << ", " << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
//                    allCubesRelativeCoord[nextCubeRelativeCoord] = -1;
//                    allCubesToTraverseRelativeCoord.push(nextCubeRelativeCoord);
//                }
//                else
//                {
//                    //cout << "duplicate" << endl;
//                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    // Case 1: not in either
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        //cout << "New cube" << endl;
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                        // Case 2: in bad
                    else
                    {
                        //cout << "In bad" << endl;
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                else
                {
                    //cout << "In good" << endl;
                }
                this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
            }
                // Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelativeCoord;
                nextCubeRelativeCoord[i] += 2;
//                cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
//                     << ", "
//                     << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", " << nextCubeRelativeCoord[4]
//                     << ", " << nextCubeRelativeCoord[5] << endl;
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        //cout << "New cube" << endl;
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        //cout << "In bad" << endl;
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
                else
                {
                    //cout << "In good" << endl;
                    this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
            }
        }
    }
    this->goodFrontierCubes.erase(cubeRelativeCoord);

    return cube;
}


vector<double> UniformCartesianSampler::intersectSimplexWithRegion(CartesianSimplex *simplex)
{
    vector<double> ret;

    for (auto vertex: simplex->vertices)
    {
        cartesian2Cayley(vertex);
    }

    auto allDistances = Eigen::MatrixXd(simplex->dim + 1, simplex->dim + 1); // Distances from simplex
    auto allConstraints = Eigen::VectorXd(simplex->dim + 1); // Constraints from acg and cc

    for (int i = 0; i < simplex->dim; ++i)
    {
        for (int j = 0; j < simplex->dim + 1; ++j)
        {
            allDistances(i, j) = simplex->vertices[j]->cayleyDistances[i];
        }
    }
    // Truncate row of 1
    for (int i = 0; i < simplex->dim + 1; ++i)
    {
        allDistances(simplex->dim, i) = 1;
    }

    for (int i = 0; i < simplex->dim; ++i)
    {
        allConstraints(i) = cc->getEdgeLength(acg->getContacts()[i].first, acg->getContacts()[i].second);
    }
    allConstraints(simplex->dim) = 1;
    // Solve linear system
    Eigen::VectorXd Lambda = allDistances.householderQr().solve(allConstraints);

    for (int i = 0; i < simplex->dim + 1; ++i)
    {
        // Intersection outside of simplex
        // Here we just need to check if each lies between 0 and 1 since we have the sum of lambda equals 1 already added by truncating row of 1 to the matrix
        if (isnan(Lambda(i)) || (Lambda(i) > 1 + TOL_LAMBDA) || (Lambda(i) < -TOL_LAMBDA))
        {
            return ret;
        }
    }

    // Apply Lambda on parameters
    auto allParameters = Eigen::MatrixXd(6 - simplex->dim, simplex->dim + 1); // Parameters
    for (int i = 0; i < 6 - simplex->dim; ++i)
    {
        for (int j = 0; j < simplex->dim + 1; ++j)
        {
            allParameters(i, j) = simplex->vertices[j]->cayleyParameters[i];
        }
    }

    for (int i = 0; i < 6 - simplex->dim; ++i)
    {
        ret.emplace_back((allParameters * Lambda)(i));
    }

    return ret;
}

CartesianHypercube *UniformCartesianSampler::intersectFacetWithRegion(const vector<int> &relativeCoord)
{
    auto facet = new CartesianHypercube(relativeCoord);
    facet->centre = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, relativeCoord));
    cartesian2Cayley(facet->centre);
    facet->stepSize = this->stepSize;
    facet->status = 0;

    facet->cayleyParaBasisDistance = Eigen::MatrixXd(facet->dim, facet->dim);
    facet->cayleyParaBasisParameter = Eigen::MatrixXd(facet->dim, 6 - facet->dim);

    int count = 0;
    for (int i = 0; i < 6; ++i)
    {
        if (facet->relCoord[i] % 2 == 0)
        {
            vector<int> start(6, 0);
            vector<int> end(6, 0);
            start[i] = -1;
            end[i] = 1;
            auto startPt = facet->getPoint(start);
            auto endPt = facet->getPoint(end);
            cartesian2Cayley(startPt);
            cartesian2Cayley(endPt);

            for (int j = 0; j < facet->dim; ++j)
            {
                facet->cayleyParaBasisDistance(count, j) =
                        (endPt->cayleyDistances[j] - startPt->cayleyDistances[j]) / 2;
            }
            for (int j = 0; j < 6 - facet->dim; ++j)
            {
                facet->cayleyParaBasisParameter(count, j) =
                        (endPt->cayleyParameters[j] - startPt->cayleyParameters[j]) / 2;
            }
            ++count;
            delete (startPt);
            delete (endPt);
        }
    }

    // Calculate facet basis. Pick needed bases from cayleyParaBasisDistance/Parameter

    //cout << "Distance" << endl;
    //cout << curParaFacetBasisDistance << endl;
    //cout << "Param" << endl;
    //cout << cube->cayleyParaBasisParameter << endl;

    // Calculate hyperplane from acg and cc
    Eigen::VectorXd hyperplane(facet->dim);
    for (int i = 0; i < facet->dim; ++i)
    {
        hyperplane(i) = cc->getEdgeLength(acg->getContacts()[i].first, acg->getContacts()[i].second) -
                        facet->centre->cayleyDistances[i];
    }

    // Call linear solver
    Eigen::VectorXd Lambda = facet->cayleyParaBasisDistance.householderQr().solve(hyperplane);

    //cout << "Distance" << endl;
    //cout << facet->cayleyParaBasisDistance << endl;
    //cout << "Hyperplane" << endl;
    //cout << hyperplane << endl;
    //cout << "Lambda" << endl;
    //cout << Lambda << endl;

    // Throw out ineligible lambda
    for (int i = 0; i < Lambda.size(); ++i)
    {
        if (isnan(Lambda(i)) || Lambda(i) > 1 || Lambda(i) < -1)
        {
            return facet;
        }
    }

    // Convert Lambda to Cayley values
    Eigen::MatrixXd newBasisParameter = facet->cayleyParaBasisParameter.transpose();
    //cout << "Found Cayley intersection: ";
    for (int i = 0; i < 6 - facet->dim; ++i)
    {
        facet->foundCayleyParametersForParaMethod.emplace_back(
                (newBasisParameter * Lambda)(i) + facet->centre->cayleyParameters[i]);
        //cout << ret[i] << " ";
    }
    //cout << endl;
    return facet;
}

void UniformCartesianSampler::calcCayleyParaBasis(CartesianHypercube *cube)
{
    cartesian2Cayley(cube->centre);
    size_t ndim = acg->getDim();
    cube->cayleyParaBasisDistance = Eigen::MatrixXd(6, 6 - ndim);
    cube->cayleyParaBasisParameter = Eigen::MatrixXd(6, ndim);
    for (int i = 0; i < 6; ++i)
    {
        CartesianPoint *startingPoint = cube->get5dFacetCentre(i, false);
        CartesianPoint *endingPoint = cube->get5dFacetCentre(i, true);
        cartesian2Cayley(startingPoint);
        cartesian2Cayley(endingPoint);

        for (int j = 0; j < 6 - ndim; ++j)
        {
            cube->cayleyParaBasisDistance(i, j) =
                    (endingPoint->cayleyDistances[j] - startingPoint->cayleyDistances[j]) / 2;
        }
        for (int j = 0; j < ndim; ++j)
        {
            cube->cayleyParaBasisParameter(i, j) =
                    (endingPoint->cayleyParameters[j] - startingPoint->cayleyParameters[j]) / 2;
        }
        delete (startingPoint);
        delete (endingPoint);
    }
}

int UniformCartesianSampler::sampleAndAnalyseEachFlip()
{
    for (int i = 0; i < 8; ++i)
    {
        cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
        cout << "Sampling flip " << i << endl;

        this->initFlip(i);
        cout << "Initial valid points from previous sampling: " << this->originalPointCount[i] << endl;

        // add neighbours of cubes with previous sampled points to traverse
        this->getAllCubesToSampleFromCurrentValidPoints(false);
        int countBeforeSampling = this->goodFrontierCubes.size();

        // traverse and sample
        this->sampleCurrentFlip();

        this->storeMapAndClearPointBuffer();

        int countAfterSampling = this->mappedValidCubeCount[this->curFlipNo];

        this->reportFlip(this->curFlipNo);

        double growthRate = double(countAfterSampling) / countBeforeSampling;

        cout << "Volume growth: " << growthRate << endl;
    }
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
    int totalVolume = this->mappedValidCubeCount[0] + this->mappedValidCubeCount[1] + this->mappedValidCubeCount[2] +
                      this->mappedValidCubeCount[3] + this->mappedValidCubeCount[4] + this->mappedValidCubeCount[5] +
                      this->mappedValidCubeCount[6] + this->mappedValidCubeCount[7];
    cout << "Total volume: " << totalVolume << endl;
    return totalVolume;
}

void UniformCartesianSampler::storeMapAndClearPointBuffer()
{
    if (this->verbose)
    {
        cout << "Storing and mapping points in buffer." << endl;
    }
    snl->saveCartesianPointsInBuffer(this->node, this->pointBuffer);
    for (auto &cartesianPoint: this->pointBuffer)
    {
        auto relativeCoord = mapPointToCube(cartesianPoint->cartesianCoord, this->origin, this->stepSize);
        switch (cartesianPoint->status)
        {
            case 5:
                break;
            case 4:
            {
                if (this->allMappedValidCubesRelativeCoord.insert(relativeCoord).second)
                {
                    ++this->mappedValidCubeCount[this->curFlipNo];
                }
                if (this->allMappedBadAngleCubesRelativeCoord.erase(relativeCoord))
                {
                    --this->mappedBadAngleCubeCount[this->curFlipNo];
                }
                else if (this->allMappedCollisionCubesRelativeCoord.erase(relativeCoord))
                {
                    --this->mappedCollisionCubeCount[this->curFlipNo];
                }
                else if (this->allMappedNotRealizableCubesRelativeCoord.erase(relativeCoord))
                {
                    --this->mappedNotRealizableCubeCount[this->curFlipNo];
                }
                break;
            }
            case 3:
            {
                if (this->allMappedBadAngleCubesRelativeCoord.insert(relativeCoord).second)
                {
                    ++this->mappedBadAngleCubeCount[this->curFlipNo];
                }
                if (this->allMappedCollisionCubesRelativeCoord.erase(relativeCoord))
                {
                    --this->mappedCollisionCubeCount[this->curFlipNo];
                }
                else if (this->allMappedNotRealizableCubesRelativeCoord.erase(relativeCoord))
                {
                    --this->mappedNotRealizableCubeCount[this->curFlipNo];
                }
                break;
            }
            case 2:
            {
                if (this->allMappedCollisionCubesRelativeCoord.insert(relativeCoord).second)
                {
                    ++this->mappedCollisionCubeCount[this->curFlipNo];
                }
                if (this->allMappedNotRealizableCubesRelativeCoord.erase(relativeCoord))
                {
                    --this->mappedNotRealizableCubeCount[this->curFlipNo];
                }
                break;
            }
            case 1:
            {
                if (this->allMappedNotRealizableCubesRelativeCoord.insert(relativeCoord).second)
                {
                    ++this->mappedNotRealizableCubeCount[this->curFlipNo];
                }
                break;
            }
            default:
            {
                cout << "Error: wrong point status when saving." << endl;
                throw;
            }
        }
    }
    for (auto &curPtToDel: this->pointBuffer)
    {
        delete (curPtToDel);
    }

    //cout << "Current mapped cubes: " << this->allMappedValidCubesRelativeCoord.size() << endl;

    this->pointBuffer.clear();
    this->savePointCounter = 0;
}

void UniformCartesianSampler::storeResultCubes(bool storeAsBaseline)
{
    unordered_set<vector<int>, vector_int_hash, vector_int_eq> foundCubes;
    vector<CartesianHypercube *> cubesToWrite;

    ofstream baselineFile;
    if (storeAsBaseline)
    {
        stringstream fileName;
        fileName << "baseline.txt";
        baselineFile.open((fileName.str()).c_str(), ios_base::trunc | ios_base::out);
    }
    for (auto &relativeCoord: this->allMappedValidCubesRelativeCoord)
    {
        auto result = foundCubes.insert(relativeCoord);
        if (result.second) // New cube found
        {
            auto cube = getCubeFromRelCoord(relativeCoord);
            cube->status = 4;
            cube->stepSize = this->stepSize;
            cube->flipNum = this->curFlipNo;
            cubesToWrite.push_back(cube);
        }
    }

    auto cubeCount = cubesToWrite.size();

    if (storeAsBaseline)
    {
        for (int i = 0; i < 6; ++i)
        {
            baselineFile << this->stepSize[i] << " ";
        }
        baselineFile << endl;
    }

    /*
    // First map all valid points
    for (auto &cartesianPoint: this->allValidPointsCartesianCoord)
    {
        auto relCoord = calcRelativeCoord(cartesianPoint);
        auto result = foundCubes.insert(relCoord);
        if (result.second) // New cube found
        {
            auto cube = getCubeFromRelativeCoord(relCoord);
            cube->status = 4;
            cube->stepSize = this->stepSize;
            auto corrPt = new CartesianPoint(cartesianPoint);
            cube->corrPoint = corrPt;
            cartesian2Cayley(corrPt);
            cube->curFlipNo = getFlipNo(cartesianPoint);
            cubesToWrite.push_back(cube);
        }
    }
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;

    for (auto &cartesianPoint: this->allBadAnglePointsCartesianCoord)
    {
        auto relCoord = calcRelativeCoord(cartesianPoint);
        auto result = foundCubes.insert(relCoord);
        if (result.second) // New cube found
        {
            auto cube = getCubeFromRelativeCoord(relCoord);
            cube->status = 3;
            cube->stepSize = this->stepSize;
            auto corrPt = new CartesianPoint(cartesianPoint);
            cube->corrPoint = corrPt;
            cartesian2Cayley(corrPt);
            cube->curFlipNo = getFlipNo(cartesianPoint);
            cubesToWrite.push_back(cube);
        }
    }

    for (auto &cartesianPoint: this->allCollisionPointsCartesianCoord)
    {
        auto relCoord = calcRelativeCoord(cartesianPoint);
        auto result = foundCubes.insert(relCoord);
        if (result.second) // New cube found
        {
            auto cube = getCubeFromRelativeCoord(relCoord);
            cube->status = 2;
            cube->stepSize = this->stepSize;
            auto corrPt = new CartesianPoint(cartesianPoint);
            cube->corrPoint = corrPt;
            cartesian2Cayley(corrPt);
            cube->curFlipNo = getFlipNo(cartesianPoint);
            cubesToWrite.push_back(cube);
        }
    }

    for (auto &cartesianPoint: this->allNotRealizablePointsCartesianCoord)
    {
        auto relCoord = mapPointToCube(cartesianPoint);
        auto result = foundCubes.insert(relCoord);
        if (result.second) // New cube found
        {
            auto cube = getCubeFromRelCoord(relCoord);
            cube->status = 1;
            cube->stepSize = this->stepSize;
            auto corrPt = new CartesianPoint(cartesianPoint);
            cube->corrPoint = corrPt;
            cube->curFlipNo = calcFlipWithMapBack(cartesianPoint);
            cubesToWrite.push_back(cube);
        }
    }*/

    if (!cubesToWrite.empty())
    {
        this->snl->saveCartesianHypercubesInList(this->node, cubesToWrite);
        for (auto &cube: cubesToWrite)
        {
            if (storeAsBaseline)
            {
                for (int i = 0; i < 6; ++i)
                {
                    baselineFile << cube->centre->cartesianCoord[i] << " ";
                }
                baselineFile << endl;
            }
            delete (cube->centre);
            //delete (cube->corrPoint);
            delete (cube);
        }
    }

    if (storeAsBaseline)
    {
        baselineFile << cubeCount;
        baselineFile.flush();
        baselineFile.close();
    }
}

void UniformCartesianSampler::getAllCubesToSampleFromCurrentValidPoints(bool startFromPoint)
{
    cout << "Mapping current valid points into cubes." << endl;
    vector<CartesianPoint *> points;
    vector<CartesianHypercube *> cubes;
    this->snl->loadCartesian(this->node->getID(), points, false, cubes);

    for (auto &point: points)
    {
        if ((point->status == 5 || point->status == 4) && point->flipNo == this->curFlipNo)
        {
            auto currentCubeRelativeCoord = mapPointToCube(point->cartesianCoord, this->origin, this->stepSize);

            auto insertResult = this->goodFrontierCubes.insert(
                    make_pair(currentCubeRelativeCoord, vector<bool>(12, true)));
            if (insertResult.second)
            {
                this->cubesToTraverse.push(currentCubeRelativeCoord);
            }
        }
        delete (point);
    }
}

void UniformCartesianSampler::sampleCurrentFlip()
{
    this->validPointCount[this->curFlipNo] = 0;
    this->badAnglePointCount[this->curFlipNo] = 0;
    this->collisionPointCount[this->curFlipNo] = 0;
    this->sampledValidCubeCount[this->curFlipNo] = 0;
    this->sampledBadAngleCubeCount[this->curFlipNo] = 0;
    this->sampledCollisionCubeCount[this->curFlipNo] = 0;
    this->sampledNotRealizableCubeCount[this->curFlipNo] = 0;
    this->sampledNoIntersectionCubeCount[this->curFlipNo] = 0;
    this->visitedCubeCount[this->curFlipNo] = 0;
    this->visitedFacetCount[this->curFlipNo] = 0;
    this->visitedSimplexCount[this->curFlipNo] = 0;
    this->pointCount[this->curFlipNo] = 0;

    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
    cout << "Start sampling procedure. " << endl;
    cout << "Sampling with distance step size " << this->stepSize[0] << " " << this->stepSize[1]
         << " " << this->stepSize[2];
    cout << " and angular step count " << this->angularStepCount[0] << " " << this->angularStepCount[1] << " "
         << this->angularStepCount[2] << endl;

    cout << "Start sampling with " << this->goodFrontierCubes.size() << " valid cubes" << endl;
    cout << "Sampling with ";
    if (this->mode == 0)
    {
        cout << "6-d simplex ";
    }
    else if (this->mode == 1)
    {
        cout << "basis ";
    }
    else if (this->mode == 2)
    {
        cout << "5d thickness enhance ";
    }
    else if (this->mode == 3)
    {
        cout << "5-d simplex ";
    }
    else if (this->mode == 4)
    {
        cout << "6-d and 5-d hybrid ";
    }
    cout << "method." << endl;
    cout << "Skip threshold set to " << this->skipThreshold << endl;

    while (!this->cubesToTraverse.empty())
    {
        ++this->visitedCubeCount[this->curFlipNo];

        auto currentRelCoord = this->cubesToTraverse.top();
        this->cubesToTraverse.pop();

        if (this->mode == 0)
        {
            this->curCube = intersectHypercubeUsing6DSimplexMethod(currentRelCoord);
        }
        else if (this->mode == 1)
        {
            this->curCube = intersectHypercubeUsingBasisMethod(currentRelCoord);
        }
        else if (this->mode == 2) // 5D enhance mode
        {
            if (this->acg->getDim() == 5)
            {
                this->curCube = intersectHypercubeThick(currentRelCoord, true);
            }
            else
            {
                cout << "UniformCartesianSampler::thickness enhance mode can only be applied on 5D nodes." << endl;
                throw;
            }
        }
        else if (this->mode == 3)
        {
            this->curCube = intersectHypercubeUsing5DSimplexMethod(currentRelCoord);
        }
        else if (this->mode == 4)
        {
            this->curCube = intersectHypercubeUsingHybridSimplexMethod(currentRelCoord);
        }
        size_t status = this->curCube->status;

        for (auto &cartesianCoord: this->curCube->foundValidCartesianPoints)
        {
            ++this->pointCount[this->curFlipNo];
            ++this->validPointCount[this->curFlipNo];

            auto pt = new CartesianPoint(cartesianCoord);
            cartesian2Cayley(pt);
            pt->status = 4;
            pt->flipNo = this->curFlipNo;
            this->pointBuffer.push_back(pt);
            ++this->savePointCounter;
        }

        if (this->skipThreshold <= 3)
        {
            for (auto &cartesianCoord: this->curCube->foundBadAngleCartesianPoints)
            {
                ++this->pointCount[this->curFlipNo];
                ++this->badAnglePointCount[this->curFlipNo];

                auto pt = new CartesianPoint(cartesianCoord);
                cartesian2Cayley(pt);
                pt->status = 3;
                pt->flipNo = this->curFlipNo;
                this->pointBuffer.push_back(pt);
                ++this->savePointCounter;
            }
        }

        if (this->skipThreshold <= 2)
        {
            for (auto &cartesianCoord: this->curCube->foundCollisionCartesianPoints)
            {
                ++this->pointCount[this->curFlipNo];
                ++this->collisionPointCount[this->curFlipNo];

                auto pt = new CartesianPoint(cartesianCoord);
                cartesian2Cayley(pt);
                pt->status = 2;
                pt->flipNo = this->curFlipNo;
                this->pointBuffer.push_back(pt);
                ++this->savePointCounter;
            }
        }

        // For unrealizable cube, store its Cartesian centre.
        if (status == 1)
        {
            auto pt = new CartesianPoint(this->curCube->centre);
            pt->status = 1;
            pt->flipNo = this->curFlipNo;
            this->pointBuffer.push_back(pt);
            ++this->savePointCounter;
        }

        if (savePointCounter >= savePointFreq)
        {
            storeMapAndClearPointBuffer();
        }

        if (status == 4)
        {
            ++this->sampledValidCubeCount[this->curFlipNo];
        }
        else if (status == 3)
        {
            ++this->sampledBadAngleCubeCount[this->curFlipNo];
        }
        else if (status == 2)
        {
            ++this->sampledCollisionCubeCount[this->curFlipNo];
        }
        else if (status == 1)
        {
            ++this->sampledNotRealizableCubeCount[this->curFlipNo];
        }
        else // status == 0
        {
            ++this->sampledNoIntersectionCubeCount[this->curFlipNo];
        }

        delete (curCube->centre);
        delete (curCube);
    }

    if (!this->pointBuffer.empty())
    {
        storeMapAndClearPointBuffer();
    }

    this->badFrontierCubes.clear();

    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
    cout << "Sampling finished. " << endl;
    cout << "Valid cubes: " << this->sampledValidCubeCount[this->curFlipNo] << endl;
    cout << "Valid points: " << this->validPointCount[this->curFlipNo] << endl;
    cout << "Bad angle cubes: " << this->sampledBadAngleCubeCount[this->curFlipNo] << endl;
    cout << "Bad angle points: " << this->badAnglePointCount[this->curFlipNo] << endl;
    cout << "Collision cubes: " << this->sampledCollisionCubeCount[this->curFlipNo] << endl;
    cout << "Collision points: " << this->collisionPointCount[this->curFlipNo] << endl;
    cout << "Unrealizable: " << this->sampledNotRealizableCubeCount[this->curFlipNo] << endl;
    cout << "No intersection: " << this->sampledNoIntersectionCubeCount[this->curFlipNo] << endl;
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
    cout << "Sampled cubes: " << this->visitedCubeCount[this->curFlipNo] << endl;
    cout << "Sampled facets: " << this->visitedFacetCount[this->curFlipNo] << endl;
    cout << "Sampled simplices: " << this->visitedSimplexCount[this->curFlipNo] << endl;
    cout << "New points found: " << this->pointCount[this->curFlipNo] << endl;
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
}


CartesianHypercube *UniformCartesianSampler::getCubeFromRelCoord(vector<int> relCoord)
{
    vector<double> centreCoord;
    for (int i = 0; i < 6; ++i)
    {
        double curEntry = this->origin[i] + relCoord[i] * this->stepSize[i] / 2;
        // angular must remain between -\pi and \pi
        if (i >= 3 && i <= 5)
        {
            if (curEntry > PI)
            {
                curEntry -= 2 * PI;
            }
            if (curEntry < -PI)
            {
                curEntry += 2 * PI;
            }
        }
        centreCoord.emplace_back(curEntry);
    }
    auto centrePoint = new CartesianPoint(centreCoord);
    return new CartesianHypercube(centrePoint, this->stepSize);
}

/*
CartesianFacet *UniformCartesianSampler::getFacetFromRelativeCoord(vector<int> cubeRelativeCoord, int dim, int facetNo)
{
    vector<int> ter = Cartesian::signedDec2ter(facetNo);
    if (count(ter.begin(), ter.end(), 1) == dim)
    {
        vector<double> centreCoord;
        vector<double> facetStepSize;
        for (int i = 0; i < 6; ++i)
        {
            if (ter[i] == 0)
            {
                centreCoord.emplace_back(
                        this->origin_[i] + cubeRelativeCoord[i] * this->stepSize_[i] - this->stepSize_[i] / 2);
                facetStepSize.emplace_back(0);
            }
            else if (ter[i] == 1)
            {
                centreCoord.emplace_back(this->origin_[i] + cubeRelativeCoord[i] * this->stepSize_[i]);
                facetStepSize.emplace_back(this->stepSize_[i]);
            }
            else // ter[i] == 2
            {
                centreCoord.emplace_back(
                        this->origin_[i] + cubeRelativeCoord[i] * this->stepSize_[i] + this->stepSize_[i] / 2);
                facetStepSize.emplace_back(0);
            }
        }
        auto centrePoint = new CartesianPoint(centreCoord);
        return new CartesianFacet(centrePoint, dim, facetStepSize);
    }
    else
    {
        return nullptr;
    }
}
*/

vector<int>
UniformCartesianSampler::mapPointToCube(vector<double> cartesianCoord, vector<double> origin_, vector<double> stepSize_)
{
    if (cartesianCoord.size() != 6)
    {
        cout << "Error: Cartesian point does not have 6 coord entries." << endl;
        throw;
    }
    vector<int> ret;
    for (int i = 0; i < 6; ++i)
    {
        double diff = (cartesianCoord[i] - origin_[i]) / stepSize_[i];
        bool flag = diff < 0;
        int diff_int;
        if ((diff <= .5) && (diff >= -.5))
        {
            diff_int = 0;
        }
        else
        {
            diff = abs(diff) + .5;
            diff_int = int(diff);
        }
        if (flag)
        {
            diff_int = -diff_int;
        }

        // Additional handling for angle part
        if (i >= 3)
        {
            int curStepCount = angularStepCount[i - 3];
            // Even num of angular steps
            // From -n / 2 to n / 2 - 1
            if (curStepCount % 2 == 0)
            {
                while (diff_int > curStepCount / 2 - 1)
                {
                    diff_int -= curStepCount;
                }
                while (diff_int < -curStepCount / 2)
                {
                    diff_int += curStepCount;
                }
            }
                // Odd num of angular steps
                // From -n / 2 to n / 2
            else
            {
                while (diff_int > curStepCount / 2)
                {
                    diff_int -= curStepCount;
                }
                while (diff_int < -curStepCount / 2)
                {
                    diff_int += curStepCount;
                }
            }
        }

        diff_int *= 2;
        ret.emplace_back(diff_int);
    }
    return ret;
}

vector<vector<int>>
UniformCartesianSampler::getAllNeighbourRelCoord(vector<int> currentRelCoord, bool diagonal)
{
    vector<vector<int>> ret;
    // Using diagonal neighbour, each cube has 729 neighbours
    if (diagonal)
    {
        for (int i = -1; i < 2; ++i)
        {
            for (int j = -1; j < 2; ++j)
            {
                for (int k = -1; k < 2; ++k)
                {
                    for (int l = -1; l < 2; ++l)
                    {
                        for (int m = -1; m < 2; ++m)
                        {
                            for (int n = -1; n < 2; ++n)
                            {
                                auto nextRelCoord = currentRelCoord;
                                int delta[6] = {i, j, k, l, m, n};
                                for (int index = 0; index < 6; ++index)
                                {
                                    nextRelCoord[index] += delta[index] * 2;
                                }
                                nextRelCoord = fixAngularEntry(nextRelCoord);
                                ret.emplace_back(nextRelCoord);
                            }
                        }
                    }
                }
            }
        }
    }
        // Not using diagonal neighbour, each cube has 12 neighbours
    else
    {
        for (int i = 0; i < 12; ++i)
        {
            auto nextRelCoord = currentRelCoord; // next cube
            int index = i % 6; // index of relative coord to be changed
            if (i >= 6)
            {
                nextRelCoord[index] += 2;
            }
            else
            {
                nextRelCoord[index] -= 2;
            }
            nextRelCoord = fixAngularEntry(nextRelCoord);
            ret.emplace_back(nextRelCoord);
        }
    }
    return ret;
}

vector<int> UniformCartesianSampler::fixAngularEntry(vector<int> currentRelCoord)
{
    vector<int> ret = std::move(currentRelCoord);
    for (int i = 0; i < 3; ++i)
    {
        int count = this->angularStepCount[i];
        int top;
        int bot;
        // Even number of angular steps
        if (count % 2 == 0)
        {
            top = count - 2;
            bot = -count;
        }
            // Odd number of angular steps
        else
        {
            top = count - 1;
            bot = -count + 1;
        }

        if (ret[i + 3] > top)
        {
            ret[i + 3] -= 2 * count;
        }
        if (ret[i + 3] < bot)
        {
            ret[i + 3] += 2 * count;
        }
    }
    return ret;
}

Orientation *
UniformCartesianSampler::computeRealization(int flipNum, vector<double> cayleyParamValue, bool &fail)
{
    bool realizable;
    double *fromA[3], *fromB[3];
    double *toA[3], *toB[3];
    double positions[12][3]; // stores position of 12 key points
    double tempParamLength[12][12]; // stores all pairwise distances for 12 key points

    // extended mode: cayleyParamValue has both distances and parameters.
    if (cayleyParamValue.size() == 6)
    {
        // use tempParamLength.edge_length to calculate distance in each molecule
        for (int i = 0; i < 6; ++i)
        {
            for (int j = 0; j < 6; ++j)
            {
                tempParamLength[i][j] = cc->getEdgeLength(i, j);
                tempParamLength[i + 6][j + 6] = cc->getEdgeLength(i + 6, j + 6);
                tempParamLength[i][j + 6] = -1;
                tempParamLength[i + 6][j] = -1;
            }
        }

        int ndim = node->getDim();
        // first distances
        for (int i = 0; i < 6 - ndim; ++i)
        {
            tempParamLength[acg->getContacts()[i].first][acg->getContacts()[i].second] = cayleyParamValue[i];
            tempParamLength[acg->getContacts()[i].second][acg->getContacts()[i].first] = cayleyParamValue[i];
        }
        // then parameters
        for (int i = 0; i < ndim; ++i)
        {
            tempParamLength[acg->getParameters()[i].first][acg->getParameters()[i].second] =
                    cayleyParamValue[i + 6 -
                                     ndim];
            tempParamLength[acg->getParameters()[i].second][acg->getParameters()[i].first] =
                    cayleyParamValue[i + 6 -
                                     ndim];
        }
    }
        // regular: parameters only
    else
    {
        for (int i = 0; i < 12; ++i)
        {
            for (int j = 0; j < 12; ++j)
            {
                tempParamLength[i][j] = cc->getEdgeLength(i, j);
            }
        }

        // add newly calculated Cayley non-edge length
        for (int i = 0; i < cayleyParamValue.size(); ++i)
        {
            tempParamLength[acg->getParameters()[i].first][acg->getParameters()[i].second] = cayleyParamValue[i];
            tempParamLength[acg->getParameters()[i].second][acg->getParameters()[i].first] = cayleyParamValue[i];
        }
    }

    vector<vector<int>> tempTetrahedra;

    fromA[0] = cc->getMolA()->getAtomAt(cc->getVerticesA()[0])->getLocation();
    fromA[1] = cc->getMolA()->getAtomAt(cc->getVerticesA()[1])->getLocation();
    fromA[2] = cc->getMolA()->getAtomAt(cc->getVerticesA()[2])->getLocation();

    fromB[0] = cc->getMolB()->getAtomAt(cc->getVerticesB()[0])->getLocation();
    fromB[1] = cc->getMolB()->getAtomAt(cc->getVerticesB()[1])->getLocation();
    fromB[2] = cc->getMolB()->getAtomAt(cc->getVerticesB()[2])->getLocation();
    realizable = true;

    for (auto &position: positions)
    {
        position[0] = -111;
        position[1] = -111;
        position[2] = -111;
    }

    vector<bool> mirrorlist;
    mirrorlist.push_back(flipNum % 2 == 1);
    mirrorlist.push_back((flipNum / 2) % 2 == 1);
    mirrorlist.push_back((flipNum / 4) % 2 == 1);

    for (int i = 0; i < 3; i++)
    {
        toA[i] = new double[3];
        toB[i] = new double[3];
    }

    tempTetrahedra = cc->getTetras();

    size_t mirrorIndex = 1;
    if (cc->setBaseTetra(mirrorlist[0], realizable, tempTetrahedra, positions,
                         tempParamLength)) //if indices are in the same helix , then no need for mirror
    {
        mirrorIndex = 0;
    }

    if (!realizable)
    {
        //cout << "Compute realization fail: could not set base tetrahedron" << endl;
        fail = true;
        delete (toA[0]);
        delete (toA[1]);
        delete (toA[2]);
        delete (toB[0]);
        delete (toB[1]);
        delete (toB[2]);
        return nullptr;
    }


    //builds new locations onto the base tetrahedron.
    size_t tetraIndex = 1;
    do
    {
        if (!cc->locateVertex(tetraIndex, mirrorlist[mirrorIndex], realizable, tempTetrahedra, positions,
                              tempParamLength)) //if locateVertex is true, then indices are in the same helix, then no need for mirror and volume is positive
        {
            if (!realizable)
            {
                //cout << "Compute realization fail: locate vertex failed on tet " << tetraIndex << endl;
                fail = true;
                delete (toA[0]);
                delete (toA[1]);
                delete (toA[2]);
                delete (toB[0]);
                delete (toB[1]);
                delete (toB[2]);
                return nullptr;
            }
            cc->computeLengthOfTheEdgesConnectedToVertex(cc->getTetras()[tetraIndex][0], positions,
                                                         tempParamLength);
            mirrorIndex++;
        }
        tetraIndex++;
    }
    while (tetraIndex < tempTetrahedra.size());
    //now it should be able to fill all the values of vertices, else it means it needs maple

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            toA[i][j] = positions[i][j];
            toB[i][j] = positions[i + 6][j];
            if (toA[i][j] == -111 || toB[i][j] == -111 || std::isnan(toA[i][j])
                || std::isnan(toB[i][j]))
            {
                cout << "eroorrrr toA[i][j]== -111 || toB[i][j]== -111" << endl;
            }
        }
    }

    if (cc->isDistorted(toA, fromA))
    {
        //cout << "Compute realization fail: distorted" << endl;
        realizable = false;
        fail = true;
        delete (toA[0]);
        delete (toA[1]);
        delete (toA[2]);
        delete (toB[0]);
        delete (toB[1]);
        delete (toB[2]);
        return nullptr;
    }

    // TRANSLATE HELIX B ACCORDING TO HELIX A
    for (auto &j: toB)
    { //translates toB[j]
        double pot[3];
        Utils::matApp(j, toA[0], toA[1], toA[2], fromA[0], fromA[1],
                      fromA[2], pot);

        for (int i = 0; i < 3; i++)
        {
            j[i] = pot[i];
        }
    }

    // FIX HELIX A
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            toA[i][j] = fromA[i][j];
        }
    }


    auto ori_output = new Orientation(fromB, toB);
    ori_output->setFlipNum(flipNum);
    delete (toA[0]);
    delete (toA[1]);
    delete (toA[2]);
    delete (toB[0]);
    delete (toB[1]);
    delete (toB[2]);
    return ori_output;
}

void UniformCartesianSampler::analyseCube(CartesianHypercube *cube)
{
    cube->allCartesianFacetCentresInCartesian = vector<vector<double >>(729);
    cube->allCartesianFacetCentresInCayley = vector<vector<double >>(729);
    cube->allCayleyFacetCentresInCayley = vector<vector<double >>(729);
    cube->allCayleyFacetCentresInCartesian = vector<vector<double >>(729);

    // Handle Cartesian facet centres
    for (int i = 0; i < 729; ++i)
    {
        auto relCoords = Cartesian::unsignedDec2ter(i);
        auto cartesianCoord = cube->centre->cartesianCoord;
        cartesianCoord[0] += (relCoords[0] - 1) * cube->stepSize[0] / 2;
        cartesianCoord[1] += (relCoords[1] - 1) * cube->stepSize[1] / 2;
        cartesianCoord[2] += (relCoords[2] - 1) * cube->stepSize[2] / 2;
        cartesianCoord[3] += (relCoords[3] - 1) * cube->stepSize[3] / 2;
        cartesianCoord[4] += (relCoords[4] - 1) * cube->stepSize[4] / 2;
        cartesianCoord[5] += (relCoords[5] - 1) * cube->stepSize[5] / 2;

        cube->allCartesianFacetCentresInCartesian[i] = cartesianCoord;
        auto cartesianPoint = new CartesianPoint(cartesianCoord);
        cartesian2Cayley(cartesianPoint);

        cube->allCartesianFacetCentresInCayley[i] = cartesianPoint->cayleyDistances;
        cube->allCartesianFacetCentresInCayley[i].insert(cube->allCartesianFacetCentresInCayley[i].end(),
                                                         cartesianPoint->cayleyParameters.begin(),
                                                         cartesianPoint->cayleyParameters.end());
    }

    // map 0-d vertices to Cayley
    for (int i = 0; i < 729; ++i)
    {
        auto relCoords = Cartesian::unsignedDec2ter(i);
        int dimCount = (relCoords[0] == 1) + (relCoords[1] == 1) + (relCoords[2] == 1) +
                       (relCoords[3] == 1) + (relCoords[4] == 1) + (relCoords[5] == 1);
        if (dimCount == 0)
        {
            cube->allCayleyFacetCentresInCayley[i] = cube->allCartesianFacetCentresInCayley[i];
            cube->allCayleyFacetCentresInCartesian[i] = cube->allCartesianFacetCentresInCartesian[i];
        }
    }

    for (int i = 0; i < 729; ++i)
    {
        auto relCoords = Cartesian::unsignedDec2ter(i);
        int dimCount = (relCoords[0] == 1) + (relCoords[1] == 1) + (relCoords[2] == 1) +
                       (relCoords[3] == 1) + (relCoords[4] == 1) + (relCoords[5] == 1);
        if (dimCount != 0)
        {
            int start = 0;
            int end = 0;

            for (int j = 0; j < 6; ++j)
            {

                if (relCoords[j] == 1)
                {
                    end += 2 * POW3[j];
                }

                else if (relCoords[j] == 2)
                {
                    start += 2 * POW3[j];
                    end += 2 * POW3[j];
                }
            }

            cube->allCayleyFacetCentresInCayley[i] = Cartesian::getMidPoint(
                    cube->allCartesianFacetCentresInCayley[start], cube->allCartesianFacetCentresInCayley[end]);
            bool fail = false;
            auto ori = computeRealization(this->curFlipNo, cube->allCayleyFacetCentresInCayley[i], fail);
            if (!fail)
            {
                cube->allCayleyFacetCentresInCartesian[i] = ori2Cartesian(ori);
            }
        }

        /*
        if (dimCount == 0)
        {
            cout << relCoords[0] << "," << relCoords[1] << "," << relCoords[2] << ","
                 << relCoords[3] << "," << relCoords[4] << "," << relCoords[5] << endl;
            auto cartesianCoord = cube->allCartesianFacetCentresInCartesian[i];
            auto point = new CartesianPoint(cartesianCoord);
            cartesian2Cayley(point);
            auto cayleyCoord = cube->allCayleyFacetCentresInCayley[i];
            bool fail = false;
            Orientation oris[8];
            for (int i = 0; i < 8; ++i)
            {
                auto ori = computeRealization(i, cayleyCoord, fail);
                if (!fail)
                {
                    auto cartesianBack = ori2Cartesian(ori);
                    auto pointBack = new CartesianPoint(cartesianBack);
                    auto dist = Cartesian::cartesianDist(point, pointBack);
                    cout << relCoords[0] << "," << relCoords[1] << "," << relCoords[2] << ","
                         << relCoords[3] << "," << relCoords[4] << "," << relCoords[5] << endl;
                    cout << get<0>(dist) << "," << get<1>(dist) << endl;
                }
                else
                {
                    cout << "fail in flip " << i << endl;
                }
            }
        }*/
    }
}


bool UniformCartesianSampler::isPointInFlipWithMapBack(vector<double> cartesianCoord, int checkFlipNum)
{
    auto pt = new CartesianPoint(std::move(cartesianCoord));
    cartesian2Cayley(pt);

    bool fail = false;
    vector<double> allCayleyEntries = pt->cayleyDistances;
    allCayleyEntries.insert(allCayleyEntries.end(), pt->cayleyParameters.begin(), pt->cayleyParameters.end());
    auto ori = computeRealization(checkFlipNum, allCayleyEntries, fail);
    if (!fail)
    {
        auto back = ori2Cartesian(ori);
        auto backpt = new CartesianPoint(back);
        cartesian2Cayley(backpt);
        auto dist = Cartesian::cartesianDist(pt, backpt);
        //cout << curFlipNum << ", " << get<0>(dist) << ", " << get<1>(dist) << endl;
        if (get<0>(dist) < TOL_EQUAL)
        {
            delete (backpt);
            return true;
        }
    }
    delete (ori);
    delete (pt);
    return false;
}

bool UniformCartesianSampler::checkPointInFlipUsingTetVol(vector<double> cartesianCoord, int checkFlipNum)
{
    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    vector<pair<int, int>> constraints = acg->getParticipants();
    vector<pair<int, int>> parameters = acg->getParamLines();

    // Calculate Cartesian location of each atom in 2 molecules.
    vector<tuple<double, double, double>> posA, posB;

    // Atoms in molecule A remains unchanged.
    vector<Atom *> atomA = molA->getAtoms();
    auto atomNumA = atomA.size();
    for (int i = 0; i < atomNumA; ++i)
    {
        tuple<double, double, double> curPos(atomA[i]->getLocation()[0],
                                             atomA[i]->getLocation()[1],
                                             atomA[i]->getLocation()[2]);
        posA.push_back(curPos);
    }

    // Calculate position of atoms in molecule B.
    // Translation
    Vector3d transVec(cartesianCoord[0], cartesianCoord[1], cartesianCoord[2]);
    // Rotation
    Matrix3d rotMat;
    rotMat(0, 0) = cos(cartesianCoord[3]) * cos(cartesianCoord[4]);
    rotMat(0, 1) = sin(cartesianCoord[3]) * cos(cartesianCoord[4]);
    rotMat(0, 2) = -sin(cartesianCoord[4]);
    rotMat(1, 0) = cos(cartesianCoord[3]) * sin(cartesianCoord[4]) * sin(cartesianCoord[5])
                   - sin(cartesianCoord[3]) * cos(cartesianCoord[5]);
    rotMat(1, 1) = sin(cartesianCoord[3]) * sin(cartesianCoord[4]) * sin(cartesianCoord[5])
                   + cos(cartesianCoord[3]) * cos(cartesianCoord[5]);
    rotMat(1, 2) = cos(cartesianCoord[4]) * sin(cartesianCoord[5]);
    rotMat(2, 0) = cos(cartesianCoord[3]) * sin(cartesianCoord[4]) * cos(cartesianCoord[5])
                   + sin(cartesianCoord[3]) * sin(cartesianCoord[5]);
    rotMat(2, 1) = sin(cartesianCoord[3]) * sin(cartesianCoord[4]) * cos(cartesianCoord[5])
                   - cos(cartesianCoord[3]) * sin(cartesianCoord[5]);
    rotMat(2, 2) = cos(cartesianCoord[4]) * cos(cartesianCoord[5]);

    vector<Atom *> atomB = molB->getAtoms();
    auto atomNumB = atomB.size();
    for (int i = 0; i < atomNumB; ++i)
    {
        Vector3d originVec(atomB[i]->getLocation()[0],
                           atomB[i]->getLocation()[1],
                           atomB[i]->getLocation()[2]);
        Vector3d target = transVec + rotMat * originVec;
        tuple<double, double, double> curPos(target(0), target(1), target(2));
        posB.push_back(curPos);
    }


    bool flips[3];
    flips[0] = (checkFlipNum % 2 == 1);
    flips[1] = ((checkFlipNum / 2) % 2 == 1);
    flips[2] = ((checkFlipNum / 4) % 2 == 1);

    for (int i = 0; i < 3; ++i)
    {
        Vector3d v[4];
        for (int j = 0; j < 4; ++j)
        {
            int curAtom = this->node->flipScheme[i][j];
            int curPosAtom;
            if (curAtom < 6)
            {
                curPosAtom = this->acg->getVerticesA()[curAtom];
                v[j](0) = get<0>(posA[curPosAtom]);
                v[j](1) = get<1>(posA[curPosAtom]);
                v[j](2) = get<2>(posA[curPosAtom]);
            }
            else
            {
                curPosAtom = this->acg->getVerticesB()[curAtom - 6];
                v[j](0) = get<0>(posB[curPosAtom]);
                v[j](1) = get<1>(posB[curPosAtom]);
                v[j](2) = get<2>(posB[curPosAtom]);
            }
        }

        double result = ((v[3] - v[0]).cross(v[3] - v[1])).dot(v[3] - v[2]);
        double maxValue = (v[3] - v[0]).norm() * (v[3] - v[1]).norm() * (v[3] - v[2]).norm();
        double ratio = result / maxValue;
        if ((ratio > TOL_FLIP_CHECK) && !flips[i])
        {
            return false;
        }
        if ((ratio < -TOL_FLIP_CHECK) && flips[i])
        {
            return false;
        }
    }
    return true;
}


void UniformCartesianSampler::dynamicStepSizeSample(double refineRate, int roundCount)
{
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
    cout << "Dynamic step size sampling starts for node " << this->node->getID() << endl;

    for (int i = 0; i < roundCount; ++i)
    {
        this->sampleAndAnalyseEachFlip();
        // Refine on translational entries
        for (int j = 0; j < 3; ++j)
        {
            this->stepSize[j] *= refineRate;
        }

        // Refine on angular entries
        for (int j = 0; j < 3; ++j)
        {
            this->angularStepCount[j] /= refineRate;
            this->stepSize[j + 3] = 2 * PI / this->angularStepCount[j];
        }
    }
}

Matrix3d UniformCartesianSampler::euler2Rotation(double phi, double cos_theta, double psi)
{
    Matrix3d ret;

    double sin_theta = sqrt(1 - cos_theta * cos_theta);
    ret(0, 0) = cos(phi) * cos(psi) - cos_theta * sin(phi) * sin(psi);
    ret(0, 1) = sin(phi) * cos(psi) + cos_theta * cos(phi) * sin(psi);
    ret(0, 2) = sin_theta * sin(psi);
    ret(1, 0) = -cos(phi) * sin(psi) - cos_theta * sin(phi) * cos(psi);
    ret(1, 1) = -sin(phi) * sin(psi) + cos_theta * cos(phi) * cos(psi);
    ret(1, 2) = sin_theta * cos(psi);
    ret(2, 0) = sin_theta * sin(phi);
    ret(2, 1) = -sin_theta * cos(phi);
    ret(2, 2) = cos_theta;
    return ret;
}

// TODO do not use this method, use 2 separate ones below
void UniformCartesianSampler::processExperimentData(bool processMC, bool processGrid)
{
    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;

    vector<Atom *> atomsA = molA->getAtoms();
    auto atomNumA = atomsA.size();    // Calculate Cartesian location of each atom in 2 molecules.
    vector<Vector3d> posA(atomNumA);
    Vector3d meanA(0, 0, 0);

    vector<Atom *> atomsB = molB->getAtoms();
    auto atomNumB = atomsA.size();
    vector<Vector3d> posB(atomNumB);
    Vector3d meanB(0, 0, 0);

    if (processMC)
    {
        ifstream inFileMC;
        inFileMC.open("traj_file_original.dat");

        while (!inFileMC.eof())
        {
            bool finished = false;
            int currentPointNum;
            vector<double> currentPoint(6, 0);
            for (int i = 0; i < 7; ++i)
            {
                string currentEntry;
                inFileMC >> currentEntry;
                if (inFileMC.eof())
                {
                    finished = true;
                    break;
                }

                if (i == 0)
                {
                    currentPointNum = stoi(currentEntry);
                    //cout << "Processing " << currentEntry << endl;
                }
                else
                {
                    currentPoint[i - 1] = stod(currentEntry);
                }
            }

            //cout << "Current point: " << currentPoint[0] << " " << currentPoint[1] << " " << currentPoint[2] << " "
            //<< currentPoint[3] << " " << currentPoint[4] << " " << currentPoint[5] << endl;

            if (finished)
            {
                break;
            }

            // Translation
            Vector3d trans(currentPoint[0], currentPoint[1], currentPoint[2]);
            // Rotation
            Matrix3d rot = euler2Rotation(currentPoint[3], currentPoint[4], currentPoint[5]);

            for (int i = 0; i < atomNumA; ++i)
            {
                Vector3d curPos
                        (atomsA[i]->getLocation()[0], atomsA[i]->getLocation()[1], atomsA[i]->getLocation()[2]);
                posA[i] = curPos;
                meanA(0) += curPos[0];
                meanA(1) += curPos[1];
                meanA(2) += curPos[2];
            }
            meanA /= atomNumA;

            for (int i = 0; i < atomNumB; ++i)
            {
                Vector3d curPos
                        (atomsB[i]->getLocation()[0], atomsB[i]->getLocation()[1], atomsB[i]->getLocation()[2]);
                posB[i] = curPos;
                meanB(0) += curPos[0];
                meanB(1) += curPos[1];
                meanB(2) += curPos[2];
            }
            meanB /= atomNumB;

            for (int i = 0; i < atomNumB; ++i)
            {
                posB[i] = rot * (posB[i] - meanB) + meanB - meanA + trans;
            }

            int contactCount = 0;
            vector<pair<int, int>> contacts;

            for (int i = 0; i < atomNumA; ++i)
            {
                for (int j = 0; j < atomNumB; ++j)
                {
                    double curDist = sqrt((posA[i](0) - posB[j](0)) * (posA[i](0) - posB[j](0)) +
                                          (posA[i](1) - posB[j](1)) * (posA[i](1) - posB[j](1)) +
                                          (posA[i](2) - posB[j](2)) * (posA[i](2) - posB[j](2)));
                    double lowerBound = this->df->bondingLowerBound(atomsA[i], atomsB[j]);
                    double upperBound = this->df->bondingUpperBound(atomsA[i], atomsB[j]);
                    if (curDist > lowerBound && curDist < upperBound)
                    {
                        ++contactCount;
                        auto currentPair = make_pair(i, j);
                        contacts.push_back(currentPair);
                    }
                }
            }
            if (contactCount >= 1)
            {
                cout << "Point " << currentPointNum << ", " << contactCount << " contacts." << endl;
                for (auto &pair: contacts)
                {
                    cout << pair.first << ", " << pair.second << endl;
                }
            }
        }
        inFileMC.close();
    }

    if (processGrid)
    {
        ifstream inFileGrid;
        inFileGrid.open("grid_to_atlas_inner_accepted.dat");
        //inFileGrid.open("grid1");

        while (!inFileGrid.eof())
        {
            bool finished = false;
            int currentPointNum;
            Vector3d trans(0, 0, 0);
            vector<double> rotQ(4, 0);
            for (int i = 0; i < 16; ++i)
            {
                string currentEntry;
                inFileGrid >> currentEntry;

                if (inFileGrid.eof())
                {
                    finished = true;
                    break;
                }
                if (i == 8)
                {
                    currentPointNum = stoi(currentEntry);
                    cout << "Processing " << currentEntry << endl;
                }
                else if (i >= 9 && i <= 11) // first 3 entries are translation
                {
                    trans(i - 9) = stod(currentEntry);
                }
                else if (i >= 12 && i <= 15)// last 4 entries are quaternion
                {
                    rotQ[i - 12] = stod(currentEntry);
                }
            }

            if (finished)
            {
                break;
            }

            Matrix3d rot;
            // NOTE: R^-1=R^T checked. Problem not from here
            rot << 1 - 2 * rotQ[2] * rotQ[2] - 2 * rotQ[3] * rotQ[3],
                    2 * rotQ[1] * rotQ[2] + 2 * rotQ[0] * rotQ[3],
                    2 * rotQ[1] * rotQ[3] - 2 * rotQ[0] * rotQ[2],
                    2 * rotQ[1] * rotQ[2] - 2 * rotQ[0] * rotQ[3],
                    1 - 2 * rotQ[1] * rotQ[1] - 2 * rotQ[3] * rotQ[3],
                    2 * rotQ[2] * rotQ[3] + 2 * rotQ[0] * rotQ[1],
                    2 * rotQ[1] * rotQ[3] + 2 * rotQ[0] * rotQ[2],
                    2 * rotQ[2] * rotQ[3] - 2 * rotQ[0] * rotQ[1],
                    1 - 2 * rotQ[1] * rotQ[1] - 2 * rotQ[2] * rotQ[2];
            Matrix3d rotT = rot.transpose();

            for (int i = 0; i < atomNumA; ++i)
            {
                Vector3d curPos
                        (atomsA[i]->getLocation()[0], atomsA[i]->getLocation()[1], atomsA[i]->getLocation()[2]);
                posA[i] = curPos;
                meanA(0) += curPos[0];
                meanA(1) += curPos[1];
                meanA(2) += curPos[2];
            }
            meanA /= atomNumA;

            //molB->getXFAtoms(trans, 0);

            for (int i = 0; i < atomNumB; ++i)
            {
                Vector3d curPos
                        (atomsB[i]->getLocation()[0], atomsB[i]->getLocation()[1], atomsB[i]->getLocation()[2]);
                posB[i] = curPos;
                meanB(0) += curPos[0];
                meanB(1) += curPos[1];
                meanB(2) += curPos[2];
            }
            meanB /= atomNumB;

            for (int i = 0; i < atomNumB; ++i)
            {
                posB[i] = rotT * posB[i] + trans;
            }

            int contactCount = 0;
            vector<pair<int, int>> contacts;

            for (int i = 0; i < atomNumA; ++i)
            {
                for (int j = 0; j < atomNumB; ++j)
                {
                    cout << "Current pair to check: " << i << "," << j << endl;
                    double curDist = sqrt((posA[i](0) - posB[j](0)) * (posA[i](0) - posB[j](0)) +
                                          (posA[i](1) - posB[j](1)) * (posA[i](1) - posB[j](1)) +
                                          (posA[i](2) - posB[j](2)) * (posA[i](2) - posB[j](2)));
                    double lowerBound = this->df->bondingLowerBound(atomsA[i], atomsB[j]);
                    double upperBound = this->df->bondingUpperBound(atomsA[i], atomsB[j]);
                    cout << "Lower: " << lowerBound << ", upper: " << upperBound << ", current: " << curDist << endl;
                    if (curDist < lowerBound)
                    {
                        cout << "Collision" << endl;
                    }
                    else if (curDist > upperBound)
                    {
                        cout << "Too far" << endl;
                    }
                    else
                    {
                        cout << "Pair found" << endl;
                        ++contactCount;
                        auto currentPair = make_pair(i, j);
                        contacts.push_back(currentPair);
                    }
                }
            }

            if (contactCount >= 1)
            {
                cout << "Point " << currentPointNum << ", " << contactCount << " contacts." << endl;
                for (auto &pair: contacts)
                {
                    cout << pair.first << ", " << pair.second << endl;
                }
            }
        }
        inFileGrid.close();
    }
}

void UniformCartesianSampler::processMCData(const string &fileName, int nodeDim)
{
    ifstream inFile;
    inFile.open(fileName);

    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    int sizeA = molA->size();
    int sizeB = molB->size();

    int tracNo;
    double c1, c2, c3, r1, r2, r3;

    Vector3d TB;
    Matrix3d RB;
    Vector3d eaB;

    /*
    int fiveDimPointCounter[sizeA][sizeB];
    for (int i = 0; i < sizeA; ++i)
    {
        for (int j = 0; j < sizeB; ++j)
        {
            fiveDimPointCounter[i][j] = 0;
        }
    }*/
    int dim = 6 - nodeDim;
    unordered_map<vector<int>, int, vector_int_hash, vector_int_eq> nodeCount;

    while (!inFile.eof())
    {
        inFile >> tracNo >> c1 >> c2 >> c3 >> r1 >> r2 >> r3;
        TB[0] = c1;
        TB[1] = c2;
        TB[2] = c3;
        eaB << r1, r2, r3;
        RB = euler2Rotation(r1, r2, r3);

        vector<Atom *> atomsA = molA->getAtoms();
        vector<Atom *> atomsB = transformHelixB(molB, TB, RB);

        bool collision = false;
        auto contactList = checkConstraints(atomsA, atomsB, collision);
        int contactCount = contactList.size();
        if (contactCount >= dim)
        {
            cout << "Point " << tracNo << ", " << contactCount << " contacts, collision " << collision << endl;
            for (auto &pair: contactList)
            {
                cout << pair.first << ", " << pair.second << endl;
            }
            if (!collision)
            {
                vector<pair < int, int>>
                curPoint;
                for (auto &pair: contactList)
                {
                    curPoint.push_back(pair);
                }

                vector<bool> mask(contactCount);
                fill(mask.end() - dim, mask.end(), true);

                do
                {
                    vector<int> pt;
                    for (int i = 0; i < contactCount; ++i)
                    {
                        if (mask[i])
                        {
                            pt.push_back(curPoint[i].first);
                            pt.push_back(curPoint[i].second);
                        }
                    }
                    ++nodeCount[pt];
                }
                while (next_permutation(mask.begin(), mask.end()));

                //++nodeCount[curPoint];
            }
        }

        for (auto &atom: atomsB)
        {
            delete (atom);
        }
    }

    vector<pair<vector<int>, int>> sortedNodeCount;

    for (auto &curNode: nodeCount)
    {
        sortedNodeCount.push_back(curNode);
    }

    auto cmp = [](pair<vector<int>, int> entry0, pair<vector<int>, int> entry1) -> bool
    {
        return entry0.second < entry1.second;
    };

    sort(sortedNodeCount.begin(), sortedNodeCount.end(), cmp);
    for (auto &curNode: sortedNodeCount)
    {
        for (auto &point: curNode.first)
        {
            cout << point << ", ";
        }
        cout << " - " << curNode.second << endl;
    }

    /*
    for (int i = 0; i < sizeA; ++i)
    {
        for (int j = 0; j < sizeB; ++j)
        {
            cout << fiveDimPointCounter[i][j] << " ";
        }
        cout << endl;
    }
     */
    inFile.close();
}

void UniformCartesianSampler::processGridData(const string &fileName, int nodeDim)
{
    ifstream inFile;
    inFile.open(fileName);

    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    int sizeA = molA->size();
    int sizeB = molB->size();

    int tracNo;
    double c1, c2, c3, q0, q1, q2, q3;

    Vector3d TB;
    Matrix3d RB;
    Vector3d eaB;

    /*
    int fiveDimPointCounter[sizeA][sizeB];
    for (int i = 0; i < sizeA; ++i)
    {
        for (int j = 0; j < sizeB; ++j)
        {
            fiveDimPointCounter[i][j] = 0;
        }
    }*/
    int dim = 6 - nodeDim;
    unordered_map<vector<int>, int, vector_int_hash, vector_int_eq> nodeCount;

    while (!inFile.eof())
    {
        inFile >> tracNo >> c1 >> c2 >> c3 >> q0 >> q1 >> q2 >> q3;
        inFile >> tracNo >> c1 >> c2 >> c3 >> q0 >> q1 >> q2 >> q3;
        TB[0] = c1;
        TB[1] = c2;
        TB[2] = c3;
        RB = quaternion2Rotation(q0, q1, q2, q3);
        eaB = rotation2Euler(RB);

        vector<Atom *> atomsA = molA->getAtoms();
        vector<Atom *> atomsB = transformHelixB(molB, TB, RB);

        bool collision = false;
        auto contactList = checkConstraints(atomsA, atomsB, collision);
        int contactCount = contactList.size();

        if (contactCount >= dim)
        {
            if (this->verbose)
            {
                cout << "Point " << tracNo << ", " << contactCount << " contacts, collision " << collision << endl;
                for (auto &pair: contactList)
                {
                    cout << pair.first << ", " << pair.second << endl;
                }
            }
            if (!collision)
            {
                vector<pair<int, int>> curPoint;
                for (auto &pair: contactList)
                {
                    curPoint.push_back(pair);
                }

                vector<bool> mask(contactCount);
                fill(mask.end() - dim, mask.end(), true);

                do
                {
                    vector<int> pt;
                    for (int i = 0; i < contactCount; ++i)
                    {
                        if (mask[i])
                        {
                            pt.push_back(curPoint[i].first);
                            pt.push_back(curPoint[i].second);
                        }
                    }
                    ++nodeCount[pt];
                }
                while (next_permutation(mask.begin(), mask.end()));

                //++nodeCount[curPoint];
            }
        }

        for (auto &atom: atomsB)
        {
            delete (atom);
        }
    }

    vector<pair<vector<int>, int>> sortedNodeCount;

    for (auto &curNode: nodeCount)
    {
        sortedNodeCount.push_back(curNode);
    }

    auto cmp = [](pair<vector<int>, int> entry0, pair<vector<int>, int> entry1) -> bool
    {
        return entry0.second < entry1.second;
    };

    sort(sortedNodeCount.begin(), sortedNodeCount.end(), cmp);
    for (auto &curNode: sortedNodeCount)
    {
        for (auto &point: curNode.first)
        {
            cout << point << ", ";
        }
        cout << " - " << curNode.second << endl;
    }


    /*
    for (int i = 0; i < sizeA; ++i)
    {
        for (int j = 0; j < sizeB; ++j)
        {
            cout << fiveDimPointCounter[i][j] << " ";
        }
        cout << endl;
    }
     */
    inFile.close();
}

Matrix3d UniformCartesianSampler::quaternion2Rotation(double q0, double q1, double q2, double q3)
{
    Matrix3d R;
    R << 1 - 2 * q2 * q2 - 2 * q3 * q3, 2 * q1 * q2 + 2 * q0 * q3, 2 * q1 * q3 - 2 * q0 * q2,
            2 * q1 * q2 - 2 * q0 * q3, 1 - 2 * q1 * q1 - 2 * q3 * q3, 2 * q2 * q3 + 2 * q0 * q1,
            2 * q1 * q3 + 2 * q0 * q2, 2 * q2 * q3 - 2 * q0 * q1, 1 - 2 * q1 * q1 - 2 * q2 * q2;
    return R;
}

Vector3d UniformCartesianSampler::rotation2Euler(Eigen::Matrix3d rot)
{
    double phi = 0.0;
    double cos_theta = 1.0;
    double psi = 0.0;

    cos_theta = rot(2, 2);

    if (abs(cos_theta - 1.0) > 1.0e-8)
    {
        psi = atan2(rot(0, 2), rot(1, 2));
        phi = atan2(rot(2, 0), -rot(2, 1));
    }
    else
    {
        psi = atan2(-rot(1, 0), rot(0, 0));
        phi = 0.0;
    }

    Vector3d out(phi, cos_theta, psi);
    return out;
}

vector<Atom *> UniformCartesianSampler::transformHelixB(MolecularUnit *helxB, Eigen::Vector3d TB, Eigen::Matrix3d RB)
{
    vector<Atom *> helB = helxB->getAtoms();
    vector<Atom *> outputB;
    Atom *current;

    int moleculeCount = helB.size();
    Vector3d meanB;
    meanB << 0, 0, 0;
    for (size_t i = 0; i < moleculeCount; ++i)
    {
        double *l = helB[i]->getLocation();
        Vector3d p(l[0], l[1], l[2]);
        meanB += p;
    }
    meanB /= moleculeCount;

    meanB = RB * meanB;

    Vector3d t = TB - meanB;

    for (size_t iter = 0; iter < helB.size(); iter++)
    {
        current = new Atom(helB[iter]);
        double *l = current->getLocation();
        Vector3d p(l[0], l[1], l[2]);
        Vector3d h = RB * p + t;
        current->setLocation(h(0), h(1), h(2));
        outputB.push_back(current);
    }

    return outputB;
}

bool UniformCartesianSampler::checkConstraint(vector<Atom *> atomsA, vector<Atom *> atomsB, pair<int, int> constraint)
{
    double *aLoc = atomsA[constraint.first]->getLocation();
    double *bLoc = atomsB[constraint.second]->getLocation();
    double distance = Utils::dist(aLoc, bLoc);
    double radiusSum = atomsA[constraint.first]->getRadius() + atomsB[constraint.second]->getRadius();
    double bondingUpperBound = df->bondingUpperBound(atomsA[constraint.first], atomsB[constraint.second]);
    double bondingLowerBound = df->bondingLowerBound(atomsA[constraint.first], atomsB[constraint.second]);

    if (distance > bondingLowerBound && distance < bondingUpperBound)
    {
        return true;
    }
    return false;
}

vector<pair<int, int>>
UniformCartesianSampler::checkConstraints(vector<Atom *> atomsA, vector<Atom *> atomsB, bool &collision)
{
    vector<pair<int, int>> contactList;

    for (size_t i = 0; i < atomsA.size(); i++)
    {
        double *aLoc = atomsA[i]->getLocation();
        for (size_t j = 0; j < atomsB.size(); j++)
        {
            double *bLoc = atomsB[j]->getLocation();
            double distance = Utils::dist(aLoc, bLoc);
            double radiusSum = atomsA[i]->getRadius() + atomsB[j]->getRadius();
            double bondingUpperBound = df->bondingUpperBound(atomsA[i], atomsB[j]);
            double bondingLowerBound = df->bondingLowerBound(atomsA[i], atomsB[j]);
            double collisionLowerBound = df->collisionLowerBound(atomsA[i], atomsB[j]);

            if (distance < collisionLowerBound)
            {
                collision = true;
            }

            if (distance > bondingLowerBound && distance < bondingUpperBound)
            {  // && contactList.size() < 6
                contactList.emplace_back(i, j);
            }
        }
    }
    return contactList;
}

vector<double>
UniformCartesianSampler::calcRelDist2Origin(vector<double> origin_, vector<double> cartesianCoord)
{
    vector<double> ret(6, 0);
    for (int i = 0; i < 6; ++i)
    {
        double dist = cartesianCoord[i] - origin_[i];
        ret[i] = dist / this->stepSize[i];
    }
    return ret;
}

// TODO seems obsolete?
int UniformCartesianSampler::coverageUsingSameSamples(vector<vector<double>> testPointsCartesianCoord,
                                                      vector<vector<double>> gridPointsCartesianCoord,
                                                      double epsilon, bool writeToFile)
{
    unordered_map<vector<int>, int, vector_int_hash, vector_int_eq> cubeMap;
    for (auto &cartesianCoord: testPointsCartesianCoord)
    {
        //auto relCoord = mapPointToCube(cartesianCoord, this->origin, epsilonStepSize, false);
        auto relCoord = calcRelDist2Origin(this->origin, cartesianCoord);
        vector<int> cubeCoord(6, 0);
        bool isIn = true;
        for (int i = 0; i < 6; ++i)
        {
            double ip, fp;
            fp = modf(relCoord[i], &ip);
            if (abs(fp) < epsilon)
            {
                cubeCoord[i] = ip;
            }
            else if (abs(fp) > 1 - epsilon)
            {
                if (fp > 0)
                {
                    cubeCoord[i] = ip + 1;
                }
                else
                {
                    cubeCoord[i] = ip - 1;
                }
            }
            else
            {
                isIn = false;
                break;
            }
        }
        if (isIn)
        {
            auto result = cubeMap.find(cubeCoord);

            if (result == cubeMap.end())
            {
                cubeMap.insert(make_pair(cubeCoord, 1));
            }
            else
            {
                ++result->second;
            }
        }
    }

    if (writeToFile)
    {
        stringstream outFileName;
        ofstream outFile;
        outFileName << "coverageResult_flip" << this->curFlipNo << ".txt";
        outFileName.flush();
        outFile.open(outFileName.str().c_str());
        for (auto &entry: cubeMap)
        {
            outFile << entry.second << endl;
        }
        outFile.close();
    }
    return cubeMap.size();
}

double UniformCartesianSampler::coverageByPoint(vector<vector<double>> testPointsCartesianCoord, int altTestCount,
                                                bool writeToFile)
{
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
    cout << "Coverage test" << endl;

    vector<vector<double>> gridPointsCartesianCoord;

    int gridPointCount;
    int mappedCubeCount;
    stringstream baseLineFileName;
    ifstream baselineFile;
    baseLineFileName << "baseline.txt";
    baseLineFileName.flush();
    baselineFile.open(baseLineFileName.str().c_str(), ios_base::in);

    cout << "Test point count: " << testPointsCartesianCoord.size() << endl;
    cout << "Use alternative test count: " << altTestCount << endl;

    int altBaselineCount;
    vector<double> gridStepSize(6, 0);
    for (int i = 0; i < 6; ++i)
    {
        baselineFile >> gridStepSize[i];
    }

    while (!baselineFile.eof())
    {
        vector<double> currentEntry(6, 0);
        for (int i = 0; i < 6; ++i)
        {
            baselineFile >> currentEntry[i];
        }
        gridPointsCartesianCoord.emplace_back(currentEntry);
    }
    altBaselineCount = gridPointsCartesianCoord.back()[0];
    gridPointsCartesianCoord.pop_back();
    gridPointCount = gridPointsCartesianCoord.size();
    cout << "Baseline point count: " << gridPointCount << endl;
    cout << "Use alternative baseline count: " << altBaselineCount << endl;

    vector<int> cubeCountMap(gridPointCount, 0);

    double gammaCoord;
    if (altTestCount == -1)
    {
        gammaCoord = pow(double(altBaselineCount) / double(testPointsCartesianCoord.size()), 1.0 / 6.0);
    }
    else
    {
        gammaCoord = pow(double(altBaselineCount) / altTestCount, 1.0 / 6.0);
    }
    cout << "Gamma: " << gammaCoord << endl;

    mappedCubeCount = 0;
    vector<double> gammaDist(6, 0);
    for (int i = 0; i < 6; ++i)
    {
        gammaDist[i] = gridStepSize[i] * gammaCoord;
    }

    for (auto &testPointCartesianCoord: testPointsCartesianCoord)
    {
        for (int i = 0; i < gridPointCount; ++i)
        {
            bool isIn = true;
            auto currentGridPoint = gridPointsCartesianCoord[i];

            for (int j = 0; j < 6; ++j)
            {
                double diff = abs(testPointCartesianCoord[j] - currentGridPoint[j]);
                if (j >= 3 && j <= 5)
                {
                    if (diff > PI)
                    {
                        diff = 2 * PI - diff;
                    }
                }
                if (diff > gammaDist[j])
                {
                    isIn = false;
                    break;
                }
            }
            if (isIn)
            {
                if (cubeCountMap[i] == 0)
                {
                    ++mappedCubeCount;
                }
                ++cubeCountMap[i];
            }
        }
    }

    if (writeToFile)
    {
        stringstream outFileName;
        ofstream outFile;
        outFileName << "coverageResult.txt";
        outFileName.flush();
        outFile.open(outFileName.str().c_str());
        for (auto entry: cubeCountMap)
        {
            if (entry > 0)
            {
                outFile << entry << endl;
            }
        }
        outFile.close();
    }
    return double(mappedCubeCount) / double(gridPointCount);
}

// TODO obsolete, maybe remove if not needed
double UniformCartesianSampler::coverageByCube(vector<vector<double>> testCubeCentreCartesianCoord,
                                               vector<double> testCubeStepSize,
                                               vector<vector<int>> gridPointsRelCoord, double epsilon,
                                               bool writeToFile)
{
    cout << "Epsilon coverage by cube" << endl;

    int totalCubeCount;
    int mappedCubeCount;
    unordered_map<vector<int>, int, vector_int_hash, vector_int_eq> cubeMap;

    totalCubeCount = gridPointsRelCoord.size();
    mappedCubeCount = 0;
    vector<double> epsilonDist(6, 0);
    for (int i = 0; i < 6; ++i)
    {
        epsilonDist[i] = this->stepSize[i] * epsilon + testCubeStepSize[i] / 2;
    }

    for (auto &testCubeCentre: testCubeCentreCartesianCoord)
    {
        for (auto &gridPointRelCoord: gridPointsRelCoord)
        {
            bool isIn = true;
            auto cube = getCubeFromRelCoord(gridPointRelCoord);
            auto gridCubeCentre = cube->centre->cartesianCoord;
            for (int i = 0; i < 6; ++i)
            {
                double diff = abs(testCubeCentre[i] - gridCubeCentre[i]);
                if (i >= 3 && i <= 5)
                {
                    if (diff > PI)
                    {
                        diff = 2 * PI - diff;
                    }
                }
                if (diff > epsilonDist[i])
                {
                    isIn = false;
                    break;
                }
            }
            delete (cube->centre);
            delete (cube);

            if (isIn)
            {
                if (cubeMap[gridPointRelCoord] == 0)
                {
                    ++mappedCubeCount;
                }
                ++cubeMap[gridPointRelCoord];
            }
        }
    }

    if (writeToFile)
    {
        stringstream outFileName;
        ofstream outFile;
        outFileName << "coverageResult_flip" << this->curFlipNo << ".txt";
        outFileName.flush();
        outFile.open(outFileName.str().c_str());
        for (auto &entry: cubeMap)
        {
            outFile << entry.second << endl;
        }
        outFile.close();
    }
    cout << "Grid points: " << totalCubeCount << endl;
    cout << "Mapped points: " << mappedCubeCount << endl;
    return double(mappedCubeCount) / double(totalCubeCount);
}

void UniformCartesianSampler::gridData2CoverageBaseline(const string &fileName,
                                                        const vector<pair<int, int>> &nodeContactList)
{
    cout << "Generating baseline data from grid result." << endl;

    ifstream inFile;
    inFile.open(fileName, ios_base::in);
    ofstream outFile;
    outFile.open("baseline.txt", ios_base::app | ios_base::out);

    for (int i = 0; i < 6; ++i)
    {
        outFile << this->stepSize[i] << " ";
    }
    outFile << endl;

    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    size_t sizeA = molA->size();
    size_t sizeB = molB->size();

    int tracNo;
    double c1, c2, c3, q0, q1, q2, q3;

    Vector3d TB;
    Matrix3d RB;
    Vector3d eaB;

    unsigned int saveCount = 0;
    unsigned int saveThreshold = 10000;
    vector<vector<double>> buffer;

    unsigned int matchCount = 0;
    size_t dim = nodeContactList.size();
    unsigned int fiveDimCount[5] = {0, 0, 0, 0, 0};

    while (!inFile.eof())
    {
        inFile >> tracNo >> c1 >> c2 >> c3 >> q0 >> q1 >> q2 >> q3;
        inFile >> tracNo >> c1 >> c2 >> c3 >> q0 >> q1 >> q2 >> q3;
        TB[0] = c1;
        TB[1] = c2;
        TB[2] = c3;
        RB = quaternion2Rotation(q0, q1, q2, q3);
        eaB = rotation2Euler(RB);

        vector<Atom *> atomsA = molA->getAtoms();
        vector<Atom *> atomsBOriginal = molB->getAtoms();
        vector<Atom *> atomsB = transformHelixB(molB, TB, RB);

        bool collision = false;
        auto pointContactList = checkConstraints(atomsA, atomsB, collision);
        matchCount = 0;

        if (!collision)
        {
            int entry = 0;
            for (auto &curContact: nodeContactList)
            {
                if (std::find(pointContactList.begin(), pointContactList.end(), curContact) != pointContactList.end())
                {
                    ++matchCount;
                    ++fiveDimCount[entry];
                }
                ++entry;
            }

            if (matchCount == dim)
            {
                ++saveCount;

                double fromB[3][3], toB[3][3];
                for (int i = 0; i < 3; ++i)
                {
                    for (int j = 0; j < 3; ++j)
                    {
                        fromB[i][j] = atomsBOriginal[i]->getLocation()[j];
                        toB[i][j] = atomsB[i]->getLocation()[j];
                    }
                }
                auto ori = new Orientation(fromB, toB);
                vector<double> cartesianPoint = ori2Cartesian(ori);
                delete (ori);
                buffer.push_back(cartesianPoint);
                if (saveCount >= saveThreshold)
                {
                    for (auto &point: buffer)
                    {
                        outFile << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                                << " " << point[5] << endl;
                    }
                    buffer.clear();
                    saveCount = 0;
                }
            }
        }

        for (auto &atom: atomsB)
        {
            delete (atom);
        }
    }

    if (!buffer.empty())
    {
        for (auto &point: buffer)
        {
            outFile << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4] << " "
                    << point[5] << endl;
        }
        buffer.clear();
        saveCount = 0;
    }

    cout << "5-dim ancestor size: " << fiveDimCount[0] << ", " << fiveDimCount[1] << ", " << fiveDimCount[2] << ", "
         << fiveDimCount[3] << ", " << fiveDimCount[4] << endl;

    double avgFiveDimSize =
            double(fiveDimCount[0] + fiveDimCount[1] + fiveDimCount[2] + fiveDimCount[3] + fiveDimCount[4]) / dim;

    outFile << avgFiveDimSize;

    inFile.close();
    outFile.close();
}

void UniformCartesianSampler::gridData2HeatMap(const string &fileName, int dim)
{
    cout << "Generating heatmap from grid result." << endl;

    ifstream inFile;
    inFile.open(fileName, ios_base::in);
    ofstream outFile; // regular heat map
    ofstream outFileW; // weighted heat map
    string outFileName = "heatmap_";
    string outFileNameW = "weighted_heatmap_";
    outFileName += to_string(dim);
    outFileNameW += to_string(dim);
    outFileName += ".txt";
    outFileNameW += ".txt";
    outFile.open(outFileName, ios_base::app | ios_base::out);
    outFileW.open(outFileNameW, ios_base::app | ios_base::out);

    vector<vector<pair<int, int>>> nodes;
    pair<int, int> tmpPair;
    vector<pair<int, int>> tmpVec;

    if (dim == 5)
    {
        tmpPair = make_pair(5, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }
    else if (dim == 4)
    {
        tmpPair = make_pair(5, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }
    else if (dim == 3)
    {
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(3, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(3, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }
    else if (dim == 2)
    {
        tmpPair = make_pair(7, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 14);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(3, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 12);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 18);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }

    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    size_t sizeA = molA->size();
    size_t sizeB = molB->size();

    int tracNo;
    double c1, c2, c3, q0, q1, q2, q3;

    Vector3d TB;
    Matrix3d RB;
    Vector3d eaB;

    unsigned int saveCount = 0;
    unsigned int saveThreshold = 10000;
    vector<vector<double>> buffer;
    unsigned char nodeMatchCount = 0;

    while (!inFile.eof())
    {
        inFile >> tracNo >> c1 >> c2 >> c3 >> q0 >> q1 >> q2 >> q3;
        inFile >> tracNo >> c1 >> c2 >> c3 >> q0 >> q1 >> q2 >> q3;
        TB[0] = c1;
        TB[1] = c2;
        TB[2] = c3;
        RB = quaternion2Rotation(q0, q1, q2, q3);
        eaB = rotation2Euler(RB);

        vector<Atom *> atomsA = molA->getAtoms();
        vector<Atom *> atomsBOriginal = molB->getAtoms();
        vector<Atom *> atomsB = transformHelixB(molB, TB, RB);

        bool collision = false;
        auto pointContactList = checkConstraints(atomsA, atomsB, collision);
        nodeMatchCount = 0;

        if (!collision)
        {
            for (auto &curNode: nodes)
            {
                int contactMatchCount = 0;
                for (auto curContact: curNode)
                {
                    if (find(pointContactList.begin(), pointContactList.end(), curContact) != pointContactList.end())
                    {
                        ++contactMatchCount;
                    }
                }
                if (contactMatchCount >= 6 - dim)
                {
                    ++nodeMatchCount;
                }
            }


            if (nodeMatchCount > 0)
            {
                ++saveCount;

                double fromB[3][3], toB[3][3];
                for (int i = 0; i < 3; ++i)
                {
                    for (int j = 0; j < 3; ++j)
                    {
                        fromB[i][j] = atomsBOriginal[i]->getLocation()[j];
                        toB[i][j] = atomsB[i]->getLocation()[j];
                    }
                }
                auto ori = new Orientation(fromB, toB);
                vector<double> cartesianPoint = ori2Cartesian(ori);
                delete (ori);
                vector<double> toBuff;
                toBuff.emplace_back(cartesianPoint[0]);
                toBuff.emplace_back(cartesianPoint[1]);
                toBuff.emplace_back(cartesianPoint[2]);
                toBuff.emplace_back(cartesianPoint[3]);
                toBuff.emplace_back(cartesianPoint[4]);
                toBuff.emplace_back(cartesianPoint[5]);
                toBuff.emplace_back(nodeMatchCount);
                buffer.push_back(toBuff);
                if (saveCount >= saveThreshold)
                {
                    for (auto &point: buffer)
                    {
                        outFile << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                                << " " << point[5] << endl;
                        outFileW << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                                 << " " << point[5] << " " << point[6] << endl;
                    }
                    buffer.clear();
                    saveCount = 0;
                }
            }

            double averageB[3] = {0, 0, 0};
            for (auto &atom: atomsB)
            {
                for (int i = 0; i < 3; ++i)
                {
                    averageB[i] += atom->getLocation()[i];
                }
            }
            for (double &i: averageB)
            {
                i /= atomsB.size();
            }
        }

        for (auto &atom: atomsB)
        {
            delete (atom);
        }
    }

    if (!buffer.empty())
    {
        for (auto &point: buffer)
        {
            outFile << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                    << " " << point[5] << endl;
            outFileW << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                     << " " << point[5] << " " << point[6] << endl;
        }
        buffer.clear();
        saveCount = 0;
    }
    inFile.close();
    outFile.close();
    outFileW.close();
}

void UniformCartesianSampler::mcData2HeatMap(const std::string &fileName, int dim)
{
    cout << "Generating heatmap from mc result." << endl;

    ifstream inFile;
    inFile.open(fileName, ios_base::in);
    ofstream outFile; // regular heat map
    ofstream outFileW; // weighted heat map
    string outFileName = "mc_heatmap_";
    string outFileNameW = "mc_weighted_heatmap_";
    outFileName += to_string(dim);
    outFileNameW += to_string(dim);
    outFileName += ".txt";
    outFileNameW += ".txt";
    outFile.open(outFileName, ios_base::app | ios_base::out);
    outFileW.open(outFileNameW, ios_base::app | ios_base::out);

    vector<vector<pair<int, int>>> nodes;
    pair<int, int> tmpPair;
    vector<pair<int, int>> tmpVec;

    if (dim == 5)
    {
        tmpPair = make_pair(5, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }
    else if (dim == 4)
    {
        tmpPair = make_pair(5, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 5);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }
    else if (dim == 3)
    {
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(3, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(3, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(11, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }
    else if (dim == 2)
    {
        tmpPair = make_pair(7, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(7, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 14);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(3, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 5);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 12);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 18);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 3);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(9, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 16);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(5, 9);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 11);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(5, 16);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(11, 11);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(7, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(14, 14);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();

        tmpPair = make_pair(1, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 1);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(9, 7);
        tmpVec.push_back(tmpPair);
        tmpPair = make_pair(16, 7);
        tmpVec.push_back(tmpPair);
        nodes.push_back(tmpVec);
        tmpVec.clear();
    }
    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    int sizeA = molA->size();
    int sizeB = molB->size();

    int tracNo;
    double c1, c2, c3, r1, r2, r3;

    Vector3d TB;
    Matrix3d RB;
    Vector3d eaB;

    unsigned int saveCount = 0;
    unsigned int saveThreshold = 10000;
    vector<vector<double>> buffer;
    unsigned char nodeMatchCount = 0;

    while (!inFile.eof())
    {
        inFile >> tracNo >> c1 >> c2 >> c3 >> r1 >> r2 >> r3;
        TB[0] = c1;
        TB[1] = c2;
        TB[2] = c3;
        eaB << r1, r2, r3;
        RB = euler2Rotation(r1, r2, r3);

        vector<Atom *> atomsA = molA->getAtoms();
        vector<Atom *> atomsBOriginal = molB->getAtoms();
        vector<Atom *> atomsB = transformHelixB(molB, TB, RB);

        bool collision = false;
        auto pointContactList = checkConstraints(atomsA, atomsB, collision);
        nodeMatchCount = 0;

        if (!collision)
        {
            for (auto &curNode: nodes)
            {
                int contactMatchCount = 0;
                for (auto curContact: curNode)
                {
                    if (find(pointContactList.begin(), pointContactList.end(), curContact) != pointContactList.end())
                    {
                        ++contactMatchCount;
                    }
                }
                if (contactMatchCount >= 6 - dim)
                {
                    ++nodeMatchCount;
                }
            }

            if (nodeMatchCount > 0)
            {
                ++saveCount;

                double fromB[3][3], toB[3][3];
                for (int i = 0; i < 3; ++i)
                {
                    for (int j = 0; j < 3; ++j)
                    {
                        fromB[i][j] = atomsBOriginal[i]->getLocation()[j];
                        toB[i][j] = atomsB[i]->getLocation()[j];
                    }
                }
                auto ori = new Orientation(fromB, toB);
                vector<double> cartesianPoint = ori2Cartesian(ori);
                delete (ori);
                vector<double> toBuff;
                toBuff.emplace_back(cartesianPoint[0]);
                toBuff.emplace_back(cartesianPoint[1]);
                toBuff.emplace_back(cartesianPoint[2]);
                toBuff.emplace_back(cartesianPoint[3]);
                toBuff.emplace_back(cartesianPoint[4]);
                toBuff.emplace_back(cartesianPoint[5]);
                toBuff.emplace_back(nodeMatchCount);
                buffer.push_back(toBuff);
                if (saveCount >= saveThreshold)
                {
                    for (auto &point: buffer)
                    {
                        outFile << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                                << " " << point[5] << endl;
                        outFileW << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                                 << " " << point[5] << " " << point[6] << endl;
                    }
                    buffer.clear();
                    saveCount = 0;
                }
            }

            double averageB[3] = {0, 0, 0};
            for (auto &atom: atomsB)
            {
                for (int i = 0; i < 3; ++i)
                {
                    averageB[i] += atom->getLocation()[i];
                }
            }
            for (double &i: averageB)
            {
                i /= atomsB.size();
            }
        }

        for (auto &atom: atomsB)
        {
            delete (atom);
        }
    }

    if (!buffer.empty())
    {
        for (auto &point: buffer)
        {
            outFile << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                    << " " << point[5] << endl;
            outFileW << point[0] << " " << point[1] << " " << point[2] << " " << point[3] << " " << point[4]
                     << " " << point[5] << " " << point[6] << endl;
        }
        buffer.clear();
        saveCount = 0;
    }
    inFile.close();
    outFile.close();
    outFileW.close();
}

void UniformCartesianSampler::mcData2Histogram(const string &fileName, const vector<pair<int, int>> &nodeContactList)
{
    ifstream inFile;
    inFile.open(fileName);

    ofstream outFiles[40];

    vector<vector<pair<int, int>>> nodes;

    pair<int, int> tmpPair;
    vector<pair<int, int>> tmpVec;

    tmpPair = make_pair(5, 5);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[0].open("0505.txt");

    tmpPair = make_pair(11, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[1].open("1111.txt");

    tmpPair = make_pair(5, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[2].open("0511.txt");

    tmpPair = make_pair(11, 5);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[3].open("1105.txt");

    tmpPair = make_pair(5, 7);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[4].open("0507.txt");

    tmpPair = make_pair(7, 5);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[5].open("0705.txt");

    tmpPair = make_pair(7, 7);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[6].open("0707.txt");

    tmpPair = make_pair(11, 7);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[7].open("1107.txt");

    tmpPair = make_pair(7, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[8].open("0711.txt");

    tmpPair = make_pair(5, 9);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[9].open("0509.txt");

    tmpPair = make_pair(5, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(5, 9);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[10].open("0501_0509.txt");

    tmpPair = make_pair(11, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[11].open("1111_1116.txt");

    tmpPair = make_pair(9, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[12].open("0911_1611.txt");

    tmpPair = make_pair(5, 3);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(5, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[13].open("0503_0511.txt");

    tmpPair = make_pair(9, 5);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 5);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[14].open("0905_1605.txt");

    tmpPair = make_pair(7, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(7, 9);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[15].open("0701_0709.txt");

    tmpPair = make_pair(11, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[16].open("1109_1116.txt");

    tmpPair = make_pair(7, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[17].open("0711_1416.txt");

    tmpPair = make_pair(1, 5);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 5);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[18].open("0105_0905.txt");

    tmpPair = make_pair(9, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[19].open("0911_1616.txt");

    tmpPair = make_pair(9, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[20].open("0911_1611_1616.txt");

    tmpPair = make_pair(7, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[21].open("0711_1411_1416.txt");

    tmpPair = make_pair(11, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[22].open("1109_1111_1116.txt");

    tmpPair = make_pair(3, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 14);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[23].open("0307_1107_1114.txt");

    tmpPair = make_pair(3, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 9);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[24].open("0301_1101_1109.txt");

    tmpPair = make_pair(1, 3);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[25].open("0103_0911_1411.txt");

    tmpPair = make_pair(1, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 14);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[26].open("0107_0907_1414.txt");

    tmpPair = make_pair(5, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(5, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(5, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[27].open("0509_0511_0516.txt");

    tmpPair = make_pair(11, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 16);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[28].open("1109_1116_1616.txt");

    tmpPair = make_pair(7, 3);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(7, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[29].open("0703_0711_1411.txt");

    tmpPair = make_pair(7, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(7, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(7, 16);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[30].open("0709_0711_0716_1416.txt");

    tmpPair = make_pair(9, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 14);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[31].open("0909_1609_1614_1616.txt");

    tmpPair = make_pair(1, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 14);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[32].open("0101_0901_0909_1614.txt");

    tmpPair = make_pair(1, 3);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 3);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[33].open("0103_0903_0911_1411.txt");

    tmpPair = make_pair(3, 5);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 5);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 12);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 18);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[34].open("0305_1105_1112_1118.txt");

    tmpPair = make_pair(1, 3);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 3);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[35].open("0103_0903_0911_1611.txt");

    tmpPair = make_pair(9, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 16);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 16);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 16);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[36].open("0909_0916_1416_1616.txt");

    tmpPair = make_pair(5, 9);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(5, 11);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(5, 16);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(11, 11);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[37].open("0509_0511_0516_1111.txt");

    tmpPair = make_pair(7, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(14, 14);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[38].open("0707_0907_1407_1414.txt");

    tmpPair = make_pair(1, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 1);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(9, 7);
    tmpVec.push_back(tmpPair);
    tmpPair = make_pair(16, 7);
    tmpVec.push_back(tmpPair);
    nodes.push_back(tmpVec);
    tmpVec.clear();
    outFiles[39].open("0101_0901_0907_1607.txt");

    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    int sizeA = molA->size();
    int sizeB = molB->size();

    int tracNo;
    double c1, c2, c3, r1, r2, r3;

    Vector3d TB;
    Matrix3d RB;
    Vector3d eaB;

    int dim = 6;
    unordered_map<vector<int>, int, vector_int_hash, vector_int_eq> nodeCount;

    while (!inFile.eof())
    {
        inFile >> tracNo >> c1 >> c2 >> c3 >> r1 >> r2 >> r3;
        TB[0] = c1;
        TB[1] = c2;
        TB[2] = c3;
        eaB << r1, r2, r3;
        RB = euler2Rotation(r1, r2, r3);

        vector<Atom *> atomsA = molA->getAtoms();
        vector<Atom *> atomsBOriginal = molB->getAtoms();
        vector<Atom *> atomsB = transformHelixB(molB, TB, RB);

        bool collision = false;
        auto pointContactList = checkConstraints(atomsA, atomsB, collision);

        if (!pointContactList.empty() && !collision)
        {
            cout << "Point " << tracNo << endl;
            // not a collision, covert into UC distance/angle format
            double fromB[3][3], toB[3][3];
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    fromB[i][j] = atomsBOriginal[i]->getLocation()[j];
                    toB[i][j] = atomsB[i]->getLocation()[j];
                }
            }
            auto ori = new Orientation(fromB, toB);
            vector<double> cartesianPoint = ori2Cartesian(ori);
            delete (ori);

            int nodeCount = 0;
            for (auto &nodeContactList: nodes)
            {
                // Check if current point's contact list is a superset of node's contact list
                int pointContactEntry = 0;
                int matchCount = 0;
                for (auto &nodeContact: nodeContactList)
                {
                    while (nodeContact != pointContactList[pointContactEntry] &&
                           pointContactEntry < pointContactList.size())
                    {
                        ++pointContactEntry;
                    }
                    if (pointContactEntry < pointContactList.size())
                    {
                        ++matchCount;
                    }
                }
                if (matchCount == nodeContactList.size())
                {
                    cout << "In node " << nodeCount << endl;
                    outFiles[nodeCount] << cartesianPoint[0] << " " << cartesianPoint[1] << " " << cartesianPoint[2]
                                        << " " << cartesianPoint[3] << " " << cartesianPoint[4] << " "
                                        << cartesianPoint[5] << endl;
                }
                ++nodeCount;
            }
        }

        for (auto &atom: atomsB)
        {
            delete (atom);
        }
    }

    inFile.close();
    for (auto &outFile: outFiles)
    {
        outFile.close();
    }
}


// TODO obsolete, remove if not needed
void UniformCartesianSampler::coverageComparison()
{
    cout << "Start epsilon coverage analysis" << endl;
    vector<CartesianPoint *> points;
    vector<CartesianHypercube *> cubes;
    vector<vector<double>> gridPointsCartesianCoord;
    snl->loadCartesian(this->node->getID(), points, true, cubes);
    for (auto &cube: cubes)
    {
        if (cube->status == 4)
        {
            gridPointsCartesianCoord.push_back(cube->centre->cartesianCoord);
        }
    }

    vector<vector<double>> testPointsCartesianCoord;
    for (auto &point: points)
    {
        if (point->status == 5 || point->status == 4)
        {
            testPointsCartesianCoord.push_back(point->cartesianCoord);
        }
    }

    double epsilon =
            pow(double(gridPointsCartesianCoord.size()) / double(testPointsCartesianCoord.size()), 1.0 / 6.0) / 2;
    auto result =
            coverageUsingSameSamples(testPointsCartesianCoord, gridPointsCartesianCoord, epsilon, true);
    cout << double(result) / cubes.size() << endl;

    for (auto &point: points)
    {
        delete (point);
    }
    for (auto &cube: cubes)
    {
        delete (cube);
    }
}

// TODO obsolete, remove if not needed
void UniformCartesianSampler::runCoverageTestWithHiResAsGrid()
{

    /*
    // run sampling, generate test point set
    this->sampleAndAnalyse();

    vector<CartesianPoint *> cartesianPoints;
    vector<CartesianHypercube *> cubes;
    this->snl->loadCartesian(this->node->getID(), cartesianPoints, false, cubes);

    vector<vector<double>> testPointsCartesianCoord;
    for (auto &point: cartesianPoints)
    {
        testPointsCartesianCoord.push_back(point->cartesianCoord);
        delete (point);
    }
    cartesianPoints.clear();

    this->allMappedValidCubesRelativeCoord.clear();
    this->allMappedBadAngleCubesRelativeCoord.clear();
    this->allMappedCollisionCubesRelativeCoord.clear();
    this->allMappedNotRealizableCubesRelativeCoord.clear();

    // run higher-resolution sampling, generate grid point set
    this->stepSize = {.5, .5, .5, 2 * PI / 36, 2 * PI / 36, 2 * PI / 36};
    this->sampleAndAnalyseEachFlip();
    vector<vector<int>> gridPointsRelativeCoord;
    for (auto &cube: this->allMappedValidCubesRelativeCoord)
    {
        gridPointsRelativeCoord.push_back(cube);
    }

    double epsilon =
            pow(double(gridPointsRelativeCoord.size()) / double(testPointsCartesianCoord.size()), 1.0 / 6.0) / 2;
    auto result = coverageByPoint(testPointsCartesianCoord, gridPointsRelativeCoord, epsilon, true);
    cout << result << endl;
    int a = 5;*/
}

vector<double> UniformCartesianSampler::intersectSegment(vector<int> relCoord0, vector<int> relCoord1,
                                                         int &status)
{
    status = 0;
    auto cp0 = new CartesianPoint(Cartesian::calcPointFromRelCoord(origin, stepSize, relCoord0));
    auto cp1 = new CartesianPoint(Cartesian::calcPointFromRelCoord(origin, stepSize, relCoord1));
    cartesian2Cayley(cp0);
    cartesian2Cayley(cp1);

    double constraint = this->cc->getEdgeLength(this->acg->getContacts()[0].first, this->acg->getContacts()[0].second);
    double segmentDistanceTolerance = .3;
    if (((constraint - cp0->cayleyDistances[0] > segmentDistanceTolerance) &&
         (constraint - cp1->cayleyDistances[0] > segmentDistanceTolerance)) ||
        ((constraint - cp0->cayleyDistances[0] < -segmentDistanceTolerance) &&
         (constraint - cp1->cayleyDistances[0] < -segmentDistanceTolerance)))
    {
        //cout << "No intersection" << endl;
        delete (cp0);
        delete (cp1);
        return {};
    }
    else
    {
        double lambda = (constraint - cp0->cayleyDistances[0]) / (cp1->cayleyDistances[0] - cp0->cayleyDistances[0]);
        vector<double> cayleyIntersect(5, 0);
        for (int i = 0; i < 5; ++i)
        {
            cayleyIntersect[i] = cp0->cayleyParameters[i] * lambda + cp1->cayleyParameters[i] * (1 - lambda);
        }
        delete (cp0);
        delete (cp1);

        // Check if intersection point in manifold
        bool fail = false;
        auto ori = UniformCartesianSampler::computeRealization(this->curFlipNo, cayleyIntersect, fail);
        // Realizable
        if (!fail)
        {
            auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);

            auto check = new ConstraintCheck(acg, df);
            bool collision = check->stericsViolated(ori);
            // Point found.
            if (!collision)
            {
                double fromB[3][3];
                double toB[3][3];
                ori->getFromTo(fromB, toB);
                bool badAngle = check->angleViolationCheck(fromB, toB);
                if (!badAngle) // status = 4
                {
                    //cout << "Feasible" << endl;
                    status = 4;
                }
                else // status = 3
                {
                    //cout << "Bad angle" << endl;
                    status = 3;
                }
            }
            else // status = 2
            {
                //cout << "Collision" << endl;
                status = 2;
            }
            delete (check);

            delete (ori);
            return cartesianCoord;
        }
        else // status = 1
        {
            //cout << "Failed realization" << endl;
            status = 1;
        }
        delete (ori);
        return {};
    }
}


vector<vector<vector<double>>>
UniformCartesianSampler::intersectSegment(vector<int> vertexARelCoord, vector<int> vertexBRelCoord, int separatorCount,
                                          int &status)
{
    vector<vector<double>> foundValidCartesianPoints;
    vector<vector<double>> foundBadAngleCartesianPoints;
    vector<vector<double>> foundCollisionCartesianPoints;

    auto vertexA = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, vertexARelCoord));
    auto vertexB = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, vertexBRelCoord));
    cartesian2Cayley(vertexA);
    cartesian2Cayley(vertexB);

    vector<double> breakStepSize(6, 0);
    for (int i = 0; i < 6; ++i)
    {
        breakStepSize[i] = (vertexB->cartesianCoord[i] - vertexA->cartesianCoord[i]) / separatorCount;
    }

    Settings *set = Settings::getInstance();
    MolecularUnit *molA = set->runTimeObjects.muA;
    MolecularUnit *molB = set->runTimeObjects.muB;
    double distance = molA->getAtomAt(this->acg->getParticipants()[0].first)->getRadius() +
                      molB->getAtomAt(this->acg->getParticipants()[0].second)->getRadius();

    Settings *sett = Settings::getInstance();
//double lowerBound = distance * .75;
//double upperBound = distance + .9;
    double lowerBound = distance * sett->Constraint.bondingLowerLambda + sett->Constraint.bondingLowerDelta;
    double upperBound = distance * sett->Constraint.bondingUpperLambda + sett->Constraint.bondingUpperDelta;

    for (int i = 0; i < separatorCount + 1; ++i)
    {
        vector<double> curPointCartesianCoord = vertexA->cartesianCoord;
        for (int j = 0; j < 6; ++j)
        {
            curPointCartesianCoord[j] += breakStepSize[j] * i;
        }

        auto curPoint = new CartesianPoint(curPointCartesianCoord);
        cartesian2Cayley(curPoint);
        if (curPoint->cayleyDistances[0] > lowerBound && curPoint->cayleyDistances[0] < upperBound)
        {
            vector<double> allCayleyEntries = curPoint->cayleyDistances;
            allCayleyEntries.insert(allCayleyEntries.end(), curPoint->cayleyParameters.begin(),
                                    curPoint->cayleyParameters.end());
            bool fail = false;
            auto ori = computeRealization(this->curFlipNo, allCayleyEntries, fail);
            if (!fail)
            {
                auto cartesianCoord = UniformCartesianSampler::ori2Cartesian(ori);

                auto check = new ConstraintCheck(acg, df);
                bool collision = check->stericsViolated(ori);
                // Point found.
                if (!collision)
                {
                    double fromB[3][3];
                    double toB[3][3];
                    ori->getFromTo(fromB, toB);
                    bool badAngle = check->angleViolationCheck(fromB, toB);
                    if (!badAngle) // status = 4
                    {
                        if (status < 4)
                        {
                            status = 4;
                        }
                        foundValidCartesianPoints.push_back(cartesianCoord);
                    }
                    else // status = 3
                    {
                        if (status < 3)
                        {
                            status = 3;
                        }
                        foundBadAngleCartesianPoints.push_back(cartesianCoord);
                    }
                }
                else // status = 2
                {
                    if (status < 2)
                    {
                        status = 2;
                    }
                    foundCollisionCartesianPoints.push_back(cartesianCoord);
                }
                delete (check);
            }
            else // status = 1
            {
                if (status < 1)
                {
                    status = 1;
                }
            }
            delete (ori);

        }
        delete (curPoint);
    }
    delete (vertexA);
    delete (vertexB);

    vector<vector<vector<double>>> ret;
    ret.emplace_back(foundValidCartesianPoints);
    ret.emplace_back(foundBadAngleCartesianPoints);
    ret.emplace_back(foundCollisionCartesianPoints);
    return ret;
}

CartesianHypercube *UniformCartesianSampler::intersectHypercubeThick(vector<int> cubeRelCoord, bool fiveDimCentreMode)
{
    auto cube = new CartesianHypercube(cubeRelCoord);
    cube->stepSize = this->stepSize;
    cube->origin = this->origin;
    cube->centre = new CartesianPoint(Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, cubeRelCoord));
    cube->flipNum = this->curFlipNo;
    cube->status = 0;

// Whether neighbour need to be checked after this cube is visited according to goodFrontierCubes
    vector<bool> neighbourUnvisited = goodFrontierCubes[cubeRelCoord];

    vector<bool> neighboursToVisit(12, false);

    auto faceRelCoords = cube->getAllFacetRelCoords(5);

    for (auto &faceRelCoord: faceRelCoords)
    {
        bool sampleThisFace = true;
        for (int i = 0; i < 6; ++i)
        {
            if ((faceRelCoord[i] == cubeRelCoord[i] - 1) && !neighbourUnvisited[i])
            {
                sampleThisFace = false;
                break;
            }
            if ((faceRelCoord[i] == cubeRelCoord[i] + 1) && !neighbourUnvisited[i + 6])
            {
                sampleThisFace = false;
                break;
            }
        }

        if (sampleThisFace)
        {
            auto face = new CartesianHypercube(faceRelCoord);
            face->centre = new CartesianPoint(
                    Cartesian::calcPointFromRelCoord(this->origin, this->stepSize, faceRelCoord));
            face->stepSize = this->stepSize;
            face->origin = this->origin;
            face->status = 0;
            if (checkPointInFlipUsingTetVol(face->centre->cartesianCoord, this->curFlipNo))
            {
                auto segments = face->getAllSegmentRelCoordsFrom5dFace();

                for (auto &segment: segments)
                {
                    int segmentStatus = 0;
// resultPoints is a vector of vector<vector<double>> (Cartesian coord of points) of found valid/bad angle/collision points
                    auto resultPoints = intersectSegment(segment[0], segment[1], 3, segmentStatus);
                    cube->foundValidCartesianPoints.insert(cube->foundValidCartesianPoints.end(),
                                                           resultPoints[0].begin(),
                                                           resultPoints[0].end());
                    cube->foundBadAngleCartesianPoints.insert(cube->foundBadAngleCartesianPoints.end(),
                                                              resultPoints[1].begin(), resultPoints[1].end());
                    cube->foundCollisionCartesianPoints.insert(cube->foundCollisionCartesianPoints.end(),
                                                               resultPoints[2].begin(), resultPoints[2].end());
                    if (face->status < segmentStatus)
                    {
                        face->status = segmentStatus;
                    }
                }
            }
            if (cube->status < face->status)
            {
                cube->status = face->status;
            }

            if (face->status >= skipThreshold)
            {
                for (int i = 0; i < 6; ++i)
                {
                    if (faceRelCoord[i] != cubeRelCoord[i])
                    {
                        vector<int> nextCubeRelativeCoord = cubeRelCoord;
                        if (faceRelCoord[i] < cubeRelCoord[i])
                        {
                            neighboursToVisit[i] = true;
                        }
                        else
                        {
                            neighboursToVisit[i + 6] = true;
                        }
                    }
                }
            }
            delete (face->centre);
            delete (face);
        }
    }

// put all neighbours checked to list
    for (int i = 0; i < 6; ++i)
    {
        if (neighbourUnvisited[i])
        {
// Add to good if checked
            if (neighboursToVisit[i])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
// Case 1: not in either
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
// Case 2: in bad
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }
                this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
            }
// Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] -= 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i + 6] = false;
                }

            }
        }
        if (neighbourUnvisited[i + 6])
        {
            if (neighboursToVisit[i + 6])
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (this->verbose)
                {
                    cout << "Handling good neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }

                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
// Case 1: not in either
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
// Case 2: in bad
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                        this->goodFrontierCubes[nextCubeRelativeCoord] = this->badFrontierCubes[nextCubeRelativeCoord];
                        this->badFrontierCubes.erase(nextCubeRelativeCoord);
                        this->cubesToTraverse.push(nextCubeRelativeCoord);
                    }
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                }
                this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
            }
// Add to bad if not checked
            else
            {
                vector<int> nextCubeRelativeCoord = cubeRelCoord;
                nextCubeRelativeCoord[i] += 2;
                if (this->verbose)
                {
                    cout << "Handling bad neighbour " << nextCubeRelativeCoord[0] << ", " << nextCubeRelativeCoord[1]
                         << ", " << nextCubeRelativeCoord[2] << ", " << nextCubeRelativeCoord[3] << ", "
                         << nextCubeRelativeCoord[4] << ", " << nextCubeRelativeCoord[5] << endl;
                }
                if (i >= 3 && i <= 5)
                {
                    nextCubeRelativeCoord = fixAngularEntry(nextCubeRelativeCoord);
                }
                if (this->goodFrontierCubes.find(nextCubeRelativeCoord) == this->goodFrontierCubes.end())
                {
                    if (this->badFrontierCubes.find(nextCubeRelativeCoord) == this->badFrontierCubes.end())
                    {
                        if (this->verbose)
                        {
                            cout << "New cube" << endl;
                        }
                        this->badFrontierCubes[nextCubeRelativeCoord] = vector<bool>(12, true);
                    }
                    else
                    {
                        if (this->verbose)
                        {
                            cout << "In bad" << endl;
                        }
                    }
                    this->badFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
                else
                {
                    if (this->verbose)
                    {
                        cout << "In good" << endl;
                    }
                    this->goodFrontierCubes[nextCubeRelativeCoord][i] = false;
                }
            }
        }
    }
    this->goodFrontierCubes.erase(cubeRelCoord);
    return cube;
}

void UniformCartesianSampler::reportFlip(int flipNo)
{
    cout << "Current valid cube count: " << mappedValidCubeCount[flipNo] << endl;
    cout << "Current bad angle cube count: " << mappedBadAngleCubeCount[flipNo] << endl;
    cout << "Current collision cube count: " << mappedCollisionCubeCount[flipNo] << endl;
    cout << "Current not realizable cube count: " << mappedNotRealizableCubeCount[flipNo] << endl;
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
}
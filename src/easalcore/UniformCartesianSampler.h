/*
 This file is part of EASAL.

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cmath>
#include <ctime>
#include <memory>
#include <stack>
#include <unordered_set>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "ActiveConstraintGraph.h"
#include "AtlasNode.h"
#include "Atom.h"
#include "Cartesian.h"
#include "CayleyPoint.h"
#include "ConstraintCheck.h"
#include "ConvexChart.h"
#include "MolecularUnit.h"
#include "Orientation.h"
#include "SaveLoader.h"
#include "Settings.h"

using namespace std;

class UniformCartesianSampler
{
public:
    UniformCartesianSampler(AtlasNode *node, ActiveConstraintGraph *acg, ConvexChart *cc, PredefinedInteractions *df,
                            Orientation *originOri, vector<double> distanceStepSize, vector<int> angularStepNum,
                            vector<Orientation *> startingOriLists[8], SaveLoader *snl, int mode, int skipThreshold);

    void initFlip(int flipNo);

    // Transform orientation (from-to) to Cartesian coord
    static vector<double> ori2Cartesian(Orientation *ori);

    // Transform Cartesian coord (x, y, z, alpha, beta, gamma) to Cayley coord
    void cartesian2Cayley(CartesianPoint *cp) const;

    vector<vector<vector<int>>> decomposeHypercubeToSimplices(vector<int> cubeRelCoord, int targetDim, int mode);

    // Calculate intersection between a Cartesian hypercube (cube) and Cayley region (acg and cc) using co-dimensional simplices method
    // Decompose 6-dim cube into co-dim facets, then simplices.
    CartesianHypercube *intersectHypercubeUsing6DSimplexMethod(vector<int> cubeRelCoord);

    // Calculate intersection between a Cartesian hypercube (cube) and Cayley region (acg and cc) using co-dimensional simplices method
    // Decompose 6-dim cube into 12 5-dim faces, then into (co-dim)-1 simplices, finally add centre of 5-d FACE into simplex.
    CartesianHypercube *intersectHypercubeUsing5DSimplexMethod(vector<int> cubeRelCoord);

    CartesianHypercube *intersectHypercubeUsingHybridSimplexMethod(vector<int> cubeRelCoord);

    CartesianHypercube *intersectFacetUsingSimplexMethod(vector<int> relativeCoord);

    // Calculate intersection between a Cartesian Simplex (simplex) and Cayley region (acg and cc)
    // Return intersection in Cayley coord, PARAMETER PART ONLY, while distance part is in cc
    vector<double> intersectSimplexWithRegion(CartesianSimplex *simplex);

    // Calculate intersection between a Cartesian hypercube (cube) and Cayley region (acg and cc) using co-dimensional hyper-parallelepiped method
    CartesianHypercube *intersectHypercubeUsingBasisMethod(vector<int> cubeRelativeCoord); // TODO update

    // Calculate intersection between a Cartesian facet (facet) and Cayley region (acg and cc)
    // Return intersection in Cayley coord, PARAMETER PART ONLY, while distance p
    //
    //
    //
    // rt is in cc
    CartesianHypercube *intersectFacetWithRegion(const vector<int> &relativeCoord);

    // Map cube's facets into Cayley space
    void calcCayleyParaBasis(CartesianHypercube *cube);

    // Start sampling with current uniform Cartesian sampler, return sum of volume (number of cubes) of all flips
    int sampleAndAnalyseEachFlip();

    // Store points in buffer to txt file
    // Map points in buffer into grid, store cube in memory
    // Clear buffer
    void storeMapAndClearPointBuffer();

    // Map points in allValidPointsCartesianCoord into Cartesian grid, store all cubes with centre and a point mapping to it.
    void storeResultCubes(bool storeAsBaseline);

    // Fill allCubesToTraverseRelativeCoord
    // If startFromPoint then fill with cubes with a valid point inside it
    // Else fill with those cubes' neighbours
    void getAllCubesToSampleFromCurrentValidPoints(bool startFromPoint);

    void sampleCurrentFlip();

    // Return a cube with input relative coord as centre
    CartesianHypercube *getCubeFromRelCoord(vector<int> relCoord);

    //CartesianFacet *getFacetFromRelativeCoord(vector<int> relCoord, int dim, int facetNo);

    // Calculate relative cartesianCoord from Cartesian coord
    // Input point's Cartesian coord, return Cartesian coord of centre of cube that point is in
    vector<int> mapPointToCube(vector<double> cartesianCoord, vector<double> origin_, vector<double> stepSize_);

    vector<vector<int>> getAllNeighbourRelCoord(vector<int> currentRelCoord, bool diagonal);

    // Fix angular entries of a relative coord according to angular step count
    vector<int> fixAngularEntry(vector<int> currentRelCoord);

    // Cayley -> orientation (from/to)
    // If cayleyParamValue has 6 entries, it's full Cayley distance and parameters
    // If cayleyParamValue has less than 6 entries, it's Cayley parameters only, with Cayley distances taken from convex chart
    Orientation *computeRealization(int flipNum, vector<double> cayleyParamValue, bool &fail);

    void analyseCube(CartesianHypercube *cube);

    bool isPointInFlipWithMapBack(vector<double> cartesianCoord, int checkFlipNum);

    bool checkPointInFlipUsingTetVol(vector<double> cartesianCoord, int checkFlipNum);

    void dynamicStepSizeSample(double refineRate, int roundCount);

    // For reading in Euler angle data
    Matrix3d euler2Rotation(double phi, double cos_theta, double psi);

    void processExperimentData(bool processMC, bool processGrid);

    void processMCData(const string &fileName, int nodeDim);

    void processGridData(const string &fileName, int nodeDim);

    static Matrix3d quaternion2Rotation(double q0, double q1, double q2, double q3);

    Vector3d rotation2Euler(Matrix3d rot);

    vector<Atom *> transformHelixB(MolecularUnit *helxB, Eigen::Vector3d TB, Eigen::Matrix3d RB);

    bool checkConstraint(vector<Atom *> atomsA, vector<Atom *> atomsB, pair<int, int> constraint);

    vector<pair<int, int>> checkConstraints(vector<Atom *> atomsA, vector<Atom *> atomsB, bool &collision);

    vector<double> calcRelDist2Origin(vector<double> origin_, vector<double> cartesianCoord);

    int coverageUsingSameSamples(vector<vector<double>> testPointsCartesianCoord,
                                 vector<vector<double>> gridPointsCartesianCoord,
                                 double epsilon, bool writeToFile);

    void coverageComparison();

    double coverageByPoint(vector<vector<double>> testPointsCartesianCoord, int altTestCount, bool writeToFile);

    double coverageByCube(vector<vector<double>> testCubeCentreCartesianCoord, vector<double> testCubeStepSize,
                          vector<vector<int>> gridPointsRelCoord, double epsilon, bool writeToFile);

    void gridData2CoverageBaseline(const string &fileName, const vector<pair<int, int>> &nodeContactList);

    // Create heat map similar to fig 7 and 8 in JCTC paper on MC comparison
    void gridData2HeatMap(const string &fileName, int dim);

    void mcData2HeatMap(const string &fileName, int dim);

    void mcData2Histogram(const string &fileName, const vector<pair<int, int>> &nodeContactList);

    static void runCoverageTestWithHiResAsGrid();

    vector<double> intersectSegment(vector<int> relCoord0, vector<int> relCoord1, int &status);

    // Intersect a SEGMENT with 5-dim Cayley region (CG and CC)
    vector<vector<vector<double>>>
    intersectSegment(vector<int> vertexARelCoord, vector<int> vertexBRelCoord, int separatorCount,
                     int &status);

    CartesianHypercube *intersectHypercubeThick(vector<int> cubeRelCoord, bool fiveDimCentreMode); // TODO update

    void reportSampleCount();

    void reportFlip(int flipNo);

    bool verbose;

    // Node currently being sampled
    AtlasNode *node;
    ActiveConstraintGraph *acg;
    ConvexChart *cc;
    PredefinedInteractions *df;
    SaveLoader *snl;
    int mode;
    int skipThreshold;

    vector<double> stepSize;
    int angularStepCount[3]{};
    vector<double> origin; // used to convert relative coord to Cartesian

    vector<Orientation *> startingOriLists[8];

    // For sampling
    CartesianHypercube *curCube;
    int curFlipNo;

    // Status: -1 for waiting to be sampled, 0 for does not exist, 1 for no intersection or intersection outside of tet bound, 2 for collision, 3 for bad angle, 4 for valid, 5 for starting cube.

    // All cubes in the "frontier" kept as relative coord - list of bools pair.
    // List of bools keeps data on whether to visit its 12 5-dim faces.
    // Faces are visited if corresponding bool is true.
    unordered_map<vector<int>, vector<bool>, vector_int_hash, vector_int_eq> goodFrontierCubes;
    unordered_map<vector<int>, vector<bool>, vector_int_hash, vector_int_eq> badFrontierCubes;
    stack<vector<int>> cubesToTraverse;

    vector<CartesianPoint *> pointBuffer;
    int savePointCounter;
    int savePointFreq;

    // For result bookkeeping

    // Number of points obtained from original Cayley sampling
    int originalPointCount[8];

    // Number of points sampled. Notice number of not realizable is not needed, since we put a blue point in all not realizable cube's centre.
    int validPointCount[8];
    int badAnglePointCount[8];
    int collisionPointCount[8];

    // Number of cubes being sampled
    int sampledValidCubeCount[8];
    int sampledBadAngleCubeCount[8];
    int sampledCollisionCubeCount[8];
    int sampledNotRealizableCubeCount[8];
    int sampledNoIntersectionCubeCount[8];

    int visitedCubeCount[8];
    int visitedFacetCount[8];
    unsigned long long visitedSimplexCount[8];
    int pointCount[8];

    // Number of cubes when points are mapped into them
    int mappedValidCubeCount[8];
    int mappedBadAngleCubeCount[8];
    int mappedCollisionCubeCount[8];
    int mappedNotRealizableCubeCount[8];

    unordered_set<vector<int>, vector_int_hash, vector_int_eq> allMappedValidCubesRelativeCoord;
    unordered_set<vector<int>, vector_int_hash, vector_int_eq> allMappedBadAngleCubesRelativeCoord;
    unordered_set<vector<int>, vector_int_hash, vector_int_eq> allMappedCollisionCubesRelativeCoord;
    unordered_set<vector<int>, vector_int_hash, vector_int_eq> allMappedNotRealizableCubesRelativeCoord;


};

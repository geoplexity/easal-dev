/*
   This file is part of EASAL.

   EASAL is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   EASAL is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
/*
 * Cartesian.cpp
 *
 *  Created on: 2020
 *      Author: Yichi Zhang
 */

#include "Cartesian.h"

#define PI 3.14159265

CartesianPoint::CartesianPoint(vector<double> coord)
{
    this->cartesianCoord = vector<double>(6);
    for (int i = 0; i < 6; ++i)
    {
        this->cartesianCoord[i] = coord[i];
    }
    this->flipNo = -1;
    this->status = -1;
}

CartesianPoint::CartesianPoint(CartesianPoint *copy)
{
    this->cartesianCoord = vector<double>(6);
    for (int i = 0; i < 6; ++i)
    {
        this->cartesianCoord[i] = copy->cartesianCoord[i];
    }
    if (!copy->cayleyDistances.empty())
    {
        for (auto &i: copy->cayleyDistances)
        {
            this->cayleyDistances.push_back(i);
        }
        for (auto &i: copy->cayleyParameters)
        {
            this->cayleyParameters.push_back(i);
        }
    }
    this->flipNo = copy->flipNo;
    this->status = copy->status;
}

CartesianPoint::~CartesianPoint() = default;

void CartesianPoint::printPoint(bool formatted)
{
    if (formatted)
    {
        cout << "x: " << this->cartesianCoord[0] << ", y: " << this->cartesianCoord[1] << ", z: "
             << this->cartesianCoord[2] << ", alpha: " << this->cartesianCoord[3] << ", beta: "
             << this->cartesianCoord[4] << ", gamma: " << this->cartesianCoord[5];
    }
    else
    {
        for (auto &coord: this->cartesianCoord)
        {
            cout << coord << ' ';
        }
    }
    cout << endl;
}

CartesianSimplex::CartesianSimplex(int _dim, vector<CartesianPoint *> _vertices)
{
    this->dim = _dim;
    this->vertices = std::move(_vertices);
}

CartesianSimplex::~CartesianSimplex()
{
    for (auto vertex: this->vertices)
    {
        delete (vertex);
    }
}
/*
CartesianFacet::CartesianFacet(CartesianPoint *point, int dim, vector<double> stepSize)
{
    this->centre = point;
    this->dim = dim;
    this->stepSize = vector<double>(6);

    // step size of angle should divide 2*\pi
    if (abs(2 * PI / stepSize[3] - floor(2 * PI / stepSize[3])) >= TOL_EQUAL ||
        abs(2 * PI / stepSize[4] - floor(2 * PI / stepSize[4])) >= TOL_EQUAL ||
        abs(2 * PI / stepSize[5] - floor(2 * PI / stepSize[5])) >= TOL_EQUAL)
    {
        throw;
    }

    for (int i = 0; i < 6; ++i)
    {
        this->stepSize[i] = stepSize[i];
    }
}
*/
//CartesianFacet::~CartesianFacet() = default;
/*
vector<CartesianSimplex *> CartesianFacet::getAllSimplices()
{
    vector<int> dims;
    for (int i = 0; i < 6; ++i)
    {
        if (this->stepSize[i] > 0)
        {
            dims.push_back(i);
        }
    }

    vector<CartesianSimplex *> ret;

    for (int i = 0; i < FACTORIAL[dim]; ++i)
    {
        vector<int> direction = Cartesian::dec2perm(i, dims);
        vector<double> coords(this->centre->cartesianCoord);
        for (int j = 0; j < 6; ++j)
        {
            coords[j] -= this->stepSize[j] / 2;
        }
        auto currentPoint = new CartesianPoint(coords);
        vector<CartesianPoint *> points;
        points.emplace_back(currentPoint);
        for (int j = 0; j < dim; ++j)
        {
            vector<double> newCoord(currentPoint->cartesianCoord);
            newCoord[direction[j]] += stepSize[direction[j]];
            currentPoint = new CartesianPoint(newCoord);
            points.emplace_back(currentPoint);
        }
        auto currentSimplex = new CartesianSimplex(dim, points);
        ret.emplace_back(currentSimplex);
    }
    return ret;
}
*/
bool vector_double_eq::operator()(const vector<double> &v0, const vector<double> &v1) const
{
    if (v0.size() != v1.size())
    {
        return false;
    }
    size_t len = v0.size();
    for (int i = 0; i < len; ++i)
    {
        if (abs(v0[i] - v1[i]) > TOL_EQUAL)
        {
            return false;
        }
    }
    return true;
}

size_t vector_double_hash::operator()(const vector<double> &v) const
{
    double tmp = 1;
    size_t len = v.size();
    for (int i = 0; i < len; ++i)
    {
        tmp *= v[i];
    }
    tmp = floor(tmp * 1e6);
    return hash<double>()(tmp);
}

bool vector_int_eq::operator()(const vector<int> &v0, const vector<int> &v1) const
{
    if (v0.size() != v1.size())
    {
        return false;
    }
    size_t len = v0.size();
    for (int i = 0; i < len; ++i)
    {
        if (v0[i] != v1[i])
        {
            return false;
        }
    }
    return true;
}

size_t vector_int_hash::operator()(const vector<int> &v) const
{
    size_t seed = v.size();
    for (auto &i: v)
    {
        seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }
    return seed;
}

bool Cartesian::isEqual(vector<double> cpA, vector<double> cpB)
{
    if (cpA.size() != cpB.size())
    {
        return false;
    }
    size_t len = cpA.size();
    for (int i = 0; i < len; ++i)
    {
        if (abs(cpA[i] - cpB[i]) > TOL_EQUAL)
        {
            return false;
        }
    }
    return true;
}

bool Cartesian::isIn(const vector<int> &cp, const unordered_set<vector<int>, vector_int_hash, vector_int_eq> &cps)
{
    if (cps.empty())
    {
        return false;
    }

    auto search = cps.find(cp);
    if (search != cps.end())
    {
        return true;
    }
    return false;
}

bool Cartesian::isIn(const vector<double> &cp,
                     const unordered_set<vector<double>, vector_double_hash, vector_double_eq> &cps)
{
    if (cps.empty())
    {
        return false;
    }

    auto search = cps.find(cp);
    if (search != cps.end())
    {
        return true;
    }
    return false;
}

tuple<double, double> Cartesian::cartesianDist(CartesianPoint *p1, CartesianPoint *p2)
{
    double trans, rot;
    trans = sqrt(pow(p1->cartesianCoord[0] - p2->cartesianCoord[0], 2) +
                 pow(p1->cartesianCoord[1] - p2->cartesianCoord[1], 2) +
                 pow(p1->cartesianCoord[2] - p2->cartesianCoord[2], 2));
    rot = sqrt(
            pow(p1->cartesianCoord[3] - p2->cartesianCoord[3] > PI ?
                PI - p1->cartesianCoord[3] + p2->cartesianCoord[3] :
                p1->cartesianCoord[3] - p2->cartesianCoord[3], 2) +
            pow(p1->cartesianCoord[4] - p2->cartesianCoord[4] > PI ?
                PI - p1->cartesianCoord[4] + p2->cartesianCoord[4] :
                p1->cartesianCoord[4] - p2->cartesianCoord[4], 2) +
            pow(p1->cartesianCoord[5] - p2->cartesianCoord[5] > PI ?
                PI - p1->cartesianCoord[5] + p2->cartesianCoord[5] :
                p1->cartesianCoord[5] - p2->cartesianCoord[5], 2));
    return tuple<double, double>(trans, rot);
}

vector<int> Cartesian::dec2perm(int dec, const vector<int> &toPerm)
{
    if (dec >= FACTORIAL[toPerm.size()])
    {
        throw;
    }

    if (toPerm.size() == 1)
    {
        return toPerm;
    }

    vector<int> beforePerm(toPerm);
    size_t length = toPerm.size();
    vector<int> afterPerm(length);

    for (int i = 0; i < length - 1; ++i)
    {
        int key = dec / FACTORIAL[length - i - 1];
        afterPerm[i] = beforePerm[key];
        beforePerm.erase(beforePerm.begin() + key);
        dec %= FACTORIAL[length - i - 1];
    }
    afterPerm[toPerm.size() - 1] = beforePerm[0];
    return afterPerm;
}

CartesianHypercube::CartesianHypercube(CartesianPoint *centre, vector<double> stepSize)
{
    this->centre = centre;
    this->stepSize.assign(stepSize.begin(), stepSize.end());
    this->flipNum = -1;
    this->status = -1;

    this->dim = 0;
    for (int i = 0; i < 6; ++i)
    {
        if (this->stepSize[i] != 0)
        {
            ++this->dim;
        }
    }
}

CartesianHypercube::CartesianHypercube(CartesianHypercube *copy)
{
    this->centre = copy->centre;
    this->stepSize.assign(copy->stepSize.begin(), copy->stepSize.end());
    this->status = copy->status;
    this->cayleyParaBasisDistance = copy->cayleyParaBasisDistance;
    this->cayleyParaBasisParameter = copy->cayleyParaBasisParameter;
    this->dim = copy->dim;
    this->flipNum = copy->flipNum;
}

CartesianHypercube::~CartesianHypercube() = default;

vector<int> Cartesian::signedDec2ter(int dec)
{
    if (dec >= 729)
    {
        throw;
    }
    vector<int> ret(6);
    for (int i = 0; i < 6; ++i)
    {
        ret[5 - i] = dec % 3 - 1;
        dec /= 3;
    }
    return ret;
}

vector<int> Cartesian::unsignedDec2ter(int dec)
{
    if (dec >= 729)
    {
        throw;
    }
    vector<int> ret(6);
    for (int i = 0; i < 6; ++i)
    {
        ret[5 - i] = dec % 3;
        dec /= 3;
    }
    return ret;
}

vector<double>
Cartesian::calcPointFromRelCoord(vector<double> origin, vector<double> stepSize, vector<int> relCoord)
{
    vector<double> ret(6, 0);
    for (int i = 0; i < 6; ++i)
    {
        ret[i] = origin[i] + stepSize[i] * relCoord[i] / 2;
    }
    return ret;
}

CartesianPoint *CartesianHypercube::getPoint(vector<int> directions)
{
    vector<double> coords(6);
    for (int i = 0; i < 6; ++i)
    {
        switch (directions[i])
        {
            case -1:
                coords[i] = this->centre->cartesianCoord[i] - this->stepSize[i] / 2;
                break;
            case 0:
                coords[i] = this->centre->cartesianCoord[i];
                break;
            case 1:
                coords[i] = this->centre->cartesianCoord[i] + this->stepSize[i] / 2;
                break;
            default:
                throw;
        }
    }
    return new CartesianPoint(coords);
}

CartesianPoint *CartesianHypercube::get5dFacetCentre(int coordAxis, bool direction)
{
    auto *ret = new CartesianPoint(this->centre);
    if (direction)
    {
        ret->cartesianCoord[coordAxis] -= this->stepSize[coordAxis] / 2;
    }
    else
    {
        ret->cartesianCoord[coordAxis] += this->stepSize[coordAxis] / 2;
    }
    return ret;
}


bool CartesianHypercube::isPointIn(vector<double> pt)
{
    return (pt[0] > this->centre->cartesianCoord[0] - this->stepSize[0] * (0.5 + TOL_EQUAL))
           && (pt[0] < this->centre->cartesianCoord[0] + this->stepSize[0] * (0.5 + TOL_EQUAL))
           && (pt[1] > this->centre->cartesianCoord[1] - this->stepSize[1] * (0.5 + TOL_EQUAL))
           && (pt[1] < this->centre->cartesianCoord[1] + this->stepSize[1] * (0.5 + TOL_EQUAL))
           && (pt[2] > this->centre->cartesianCoord[2] - this->stepSize[2] * (0.5 + TOL_EQUAL))
           && (pt[2] < this->centre->cartesianCoord[2] + this->stepSize[2] * (0.5 + TOL_EQUAL))
           && (pt[3] > this->centre->cartesianCoord[3] - this->stepSize[3] * (0.5 + TOL_EQUAL))
           && (pt[3] < this->centre->cartesianCoord[3] + this->stepSize[3] * (0.5 + TOL_EQUAL))
           && (pt[4] > this->centre->cartesianCoord[4] - this->stepSize[4] * (0.5 + TOL_EQUAL))
           && (pt[4] < this->centre->cartesianCoord[4] + this->stepSize[4] * (0.5 + TOL_EQUAL))
           && (pt[5] > this->centre->cartesianCoord[5] - this->stepSize[5] * (0.5 + TOL_EQUAL))
           && (pt[5] < this->centre->cartesianCoord[5] + this->stepSize[5] * (0.5 + TOL_EQUAL));
}


CartesianHypercube *CartesianHypercube::getNeighbour(int direction)
{
    auto point = new CartesianPoint(this->centre);
    if (direction >= 0 && direction <= 5)
    {
        point->cartesianCoord[direction] += this->stepSize[direction];
        if (direction >= 3)
        {
            while (point->cartesianCoord[direction] > PI)
            {
                point->cartesianCoord[direction] -= 2 * PI;
            }
        }
        return new CartesianHypercube(point, this->stepSize);
    }
    else if (direction >= 6 && direction <= 11)
    {
        point->cartesianCoord[direction - 6] -= this->stepSize[direction - 6];
        if (direction >= 9)
        {
            while (point->cartesianCoord[direction - 6] < -PI)
            {
                point->cartesianCoord[direction - 6] += 2 * PI;
            }
        }
        return new CartesianHypercube(point, this->stepSize);
    }
    else
    {
        throw;
    }
}

CartesianHypercube::CartesianHypercube(vector<int> centreRelCoord)
{
    this->relCoord = centreRelCoord;
    this->centre = nullptr;
    this->dim = 0;

    for (int i = 0; i < 6; ++i)
    {
        if (centreRelCoord[i] % 2 == 0)
        {
            ++this->dim;
        }
    }
    this->status = -1;
    this->flipNum = -1;
}

vector<vector<int>> CartesianHypercube::getAllFacetRelCoords(int resultDim)
{
    vector<vector<int>> ret;

    for (int i = 0; i < 729; ++i)
    {
        vector<int> ter = Cartesian::signedDec2ter(i);

        vector<int> facetCoord = this->relCoord;
        bool goodFacet = true;
        for (int j = 0; j < 6; ++j)
        {
            if ((this->relCoord[j] % 2 != 0) && (ter[j] != 0))
            {
                goodFacet = false;
                break;
            }
            facetCoord[j] += ter[j];
        }

        int curDim = 0;
        for (int j = 0; j < 6; ++j)
        {
            if (facetCoord[j] % 2 == 0)
            {
                ++curDim;
            }
        }
        if (curDim != resultDim)
        {
            goodFacet = false;
        }

        if (goodFacet)
        {
            ret.emplace_back(facetCoord);
        }
    }
    return ret;
}

vector<vector<vector<int>>> CartesianHypercube::getAllSimplexRelCoords()
{
    vector<vector<vector<int>>> ret;
    if (this->dim == 0)
    {
        vector<vector<int>> vertices;
        vertices.push_back(this->relCoord);
        ret.push_back(vertices);
    }
    else
    {
        vector<bool> directionIsActive(6, false);
        vector<int> activeDirections;
        for (int i = 0; i < 6; ++i)
        {
            if (this->relCoord[i] % 2 == 0)
            {
                directionIsActive[i] = true;
                activeDirections.emplace_back(i);
            }
        }

        for (int i = 0; i < FACTORIAL[this->dim]; ++i)
        {
            vector<vector<int>> vertices;
            vector<int> direction = Cartesian::dec2perm(i, activeDirections); // TODO use std::next_permutation

            vector<int> simplexCoord = this->relCoord;
            for (int j = 0; j < 6; ++j)
            {
                if (directionIsActive[j])
                {
                    simplexCoord[j] -= 1;
                }
            }
            vertices.emplace_back(simplexCoord);

            for (int j = 0; j < this->dim; ++j)
            {
                simplexCoord[direction[j]] += 2;
                vertices.emplace_back(simplexCoord);
            }
            ret.emplace_back(vertices);
        }
    }
    return ret;
}

vector<vector<vector<int>>> CartesianHypercube::getAllSegmentRelCoordsFrom5dFace()
{
    vector<vector<vector<int>>> ret;

    vector<int> activeDirections = {0, 1, 2, 3, 4, 5};
    for (int i = 0; i < 6; ++i)
    {
        if (this->relCoord[i] % 2 != 0)
        {
            activeDirections.erase(activeDirections.begin() + i);
            break;
        }
    }

    for (int j = 0; j < 32; ++j)
    {
        vector<int> vertexRelCoord = this->relCoord;
        for (int k = 0; k < 5; ++k)
        {
            if ((j >> k) % 2 == 0)
            {
                --vertexRelCoord[activeDirections[k]];
            }
            else
            {
                ++vertexRelCoord[activeDirections[k]];
            }
        }
        vector<vector<int>> curSegment;
        curSegment.emplace_back(this->relCoord);
        curSegment.emplace_back(vertexRelCoord);
        ret.emplace_back(curSegment);
    }
    return ret;
}

vector<CartesianSimplex *> CartesianHypercube::getAllSimplices()
{
    vector<CartesianSimplex *> ret;

    vector<bool> directionIsActive(6, false);
    vector<int> activeDirections;
    for (int i = 0; i < 6; ++i)
    {
        if (this->relCoord[i] % 2 == 0)
        {
            directionIsActive[i] = true;
            activeDirections.emplace_back(i);
        }
    }
/*
    if (dim == 1) // special treatment for 5D node's 1D simplex (segment)
    {
        vector<CartesianPoint *> vertices;
        vector<int> relativeCoord0;
        vector<int> relativeCoord2;
        relativeCoord0.assign(this->relCoord.begin(), this->relCoord.end());
        relativeCoord2.assign(this->relCoord.begin(), this->relCoord.end());
        --relativeCoord0[activeDirections[0]];
        ++relativeCoord2[activeDirections[0]];

        auto cartesianCoord0 = Cartesian::calcPointFromRelCoord(origin, stepSize, relativeCoord0);
        auto cartesianCoord1 = Cartesian::calcPointFromRelCoord(origin, stepSize, this->relCoord);
        auto cartesianCoord2 = Cartesian::calcPointFromRelCoord(origin, stepSize, relativeCoord2);

        auto cartesianPoint0 = new CartesianPoint(cartesianCoord0);
        auto cartesianPoint1 = new CartesianPoint(cartesianCoord1);
        auto cartesianPoint2 = new CartesianPoint(cartesianCoord1);
        auto cartesianPoint3 = new CartesianPoint(cartesianCoord2);

        vector<CartesianPoint *> vectors0;
        vector<CartesianPoint *> vectors1;
        vectors0.emplace_back(cartesianPoint0);
        vectors0.emplace_back(cartesianPoint1);
        vectors1.emplace_back(cartesianPoint2);
        vectors1.emplace_back(cartesianPoint3);

        auto simplex0 = new CartesianSimplex(dim, vectors0);
        auto simplex1 = new CartesianSimplex(dim, vectors1);
        ret.emplace_back(simplex0);
        ret.emplace_back(simplex1);
    }
    else*/

    for (int i = 0; i < FACTORIAL[this->dim]; ++i)
    {
        vector<CartesianPoint *> vertices;
        vector<int> direction = Cartesian::dec2perm(i, activeDirections);

        vector<int> pointRelCoord;
        pointRelCoord.assign(this->relCoord.begin(), this->relCoord.end());
        for (int j = 0; j < 6; ++j)
        {
            if (directionIsActive[j])
            {
                --pointRelCoord[j];
            }
        }
        auto pointCartesianCoord = Cartesian::calcPointFromRelCoord(origin, stepSize, pointRelCoord);
        auto point = new CartesianPoint(pointCartesianCoord);
        vertices.emplace_back(point);

        for (int j = 0; j < dim; ++j)
        {
            pointRelCoord[direction[j]] += 2;
            pointCartesianCoord = Cartesian::calcPointFromRelCoord(origin, stepSize, pointRelCoord);
            point = new CartesianPoint(pointCartesianCoord);
            vertices.emplace_back(point);
        }
        auto simplex = new CartesianSimplex(dim, vertices);
        ret.emplace_back(simplex);
    }

    return ret;
}

vector<double> Cartesian::getMidPoint(vector<double> pt1, vector<double> pt2)
{
    auto len = pt1.size();
    if (len != pt2.size())
    {
        throw;
    }
    vector<double> ret;
    for (int i = 0; i < len; ++i)
    {
        double entry = (pt1[i] + pt2[i]) / 2;
        ret.push_back(entry);
    }
    return ret;
}

vector<vector<int>> Cartesian::calcCubesFromFacet(vector<int> facetRelCoord)
{
    vector<vector<int>> ret;
    vector<int> activeDirections;
    for (int i = 0; i < 6; ++i)
    {
        if (facetRelCoord[i] % 2 != 0)
        {
            activeDirections.push_back(i);
        }
    }

    int directionCount = activeDirections.size();
    int returnCount = POWER_OF_TWO[directionCount];
    for (int i = 0; i < returnCount; ++i)
    {
        vector<int> curEntry = facetRelCoord;
        for (int j = 0; j < directionCount; ++j)
        {
            bool direction = (i >> j) % 2 != 0;
            if (direction)
            {
                ++curEntry[activeDirections[j]];
            }
            else
            {
                --curEntry[activeDirections[j]];
            }
        }
        ret.push_back(curEntry);
    }

    return ret;
}

